<?php
require_once '.././conexion/Conexion.php';
    class app extends Conexion{
	private $username;
	private $password;
	private $id_user;
	private $id_formu;
	private $lat;
	private $lon;
	private $idemp;
	//************* update - variables******************
	private $fecha;
	private $idevento;
	private $idformulario;

	private $cellphone;
	private $correo;
	private $imagen;
	private $firma;
	private $nombre_img;

	//***************update - funciones*****************
	public function getfecha(){
	  return $this->fecha;
	}
	public function setfecha($fecha){
	  $this->fecha = $fecha;
	  return $this;
	}
	public function getidevento(){
	  return $this->idevento;
	}
	public function setidevento($idevento){
	  $this->idevento = $idevento;
	  return $this;
	}
	public function getidformulario(){
	  return $this->idformulario;
	}
	public function setidformulario($idformulario){
	  $this->idformulario = $idformulario;
	  return $this;
	}
	public function getCellphone() 
	{ 
	return $this->cellphone; 
	} 
	public function setCellphone($cellphone) 
	{ 
	$this->cellphone = $cellphone; return $this; 
	} 
	public function getCorreo() 
	{ 
	return $this->correo; 
	} 
	public function setCorreo($correo) 
	{ 
	$this->correo = $correo; return $this; 
	} 

	public function getImagen() 
	{ 
	return $this->imagen; 
	} 
	public function setImagen($imagen) 
	{ 
	$this->imagen = $imagen; return $this; 
	} 

	public function getFirma() 
	{ 
	return $this->firma; 
	} 
	public function setFirma($firma) 
	{ 
	$this->firma = $firma; return $this; 
	} 

	public function getNombre_img() 
	{ 
	return $this->nombre_img; 
	} 
	public function setNombre_img($nombre_img) 
	{ 
	$this->nombre_img = $nombre_img; return $this; 
	} 

	//******************************************************************************

	  public function getusername()
	  {
		  return $this->username;
	  }      
	  public function setusername($username)
	  {
		  $this->username = $username;
		  return $this;
	  }
	  public function getpassword()
      {
          return $this->password;
      }      
      public function setpassword($password)
      {
          $this->password = $password;
          return $this;
      }
      public function getid_user()
      {
          return $this->id_user;
      }      
      public function setid_user($id_user)
      {
          $this->id_user = $id_user;
          return $this;
      }

      public function getid_formu()
      {
          return $this->id_formu;
      }
      
      public function setid_formu($id_formu)
      {
          $this->id_formu = $id_formu;
          return $this;
      }
      public function getidemp()
      {
          return $this->idemp;
      }
      public function setidemp($idemp)
      {
           $this->idemp = $idemp;
      }
      public function getLat()
      {
          return $this->lat;
      }
      
      public function setLat($lat)
      {
          $this->lat = $lat;
      }
      public function getLon()
      {
          return $this->lon;
      }
      
      public function setLon($lon)
      {
          $this->lon = $lon;
      }
        

      public function verifica_user() {
          $user = $this->getusername();
          $pass = $this->getpassword();

        //   $sql = "SELECT * FROM usuario WHERE username = '" . $user . "' AND password = '" . $pass . "'";
        $sql = "SELECT * FROM inv_users INNER JOIN inv_user_movil WHERE inv_user_movil.idUsua = inv_users.idUsua and inv_users.usua = '".$user."' and inv_user_movil.code_login = '".$pass."'";

          $sentencia = $this->dblink->prepare($sql);

          $sentencia->execute();
          $resultado = $sentencia->fetch();

          if ($resultado != FALSE){
              return TRUE;
          }else{
              return FALSE;
          }
      }
      public function login()
      {
       
          
          $user = $this->getusername();
          $pass = $this->getpassword();
          $sql = "SELECT * FROM inv_users INNER JOIN inv_user_movil WHERE inv_user_movil.idUsua = inv_users.idUsua and inv_users.usua = '".$user."' and inv_user_movil.code_login = '".$pass."'";
            $sentencia = $this->dblink->prepare($sql);

            $sentencia->execute();
          $count = $sentencia->rowCount();
          if ($count > 0) {
              foreach($sentencia as $row) {
                    $data["id_user"] = utf8_encode($row["idUsua"]);
                    $data["username"] = utf8_encode($row["usua"]);
                    $data["password"] = utf8_encode($row["pwd"]);
                    $data["id_movilidad"] = utf8_encode($row["idUserM"]);
                    $data["estado"] = utf8_encode($row["state"]);
                    $data["android_id"] = utf8_encode($row["android_id"]);
                    $data["code"] = utf8_encode($row["code_login"]); 
                    $data["idemp"] = utf8_encode($row["idemp"]); //ANEXADO POR JEAN      
              }
              $data["state"] = "Success";
          }
         else{
             $data['state'] ="No Encontrado"; 
         }
          
          
      
         return $data;
      }//getevent

      //se agrega la posicion
      public function agregar_position2() {
                $id_user = $this->getid_user();
                $id_formu = $this->getid_formu();
                $lat = $this->getLat();
                $lon = $this->getLon();
                $idemp = $this->getidemp();

                $stmt2 = $this->dblink->prepare("SELECT MAX(idform) as id FROM inv_pl_forms");
                $stmt2->execute();
                $respuesta = $stmt2->fetch();
                $r = trim($respuesta['id']);
                //se valida que el [id_formu] no este repetido
                if($r == $id_formu){
                  return FALSE;
                }else{
                  /////////////////////////////////////////////////////////////////////
                  $sql = "INSERT INTO inv_pl_position(user_reg,idform,idemp,lat,lon) 
                  VALUES ('" . $id_user . "','" . $id_formu . "','" . $idemp . "','" . $lat . "', '" . $lon . "')";

                  $sentencia = $this->dblink->prepare($sql);
                  $resultado = $sentencia->execute();

                  if ($resultado != 1){
                      //ocurrio un error al insertar
                      return FALSE;
                  }    
                  //Inserta correctamente
                  return TRUE;
                }     
    }
        //se agrega la taryectoria del usuario
        public function agregar_trayectoriauser() {
              
              $id_user = $this->getid_user();
              $lat = $this->getLat();
              $lon = $this->getLon();
			  $fecha = $this->getfecha(); //agregar fecha - luis
              $idempp = $this->getidemp();//add jean
              
              //get day:hours
              //$fecha = date('Y-m-d H:i:s');

              $sql = "INSERT INTO inv_trajectory_users(idemp,idUsua,lat,lon,date) 
              VALUES ('" . $idempp . "','" . $id_user . "','" . $lat . "','" . $lon . "', '" . $fecha . "')";

              $sentencia = $this->dblink->prepare($sql);
              $resultado = $sentencia->execute();

              if ($resultado != 1){
                  //ocurrio un error al insertar
                  return FALSE;
              }    
              //InsertĂ³ correctamente
              return TRUE;
          
        }

        //get fileds values mobilidad - estado -id_user => the table usuario
        public function get_mobilidad_estado_iduser(){
              $user = $this->getusername();
              $pass = $this->getpassword();

            //   $stmt = $this->dblink->prepare("SELECT * FROM usuario WHERE username = :username AND password = :password");
              $stmt = $this->dblink->prepare("SELECT * FROM inv_users INNER JOIN inv_user_movil WHERE inv_user_movil.idUsua = inv_users.idUsua and inv_users.usua = :username and inv_user_movil.code_login = :password");

              $stmt->bindParam(":username", $user);
              $stmt->bindParam(":password", $pass);

              $stmt->execute();
              $stmt->bindColumn("idUsua", $id_user);
              $stmt->bindColumn("usua", $username);
              $stmt->bindColumn("state", $estado);

              while ($fila = $stmt->fetch(PDO::FETCH_BOUND)){
                $respuesta = array();
                $respuesta["idUsua"] = utf8_encode($id_user);
                $respuesta["usua"] = utf8_encode($username);
                $respuesta["state"] = utf8_encode($estado);
                $respuesta["code_login"] = utf8_encode($fila['code_login']);
                // $respuesta["username"] = utf8_encode($username);
                // $respuesta["estado"] = utf8_encode($estado);
                // $respuesta["id_user"] = utf8_encode($id_user);
                // $respuesta["username"] = utf8_encode($username);
                // $respuesta["estado"] = utf8_encode($estado);
              }
              return $respuesta;
        }

        public function verifica_internet(){

            $connected = @fsockopen("www.google.com", 80); //website, port  (try 80 or 443)
            $respuesta = array();
            if ($connected){
                 $respuesta["internet"] = utf8_encode("yes");
                 fclose($connected);
            }else{
                $respuesta["internet"] = utf8_encode("no");
            }
        return $respuesta;
      }

      public function insert_android_id($android_id,$id_mov)
      {
          $sql = "UPDATE `inv_user_movil` SET `android_id`= :android WHERE idUserM = :ID";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->execute(array(":android" => $android_id, ":ID" => $id_mov));
          $count = $sentencia->rowCount();
          if ($count >0) {
              $data["state"] = "Success";
          }
          else{
            $data["state"] = "Wrong";
          }
          return $data;
      }
      public function Planner($id_user)
      {
        $data = [];
        
        
            $sql = "SELECT * FROM `inv_planner` where create_it_user = :ID_USER AND state != 'Cumplida'";
            $resultado = $this->dblink->prepare($sql);
            $resultado ->execute(array(":ID_USER" => $id_user));
            $count = $resultado->rowCount();
            if ($count > 0) {
               
        
                foreach($resultado as $row) {
                    
                    $data[] = [
                        'id'              => utf8_encode($row["idplanner"]),
                        'idemp'           => utf8_encode($row["idemp"]),
                        'title'           => utf8_encode($row["title"]),
                        'descripcion'     => utf8_encode($row["description"]),
                        'start'           => utf8_encode($row["start_event"]),
                        'end'             => utf8_encode($row["end_event"]),
                        'color'           => utf8_encode($row["color"]),
                        'text_color'      => utf8_encode($row["text_color"]),                        
                        'detalles'        => utf8_encode($row["observations"]),
                        'estado'          => utf8_encode($row["state"]),
                    ];
                }
            }
            if ($data) {

                $datos["state"] = 1;
                $datos["datos"] = $data;
        
                print json_encode($datos);
            } else {
                print json_encode(array(
                    "state" => 2,
                    "mensaje" => "Ha ocurrido un error"
                ));
            }
            // return $data;
        }
		
		//***************** update - ufuncion para guardar el formulario************************
		public function guardar_formulario(){
              $username = $this->getusername();
              $cellphone = $this->getCellphone();
              $correo = $this->getCorreo();
              $imagen = $this->getImagen();
              $id_user = $this->getid_user();//add
              $firma = $this->getFirma();
              $nombre_img = $this->getNombre_img();
              $fecha = $this->getfecha();
              $idemp = $this->getidemp();//add jean
              $name_img = $idemp . "" . $id_user . "" . $username . "_" . $nombre_img; 



              $path = "../webservices/img_cedula/$name_img.png";
              $url = "".$name_img.".png";

              $path2 = "../webservices/img_firma/$name_img.png";
              $url2 = "".$name_img.".png";

			  
              $name_img = $idemp . "_" . $id_user . "_" . $username . "_" . $nombre_img; //anexarlo

              $path = "../webservices/img_cedula/$name_img.png";
              
			        $url = "".$name_img.".png";   //anexarlo

              $path2 = "../webservices/img_firma/$name_img.png";
			        $url2 = "".$name_img.".png"; //anexarlo


              file_put_contents($path,base64_decode($imagen));
              $bytesArchivo=file_get_contents($path);

              file_put_contents($path2,base64_decode($firma));
              $bytesArchivo=file_get_contents($path2);

              $sql = "INSERT INTO inv_pl_forms(idemp,name,movil,mail,img_document,img_firm,user_reg,fecha_reg) 
                VALUES ('" . $idemp . "','" . $username . "', '" . $cellphone . "', '" . $correo . "', 
                '" . $url . "', '" . $url2 . "', '" . $id_user . "' , '" . $fecha . "')";

              $sentencia = $this->dblink->prepare($sql);
              $resultado = $sentencia->execute();

              if ($resultado != 1){
                  //ocurrio un error al insertar
                  return FALSE;
              }    
              //InsertĂ³ correctamente
              return TRUE;
        }
		//se obtiene el ultimo formulario registrado
		public function id_table_form(){
			$stmt = $this->dblink->prepare("SELECT MAX(idform) as id FROM inv_pl_forms");
			$stmt->execute();
			$stmt->bindColumn("id", $id_formu);
			while ($fila = $stmt->fetch(PDO::FETCH_BOUND)){
			  $respuesta = array();
			  $respuesta["id"] = utf8_encode($id_formu);
			}
			return $respuesta;
        }
		
		//se agrega la posicion
      public function agregar_position() {
                $stage = "Cumplida";
                $id_user = $this->getid_user();
                $id_formu = $this->getid_formu();
                $lat = $this->getLat();
                $lon = $this->getLon();
                $idevento = $this->getidevento();
                $idemp = $this->getIdemp();

                //se inserta en la tabla posicion
                $sql = "INSERT INTO inv_pl_position(user_reg,idform,idplanner,idemp,lat,lon) 
                VALUES ('" . $id_user . "','" . $id_formu . "','" . $idevento . "','" . $idemp . "','" . $lat . "', '" . $lon . "')";
                $sentencia = $this->dblink->prepare($sql);
                $resultado = $sentencia->execute();

                 //se actualiza el estado del evento en la tabla events
                $stmt = $this->dblink->prepare("UPDATE inv_planner SET state = :stage where idplanner = :idevento");
                $stmt->bindParam(":stage", $stage);
                $stmt->bindParam(":idevento", $idevento);
                $resultado2 = $stmt->execute();

                if ($resultado != 1){
                    //ocurrio un error al insertar
                    return FALSE;
                }    
                //Inserta correctamente
                return TRUE;   
        }
        //Show img
        public function get_foto_user($idusuario)
        {
          $stmt = $this->dblink->prepare("SELECT * FROM inv_users INNER JOIN inv_person ON inv_users.idperson = inv_person.idperson WHERE inv_users.idUsua = :id");
          $stmt->bindParam(":id", $idusuario);
          $stmt->execute();
          $stmt->bindColumn("imagen_Img", $foto);
      
          while ($fila = $stmt->fetch(PDO::FETCH_BOUND)){
            $respuesta = array();
            $respuesta["imagen_Img"] = utf8_encode($foto);
          }
          return $respuesta;
        }
        public function id_evento_user($id_evento){

          $stmt = $this->dblink->prepare("SELECT MAX(idplanner) as idplanner FROM inv_planner WHERE create_it_user = :id_evento");
          $stmt->bindParam(":id_evento", $id_evento);
          $stmt->execute();
          $stmt->bindColumn("idplanner", $idplanner);
          while ($fila = $stmt->fetch(PDO::FETCH_BOUND)){
            $respuesta = array();
            $respuesta["idplanner"] = utf8_encode($idplanner);
          }
  
          return $respuesta;
        }  
        
}
?>