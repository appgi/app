-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-12-2020 a las 16:41:55
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `app`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `descripcion` varchar(1000) NOT NULL,
  `start_event` datetime NOT NULL,
  `end_event` datetime NOT NULL,
  `color` varchar(191) DEFAULT NULL,
  `text_color` varchar(191) DEFAULT NULL,
  `create_it` int(11) NOT NULL,
  `create_by` text DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_by` text DEFAULT NULL,
  `update_at` datetime NOT NULL,
  `detalles` varchar(500) DEFAULT NULL,
  `Id_usuario` int(11) NOT NULL,
  `estado` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `title`, `descripcion`, `start_event`, `end_event`, `color`, `text_color`, `create_it`, `create_by`, `create_at`, `update_by`, `update_at`, `detalles`, `Id_usuario`, `estado`) VALUES
(1, 'evento area 2 de rrhh', 'az', '2020-12-01 17:18:02', '2020-12-01 21:18:02', '#178bf7', '#d44040', 6, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 6, 'Pendiente'),
(2, 'evento por si no funciona VENCIDO', ' ', '2020-11-11 09:18:02', '2020-11-11 09:18:02', '#4a9957', '#26179f', 20, '  ', '0000-00-00 00:00:00', '  ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(4, 'evento area 1 de RRHH VENCIDO', ' ', '2020-11-04 09:18:02', '2020-11-05 09:18:02', '#4a9957', '#26179f', 5, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 6, 'Vencido'),
(5, 'evento para la area 2 de it  VENCIDO', '', '2020-11-20 09:18:02', '2020-11-21 09:18:02', '#4a9957', '#26179f', 8, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(6, 'evento desde el usuario2', ' ', '2020-11-23 09:18:02', '2020-11-24 09:18:02', '#4a9957', '#26179f', 2, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 6, 'Pendiente'),
(7, 'kk3', '', '2020-12-11 09:18:02', '2020-12-12 09:18:02', '#4a9957', '#26179f', 2, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 6, 'Pendiente'),
(8, 'kk3 VENCIDO', ' ', '2020-11-08 09:18:02', '2020-11-09 09:18:02', '#4a9957', '#26179f', 0, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(9, 'evento de area 2 de rrhh  VENCIDO', ' ', '2020-11-13 21:18:02', '2020-11-14 21:18:02', '#81b1dd', '#ffffff', 6, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(13, 'prueba insert 1 VENCIDO', ' ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '#1998b5', '#0002ff', 6, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(14, 'prueba insert 2 VENCIDO', ' ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '#1998b5', '#0002ff', 6, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(15, 'evento area 2 de recurso humanos ', '  ', '2020-12-07 17:00:00', '2020-12-07 23:59:00', '#ad9b5b', '#ad9b5b', 6, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Pendiente'),
(16, 'prueba modal insert 2', ' ', '2020-11-29 00:00:00', '2020-11-30 00:00:00', '#53a0e9', '#000000', 2, ' ', '0000-00-00 00:00:00', 'dptRRHH', '2020-11-23 05:08:35', 'prueba detalles', 6, 'Cumplida'),
(17, 'prueba insert dependencia VENCIDO', ' ', '2020-11-22 09:00:00', '2020-11-22 18:00:00', '#000000', '#ffffff', 6, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(18, 'fdsgdsd', ' ', '2020-11-26 00:00:00', '2020-11-26 18:00:00', '#000000', '#ffffff', 6, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Pendiente'),
(19, 'prueba de insert para dpt IT', ' ', '2020-12-10 06:00:00', '2020-12-12 19:00:00', '#000000', '#ff0000', 4, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Pendiente'),
(20, 'prueba insert para nodo padre VENCIDO', ' ', '2020-11-20 10:00:00', '2020-11-21 21:00:00', '#1c00ff', '#ffffff', 2, 'dptRRHH', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Vencido'),
(21, 'prueba para hijop', ' ', '2020-11-26 00:00:00', '2020-11-30 21:00:00', '#1c00ff', '#ff0000', 8, ' ', '0000-00-00 00:00:00', ' ', '0000-00-00 00:00:00', ' ', 0, 'Pendiente'),
(25, 'prueba create at', ' ', '2020-12-08 16:00:27', '2020-12-08 23:01:39', '#e560c0', '#000000', 6, 'alf', '2020-11-20 16:01:56', 'dptRRHH', '2020-11-20 23:11:59', ' ', 0, 'Reprogramada'),
(26, 'dasdas asdasdsadas', ' ', '2020-12-09 18:13:28', '2020-12-09 23:13:35', '#53e98f', '#ffffff', 6, 'dptRRHH', '2020-11-20 16:14:01', ' ', '0000-00-00 00:00:00', ' ', 6, 'Pendiente'),
(27, 'evento con descripcion', 'se describe', '2020-11-27 18:06:25', '2020-11-27 21:06:33', '#757193', '#000000', 6, 'dptRRHH', '2020-11-20 17:06:55', ' ', '0000-00-00 00:00:00', ' ', 6, 'Pendiente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Id_usuario` (`Id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
