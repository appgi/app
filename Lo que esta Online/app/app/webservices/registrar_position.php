<?php
    if  ( ( !isset(  $_POST["id_user"]) ) || ( !isset(  $_POST["id_formu"]) ) 
        || ( !isset(  $_POST["lat"]) ) || ( !isset(  $_POST["lon"]))  
        || ( !isset(  $_POST["idevento"]) ) || ( !isset(  $_POST["idemp"]))
        )
        {
            $respuesta = array(
                "estado"=>"error"
            );
            
            echo json_encode($respuesta);
            exit();
        }
        $id_user = $_POST["id_user"];
        $id_formu = $_POST["id_formu"];
        $lat = $_POST["lat"];
        $lon = $_POST["lon"];
        $idevento = $_POST["idevento"];
        $idemp = $_POST["idemp"];
        
        require_once '../datos/app.clase.php';
        $postion = new app();
        $postion->setid_user($id_user);
        $postion->setid_formu($id_formu);
        $postion->setLat($lat);
        $postion->setLon($lon);
        $postion->setidevento($idevento);
        $postion->setIdemp($idemp);
        
        if ($postion->agregar_position()==TRUE){
            $respuesta = array(
                "estado"=>"exito"
            );
        }else{
            $respuesta = array(
                "estado"=>"error",
                "datos"=>""
            );
        }
        
        echo json_encode($respuesta);
?>