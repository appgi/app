<?php

if (isset($_POST['id']) && isset($_POST['android_id'])) {
    $id = $_POST["id"];
    $android_id = $_POST["android_id"];
    require_once '../datos/app.clase.php';
    $objGps = new app();
    echo json_encode($objGps->insert_android_id($android_id,$id));
} else {
    $respuesta = array(
        "state"=>"ERROR"
    );

    echo json_encode($respuesta);
    exit();
   
}
?>