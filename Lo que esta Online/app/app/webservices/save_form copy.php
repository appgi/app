<?php
    if  ( ( !isset(  $_POST["username"]) ) || ( !isset(  $_POST["cellphone"]) ) 
        || ( !isset(  $_POST["correo"]) ) || ( !isset(  $_POST["imagen_c"]))  
        || ( !isset(  $_POST["firma"]) ) || ( !isset(  $_POST["nombre_img"]))  
        || ( !isset(  $_POST["fecha"])) || ( !isset(  $_POST["idemp"]) || ( !isset(  $_POST["id_user"]))
    	)
        {
            $respuesta = array(
                "estado"=>"error"
            );
            
            echo json_encode($respuesta);
            exit();
        }
        $username = $_POST["username"];
        $cellphone = $_POST["cellphone"];
        $correo = $_POST["correo"];
        $imagen_c = $_POST["imagen_c"];
        $firma = $_POST["firma"];
        $nombre_img = $_POST["nombre_img"];
        $fecha = $_POST["fecha"];
        $id_user = $_POST["id_user"];
        $idemp = $_POST["idemp"];
        
        require_once '../datos/app.clase.php';
        $postion = new app();
        $postion->setusername($username);
        $postion->setCellphone($cellphone);
        $postion->setCorreo($correo);
        $postion->setImagen($imagen_c);
        $postion->setFirma($firma);
        $postion->setNombre_img($nombre_img);
        $postion->setfecha($fecha);
        $postion->setid_user($id_user);
        $postion->setidemp($idemp);//add jean
        
        if ($postion->guardar_formulario()==TRUE){
            $respuesta = array(
                "estado"=>"exito"
            );
        }else{
            $respuesta = array(
                "estado"=>"error",
                "datos"=>""
            );
        }
        
        echo json_encode($respuesta);
?>