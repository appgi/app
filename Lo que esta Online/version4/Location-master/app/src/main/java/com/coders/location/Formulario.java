package com.coders.location;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.coders.location.Services.GPS_Service;
import com.coders.location.Services.Utils;
import com.coders.location.eventos.ListEvent;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.sql.BDAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Formulario extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener, Dialogo_firma.finalizocuadrodialogo{

    help_coordenadas help_coordenadas;

    private MyReceiver myReceiver3;

    private final String CARPETA_RAIZ="misImagenesPrueba/";
    private final String RUTA_IMAGEN=CARPETA_RAIZ+"misFotos";

    final int COD_FOTO=20;

    Bitmap bitmap_img, bitmap_firma;
    ProgressDialog progreso;
    Button botonCargar,boton_firmar;
    ImageView imagen,img_firma;
    String path_img;
    Context context;

    // Creating Separate Directory for saving Generated Images
    String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Signature/";
    String pic_name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

    String StoredPath = DIRECTORY + pic_name + ".png";
    String name = pic_name;

    TextInputLayout username;
    TextInputLayout telefono;
    TextInputLayout correo;
    Button btn_registrar;

    public static final String REGEX_EMAIL ="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
    public static final String REGEX_username ="^[a-zA-ZÀ-ÿ\\s]{1,40}$";

    public static BDAdapter BD;
    String id_user,idevento;

    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        help_coordenadas = new help_coordenadas();

        context = this;

        //se obtiene el id del evento
        Bundle mibundle = this.getIntent().getExtras();
        if(mibundle!=null){
            idevento = mibundle.getString("idevento");
        }
        //Toast.makeText(this,idevento,Toast.LENGTH_SHORT).show();

        //get idusuario mediante sharedpreferneces
        LoginPrefences p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
            Toast.makeText(this,"id"+p.Getid(),Toast.LENGTH_SHORT).show();
        }

        BD = new BDAdapter(this);
        BD.openDB();

        //**************************************************************************************
        myReceiver3 = new Formulario.MyReceiver();
        setContentView(R.layout.activity_formulario);
        //**************************************************************************************

        //*** camara
        imagen= findViewById(R.id.imagemId);
        botonCargar= findViewById(R.id.btnCargarImg);
        //*** firma
        img_firma= findViewById(R.id.imagenfirma);
        boton_firmar= findViewById(R.id.btnCargarFirma);

        username = findViewById(R.id.input_username);
        telefono = findViewById(R.id.input_cellphone_user);
        correo = findViewById(R.id.input_corre_usuario);
        btn_registrar = findViewById(R.id.btn_registar);


        //********************************************************** verificacion de internet **********************************************************
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 || tipoConexion2) {
                /* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                btn_registrar.setOnClickListener(v -> {
                    //se valida que los campos nombre telefono correo esten completo
                    if (!validatecellphone() | !validateUsername()) {
                        return;
                    }
                    //se valida que una imagen exista!!!!
                    if (imagen.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.img_base).getConstantState()){
                        Toast.makeText(this, "Agrege una Foto a la Cedula!!!!!", Toast.LENGTH_LONG).show();
                    }else{
                        //se valida que una firma exista.!!!
                        if(img_firma.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.img_base).getConstantState()){
                            Toast.makeText(this, "Agrege una firma!!!!!!", Toast.LENGTH_LONG).show();
                        }else{
                            verifica_internet();
                        }
                    }
                });
            }
        }
        else {
            /* No estas conectado a internet */
            //Toast.makeText(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
            //boton registrar -> sin conexion a internet
                btn_registrar.setOnClickListener(v -> {
                //se valida los campos nombre telefono correo!!!!!
                if (!validatecellphone() | !validateUsername()) {
                    return;
                }
                //se valida que una imagen exista!!!!
                if (imagen.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.img_base).getConstantState()){
                    Toast.makeText(this, "Agrege una Foto a la Cedula!!!!!", Toast.LENGTH_LONG).show();
                }else{
                    //se valida que una firma exista.!!!
                    if(img_firma.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.img_base).getConstantState()){
                        Toast.makeText(this, "Agrege una firma!!!!!!", Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(this, "firma!!!!!!", Toast.LENGTH_LONG).show();///ok
                        save_local_formulario();
                    }
                }
            });
        }
        //********************************************************************************************************************
        boton_firmar.setOnClickListener(v -> {
            new Dialogo_firma(context,Formulario.this);
        });

        if(validaPermisos()){
            botonCargar.setEnabled(true);
        }else{
            botonCargar.setEnabled(false);
        }
    }
    @Override
    public void ResultadoDialogo(Bitmap bitmapfirma) {
        img_firma.setImageBitmap(bitmapfirma);
        bitmap_firma = bitmapfirma;
    }
    //*********************************************** validacion de campos ***********************************************
    private boolean validateUsername() {
        String usernameInput = username.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_username);

        if (usernameInput.isEmpty()) {
            username.setError("Campo vacio");
            Toast.makeText(this, "Campo Nombre vacio", Toast.LENGTH_LONG).show();
            return false;
        }else if(!patron.matcher(usernameInput).matches()){
            username.setError("Nombre invalido");
            Toast.makeText(this, "Nombre invalido", Toast.LENGTH_LONG).show();
            return false;
        }else {
            username.setError(null);
            return true;
        }
    }
    private boolean validatecellphone() {
        String cellphoneInput = telefono.getEditText().getText().toString().trim();
        if (cellphoneInput.isEmpty()) {
            telefono.setError("Campo vacio");
            Toast.makeText(this, "Campo telefono vacio", Toast.LENGTH_LONG).show();
            return false;
        }else if (cellphoneInput.length() > 8) {
            telefono.setError("Numero de Telefono muy LARGO");
            Toast.makeText(this, "Numero de Telefono muy LARGO", Toast.LENGTH_LONG).show();
            return false;
        }else if (cellphoneInput.length() < 7) {
            telefono.setError("Numero de Telefono muy CORTO");
            Toast.makeText(this, "Numero de Telefono muy CORTO", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            telefono.setError(null);
            return true;
        }
    }
    private boolean validateEmail() {
        String emailInput = correo.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_EMAIL);

        if (emailInput.isEmpty()) {
            correo.setError("Campo vacio");
            return false;
        }else if(!patron.matcher(emailInput).matches()){
            correo.setError("correo invalido");
            return false;
        }else {
            correo.setError(null);
            return true;
        }
    }

    //*********************************************** verifica interet ***********************************************
    //se verifica la conexiona a internet
    public void verifica_internet(){
        RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,"http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(Login.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            send_formulario();
                        }
                        else {
                            Toast.makeText(Formulario.this,"Su Formulario se ha guarda localmente",Toast.LENGTH_LONG).show();
                            save_local_formulario();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(Formulario.this, "ERROR EN LA CONEXION", Toast.LENGTH_LONG).show()) {
        };
        queue.add(stringRequest);
    }

    //si existe conexion a internet se envia el formulario al servidor
    public void send_formulario(){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        progreso=new ProgressDialog(Formulario.this);
        progreso.setMessage("Cargando...");
        progreso.show();
        RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_form.php",
                response -> {
                    progreso.hide();
                    if (response.trim().equalsIgnoreCase("registra")){
                        get_id_form();
                        Toast.makeText(Formulario.this,"se ha registrado con exito",Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(Formulario.this, "No se pudo registrar", Toast.LENGTH_LONG).show();
                    }
                }, error -> {Toast.makeText(Formulario.this, "Conexion invalida",
                Toast.LENGTH_LONG).show(); progreso.hide();}) {
            @Override
            protected Map<String, String> getParams() {
                String imagen=convertirImgString(bitmap_img);
                String firma=convertirImgString(bitmap_firma);

                Map<String, String> params = new HashMap<>();
                params.put("username",username.getEditText().getText().toString().trim());
                params.put("cellphone",telefono.getEditText().getText().toString().trim());
                params.put("correo",correo.getEditText().getText().toString().trim());
                params.put("imagen_c",imagen);
                params.put("firma",firma);
                params.put("nombre_img",name);
                params.put("fecha",fecha);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    public void get_id_form(){
        RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/get_id_form.php",
                response -> {
                    try {
                        // se obtine el ultimo id del formulario
                        JSONObject objResultado = new JSONObject(response);
                        save_eventsformu(objResultado.get("id").toString());
                        guardarGps(objResultado.get("id").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(Formulario.this, "ERROR EN LA CONEXION", Toast.LENGTH_LONG).show()) {
        };
        queue.add(stringRequest);
    }

    public void save_eventsformu(String idformulario){
        RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/guardar_eventoformu.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toast.makeText(Formulario.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Formulario.this, "evento update", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(Formulario.this, "CONEXION INVALIDA - event", Toast.LENGTH_LONG).show()){//var = "error") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idevento", idevento);
                params.put("idformulario", idformulario);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    public void guardarGps(String id_formu){
        RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/registrar_position.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            // var = "error";
                            Toast.makeText(Formulario.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Formulario.this, "Registrado con exito", Toast.LENGTH_LONG).show();
                            //se envia el id del usuario al mainactivity, para evitar la permitda de informacion
                            Intent intent = new Intent(Formulario.this, ListEvent.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(Formulario.this, "Invalid conexion - al registrar GPS", Toast.LENGTH_LONG).show()){//var = "error") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", id_user);
                params.put("id_formu", id_formu);
                params.put("lat", help_coordenadas.getLat());
                params.put("lon", help_coordenadas.getLon());
                return params;
            }
        };
        queue.add(stringRequest);
    }

    //se guarda el formulario de manera local
    public void save_local_formulario(){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        progreso=new ProgressDialog(Formulario.this);
        progreso.setMessage("Cargando...");
        progreso.show();
        BD.insertData(
                username.getEditText().getText().toString().trim(),
                telefono.getEditText().getText().toString().trim(),
                correo.getEditText().getText().toString().trim(),
                imageViewToByte(imagen),
                imageViewToByte(img_firma),
                name,
                fecha
        );

        BD.closeDB();
        progreso.hide();
        Toast.makeText(Formulario.this,"se ha registrado con exito",Toast.LENGTH_SHORT).show();
        //retorna al main activity
        Intent intent = new Intent(Formulario.this, ListEvent.class);
        startActivity(intent);
        finish();
    }

    private String convertirImgString(Bitmap bitmap) {
        ByteArrayOutputStream array=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,array);
        byte[] imagenByte=array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);
        return imagenString;
    }
    public byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    //*********************************************** Imagen ***********************************************
    private boolean validaPermisos() {

        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;
        }

        if((checkSelfPermission(CAMERA)== PackageManager.PERMISSION_GRANTED)&&
                (checkSelfPermission(WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED)){
            return true;
        }

        if((shouldShowRequestPermissionRationale(CAMERA)) ||
                (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
            cargarDialogoRecomendacion();
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==100){
            if(grantResults.length==2 && grantResults[0]== PackageManager.PERMISSION_GRANTED
                    && grantResults[1]==PackageManager.PERMISSION_GRANTED){
                botonCargar.setEnabled(true);
            }else{
                solicitarPermisosManual();
            }
        }

    }

    private void solicitarPermisosManual() {
        final CharSequence[] opciones={"si","no"};
        final AlertDialog.Builder alertOpciones=new AlertDialog.Builder(Formulario.this);
        alertOpciones.setTitle("¿Desea configurar los permisos de forma manual?");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("si")){
                    Intent intent=new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri=Uri.fromParts("package",getPackageName(),null);
                    intent.setData(uri);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Los permisos no fueron aceptados",Toast.LENGTH_SHORT).show();
                    dialogInterface.dismiss();
                }
            }
        });
        alertOpciones.show();
    }

    private void cargarDialogoRecomendacion() {
        AlertDialog.Builder dialogo=new AlertDialog.Builder(Formulario.this);
        dialogo.setTitle("Permisos Desactivados");
        dialogo.setMessage("Debe aceptar los permisos para el correcto funcionamiento de la App");

        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
            }
        });
        dialogo.show();
    }

    public void onclick(View view) {
        tomarFotografia();
    }
    private void tomarFotografia() {
        File fileImagen=new File(Environment.getExternalStorageDirectory(),RUTA_IMAGEN);
        boolean isCreada=fileImagen.exists();
        String nombreImagen="";
        if(!isCreada){
            isCreada=fileImagen.mkdirs();
        }

        if(isCreada){
            nombreImagen=(System.currentTimeMillis()/1000)+".png";
        }


        path_img=Environment.getExternalStorageDirectory()+
                File.separator+RUTA_IMAGEN+File.separator+nombreImagen;

        File imagen=new File(path_img);

        Intent intent=null;
        intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ////
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
        {
            String authorities=getApplicationContext().getPackageName()+".provider";
            Uri imageUri= FileProvider.getUriForFile(this,authorities,imagen);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        }else
        {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imagen));
        }
        startActivityForResult(intent,COD_FOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == COD_FOTO) {
                MediaScannerConnection.scanFile(this, new String[]{path_img}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("Ruta de almacenamiento", "Path: " + path);
                            }
                        });

                bitmap_img = BitmapFactory.decodeFile(path_img);
                imagen.setImageBitmap(bitmap_img);
                bitmap_img=redimensionarImagen(bitmap_img);
            }
        }
    }
    private Bitmap redimensionarImagen(Bitmap bitmap) {

        int ancho=bitmap.getWidth();
        int alto=bitmap.getHeight();

        if(ancho> (float) 600 || alto> (float) 800){
            float escalaAncho= (float) 600 /ancho;
            float escalaAlto= (float) 800 /alto;

            Matrix matrix=new Matrix();
            matrix.postScale(escalaAncho,escalaAlto);

            return Bitmap.createBitmap(bitmap,0,0,ancho,alto,matrix,false);

        }else{
            return bitmap;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.formulario));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver3);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver3,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(Formulario.this, Utils.getlatitud(location) + " /formu/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
                trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                help_coordenadas.setLat(Utils.getlatitud(location));
                help_coordenadas.setLon(Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectoria(id_user,lat, lon);
                        }
                        else {
                            Toast.makeText(Formulario.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,lat, lon);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon);
        }) {
        };
        queue.add(stringRequest);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toast.makeText(Formulario.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Formulario.this, "Guardando Trayectoria al Servidor - formulario", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(Formulario.this, "ERROR EN LA CONEXION - TRAYECTORIA_USUARIO", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        try {
            BD.insert_trayect(iduser, c1, c2, fecha);
            Toast.makeText(Formulario.this, "Guardando Trayectoria localmente - Formulario", Toast.LENGTH_LONG).show();
            BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}