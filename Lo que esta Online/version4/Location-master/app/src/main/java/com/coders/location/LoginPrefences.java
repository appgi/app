package com.coders.location;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginPrefences {
    Context context;

    public LoginPrefences(Context context) {
        this.context = context;
    }

    public void saveLoginDetails(String email,String password,String iduser,String estado,String android_id,String empr){
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id", iduser);
        editor.putString("Name", email);
        editor.putString("Password", password);
        editor.putString("Estado", estado);
        editor.putString("Android_id", android_id);
        editor.putString("Emp",empr);
        editor.apply();
    }
    public String Getid() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("id", "");
    }
    public String GetName() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Name", "");
    }

    public String Getpassword() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Password", "");
    }
    public String GetAndroid_id() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Android_id", "");
    }

    public boolean isUserLogedOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        boolean isEmailEmpty = sharedPreferences.getString("Name", "").isEmpty();
        boolean isPasswordEmpty = sharedPreferences.getString("Password", "").isEmpty();
        return isEmailEmpty || isPasswordEmpty;
    }
    public boolean isEmpty() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        boolean ValorEmpty = sharedPreferences.getString("id", "").isEmpty();
        return ValorEmpty;
    }

}
