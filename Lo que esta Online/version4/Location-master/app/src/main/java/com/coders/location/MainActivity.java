package com.coders.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.Services.GPS_Service;
import com.coders.location.Services.Utils;
import com.coders.location.eventos.ListEvent;
import com.coders.location.notificacion.ExampleService;
import com.coders.location.sql.BDAdapter;
import com.coders.location.sql.FormuList;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import androidx.appcompat.widget.Toolbar;
import org.json.JSONException;
import org.json.JSONObject;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


public class MainActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{


    private static final String PACKAGE_NAME = "com.coders.location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    help_coordenadas help_coordenadas;
    private boolean isGPS = false;
    String id_user;
    BDAdapter BD;
    ImageButton btn_verlist,btn_evento;

    //new capture gps ***********************************************************************************
    //services
    private static final String TAG = MainActivity.class.getSimpleName();

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    // A reference to the service used to get location updates.
    private GPS_Service mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    public static final String BroadcastStringForAction = "checkinternet";
    private IntentFilter nIntentFilter;
    BroadcastReceiver myReceiver_principal;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BD = new BDAdapter(this);
        BD.openDB();

        help_coordenadas = new help_coordenadas();


        //get idusuario mediante sharedpreferneces
        LoginPrefences p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
            Toast.makeText(this,"id"+p.Getid(),Toast.LENGTH_SHORT).show();
        }

        //**************************************************************************************
        myReceiver = new MyReceiver();
        //setContentView(R.layout.activity_main);
        //**************************************************************************************

        btn_verlist = findViewById(R.id.verlista);
        btn_evento = findViewById(R.id.btn_eventos);

        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 || tipoConexion2) {
                //********************************* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                 Toast.makeText(getApplicationContext(), "hay internet ", Toast.LENGTH_SHORT).show();
                 sendtrayec();
            }
        }
        else {
            //Toast.makeText(getApplicationContext(), "Favor encender el WIFI ó Datos Moviles", Toast.LENGTH_LONG).show();
            /* ******************************** No estas conectado a internet********************************  */
            Toast.makeText(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
        }

        btn_verlist.setOnClickListener(v -> {
            Intent intent4 = new Intent(MainActivity.this, FormuList.class);
            startActivity(intent4);
        });

        btn_evento.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ListEvent.class);
            startActivity(intent);
        });

        //si hay formularios pendientes se inicia la notificacion
        VerificaRegistroFormu();
    }

    public void verifica_gps(){
        new GpsUtils(this).turnGPSOn(isGPSEnable -> {
            // turn on GPS
            isGPS = isGPSEnable;
        });
    }

    public void verifica_internet(String c1, String c2){
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,"http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectoria(id_user,c1, c2);
                        }
                        else {
                            Toast.makeText(MainActivity.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,c1, c2);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                        guardartrayectorialocal(id_user,c1, c2);
            }) {
        };
        queue.add(stringRequest);
    }
    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2){

        help_coordenadas.setLat(c1);
        help_coordenadas.setLon(c2);

        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toast.makeText(MainActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            //var = "error2";
                            Toast.makeText(MainActivity.this, "Guardando Trayectoria al Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(MainActivity.this, "ERROR EN LA CONEXION - TRAYECTORIA_USUARIO", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                return params;
            }
        };
        queue.add(stringRequest);
    }
    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2){
        help_coordenadas.setLat(c1);
        help_coordenadas.setLon(c2);
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try{
            BD.insert_trayect(iduser,c1,c2,fecha);
            Toast.makeText(MainActivity.this, "Guardando Trayectoria localmente", Toast.LENGTH_LONG).show();
            BD.closeDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void sendtrayec(){
        Cursor cursor =  BD.getData("SELECT * FROM trayectoria");
        if(cursor.getCount() > 0){
            if (cursor.moveToFirst()) {
                do {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_user", cursor.getString(1));
                    params.put("lat", cursor.getString(2));
                    params.put("lon", cursor.getString(3));
                    params.put("fecha", cursor.getString(4));
                    processp(params);
                    BD.deletelisttrayectoria(cursor.getInt(0));
                } while (cursor.moveToNext());
            }
        }else{
            Toast.makeText(MainActivity.this, "Datos locales se enviaron satisfactoriamente", Toast.LENGTH_LONG).show();
        }
    }

    public void processp(final Map<String, String> parameters){
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toast.makeText(MainActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            //var = "error2";
                            Toast.makeText(MainActivity.this, "Subiendo Trayectoria al Servidor....", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(MainActivity.this, "ERROR EN LA CONEXION -> TRAYECTORIA_USUARIO ", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }
        };
        queue.add(stringRequest);
    }

    public void VerificaRegistroFormu(){
        Cursor cursor =  BD.getData("SELECT * FROM form");
        if(cursor.getCount() > 0){
            startService();
        }
    }

    //notificaciones
    public void startService() {
        String input = "Registros Pendientes a Enviar";
        Intent serviceIntent = new Intent(this, ExampleService.class);
        serviceIntent.putExtra("inputExtra", input);
        ContextCompat.startForegroundService(this, serviceIntent);
    }
    //***************************************************************************************************************
    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.app));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        nIntentFilter = new IntentFilter();
        nIntentFilter.addAction(BroadcastStringForAction);
        Intent serviceIntent = new Intent(this,Service_GPS.class);
        startService(serviceIntent);

        myReceiver_principal = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(Objects.equals(intent.getAction(), BroadcastStringForAction)){
                    if(intent.getStringExtra("verifica_gps").equals("true")){
                        if(intent.getStringExtra("verifica_permiso").equals("true")){
                            // Toast.makeText(getApplicationContext(), "Permisos activado", Toast.LENGTH_SHORT).show();
                            mService.requestLocationUpdates();
                        }else{
                            checkLocationPermission();
                        }
                    }else{
                        //verifica_gps();
                        Toast.makeText(getApplicationContext(), "xd", Toast.LENGTH_SHORT).show();
                        verifica_gps();//se verifica el status del gps
                    }
                }
            }
        };

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        registerReceiver(myReceiver_principal,nIntentFilter);
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
        registerReceiver(myReceiver_principal,nIntentFilter);
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mService.requestLocationUpdates();
            }  else {
                Toast.makeText(getApplicationContext(), "Se requieren Permisos", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Receiver for broadcasts sent by {@link GPS_Service}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(MainActivity.this, Utils.getlatitud(location) + " /main/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
                  verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
//                help_coordenadas.setLat(Utils.getlatitud(location));
//                help_coordenadas.setLon(Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    //*************************menu*************************
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent intent2 = new Intent(this, GPS_Service.class);
        intent2.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent2,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (id == R.id.logout){
            // Toast.makeText(this,"Logout",Toast.LENGTH_SHORT).show();
            servicePendingIntent.cancel();
            if (Logout()){
                mService.removeLocationUpdates();
                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivity(intent);
                this.finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean Logout(){
        SharedPreferences sp = this.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}