package com.coders.location;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import androidx.core.app.ActivityCompat;

public class Service_GPS  extends Service {
    LocationManager locationManager ;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intencion, int flags, int idArranque) {
        handler.post(timeupdate);

        return START_STICKY;
    }

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean CheckGpsStatus(Context c){
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    Handler handler = new Handler();
    private final Runnable timeupdate = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(timeupdate, 7000 - SystemClock.elapsedRealtime()%7000);
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(MainActivity.BroadcastStringForAction);
            broadcastIntent.putExtra("verifica_gps", ""+CheckGpsStatus(Service_GPS.this));
            broadcastIntent.putExtra("verifica_permiso", ""+checkPermission(Service_GPS.this));
            sendBroadcast(broadcastIntent);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;// throw new UnsupportedOperationException("not yet implement");//return null;
    }
}
