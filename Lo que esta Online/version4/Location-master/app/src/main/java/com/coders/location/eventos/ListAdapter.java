package com.coders.location.eventos;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.coders.location.Formulario;
import com.coders.location.R;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<ListElements> mData;
    private final LayoutInflater mInflater;
    private Context context;


    public ListAdapter(List<ListElements> itemList,Context context){
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mData = itemList;
        setItems(itemList);
    }
    @Override
    public int getItemCount(){
        return mData.size();
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = mInflater.inflate(R.layout.list_elements,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ListAdapter.ViewHolder holder,final int position){
        holder.binData(mData.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(
//                        context,
//                        holder.title + " id: "+ holder.id,
//                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context,Formulario.class);
                Bundle mibundle = new Bundle();
                mibundle.putString("idevento",holder.id);
                intent.putExtras(mibundle);
                context.startActivity(intent);
            }
        });
//        holder.Cv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(
//                        context,
//                        ,
//                        Toast.LENGTH_LONG).show();
//            }
//        });
    }
    public void setItems(List<ListElements> items){
        mData = items;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView Cv;
        LinearLayout linear;
        ImageView iconImage;
        TextView titulo,fecha,fecha2,estatus;
        String title = "",id = "";

        ViewHolder(View itemView){
            super(itemView);
            Cv =  itemView.findViewById(R.id.cv);
            linear = itemView.findViewById(R.id.bk);
            iconImage = itemView.findViewById(R.id.imgview);
            titulo = itemView.findViewById(R.id.txtTitulo);
            fecha = itemView.findViewById(R.id.txtDescripcion);
            fecha2 = itemView.findViewById(R.id.txtDescripcion2);
            estatus = itemView.findViewById(R.id.txt_estado);
        }

        void binData(final ListElements item){
            iconImage.setColorFilter(Color.parseColor(item.getcolor()), PorterDuff.Mode.SRC_IN);
            linear.setBackgroundColor(Color.parseColor(item.getcolor()));
            titulo.setText(item.getTitle());
            fecha.setText(item.getStart_event());
            fecha2.setText(item.getStart_event());
            estatus.setText(item.getEstado());
            title = item.getTitle();
            id = item.getId();
        }
    }

}
