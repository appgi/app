package com.coders.location.eventos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.Formulario;
import com.coders.location.LoginPrefences;
import com.coders.location.MainActivity;
import com.coders.location.R;
import com.coders.location.Ruta;
import com.coders.location.Service_GPS;
import com.coders.location.Services.GPS_Service;
import com.coders.location.Services.Utils;
import com.coders.location.sql.BDAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class ListEvent extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{

    BDAdapter BD;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver2;
    List<ListElements> elements;
    private Gson gson = new Gson();
    String id_user;

    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_event);

        BD = new BDAdapter(this);
        BD.openDB();

//        Bundle mibundle = this.getIntent().getExtras();
//        if(mibundle!=null){
//            id_user = mibundle.getString("user");
//        }

        //get idusuario mediante sharedpreferneces
        LoginPrefences p = new LoginPrefences(getApplicationContext());
        if (!p.isEmpty()){
            id_user = p.Getid();
            Toast.makeText(this,"id"+p.Getid(),Toast.LENGTH_SHORT).show();
        }

        //**************************************************************************************
        myReceiver2 = new ListEvent.MyReceiver();
        setContentView(R.layout.activity_list_event);
        //**************************************************************************************

        init();
    }

    public void init() {
        elements = new ArrayList<>();
//        Toast.makeText(
//                this,
//                "init",
//                Toast.LENGTH_LONG).show();
        RequestQueue queue = Volley.newRequestQueue(ListEvent.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "Planner.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        // Obtener atributo "estado"
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1": // EXITO
                                // Obtener array "metas" Json
                                JSONArray array = objResultado.getJSONArray("datos");
                                int i = 0;
                                while (i<array.length()){
                                    JSONObject object1=array.getJSONObject(i);
                                    String id =object1.getString("id");
                                    String title =object1.getString("title");
                                    String description =object1.getString("descripcion");
                                    String start_event =object1.getString("start");
                                    String end_event =object1.getString("end");
                                    String color =object1.getString("color");
                                    String text_color =object1.getString("text_color");
                                    String create_it =object1.getString("create_it");
                                    String create_by =object1.getString("create_by");
                                    String create_at =object1.getString("create_at");
                                    String update_by =object1.getString("update_by");
                                    String update_at =object1.getString("update_at");
                                    String detalles =object1.getString("detalles");
                                    String estado =object1.getString("estado");
                                    elements.add(new ListElements(id,title,description,start_event,end_event,color,text_color,create_it,create_by,create_at,update_by,update_at,detalles,estado));
                                    ListAdapter listAdapter = new ListAdapter(elements,this);
                                    RecyclerView recyclerView = findViewById(R.id.list_recycler);
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                                    recyclerView.setAdapter(listAdapter);
                                    i++;
                                }
                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(ListEvent.this, "ERROR EN LA CONEXION", Toast.LENGTH_LONG).show() ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("idusuario",id_user);
                    return params;
                }
        };
        queue.add(stringRequest);
    }
    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.eventos));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver2);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver2,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(ListEvent.this, Utils.getlatitud(location) + " /event/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
                trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        RequestQueue queue = Volley.newRequestQueue(ListEvent.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectoria(id_user,lat, lon);
                        }
                        else {
                            Toast.makeText(ListEvent.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,lat, lon);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon);
        }) {
        };
        queue.add(stringRequest);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        RequestQueue queue = Volley.newRequestQueue(ListEvent.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toast.makeText(ListEvent.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(ListEvent.this, "Guardando Trayectoria al Servidor - Eventos", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(ListEvent.this, "ERROR EN LA CONEXION - TRAYECTORIA_USUARIO", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            BD.insert_trayect(iduser, c1, c2, fecha);
            Toast.makeText(ListEvent.this, "Guardando Trayectoria localmente - Eventos", Toast.LENGTH_LONG).show();
            BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }
}