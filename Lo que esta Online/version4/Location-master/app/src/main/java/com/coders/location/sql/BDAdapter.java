package com.coders.location.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class BDAdapter {
    Context c;
    SQLiteDatabase db;
    BDHelper helper;

    public BDAdapter(Context c) {
        this.c = c;
        helper=new BDHelper(c);
    }

    //Abrir Base de Datos
    public void openDB()
    {
        try
        {
            db=helper.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //Cerrar Base de Datos
    public void closeDB()
    {
        try
        {
            helper.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //************************************************************* the table formulario*************************************************************
    public void insertData(String username, String cellphone, String correo, byte[] image_c, byte[] firma, String name, String fecha){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO form VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, username);
        statement.bindString(2, cellphone);
        statement.bindString(3, correo);
        statement.bindBlob(4, image_c);
        statement.bindBlob(5, firma);
        statement.bindString(6, name);
        statement.bindString(7, fecha);

        statement.executeInsert();
    }

    public void deleteData(int id) {
        db =  helper.getWritableDatabase();

        String sql = "DELETE FROM form WHERE id = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        db.close();
    }

    public Cursor getData(String sql){
        db =  helper.getReadableDatabase();
        return db.rawQuery(sql, null);
    }
    public void get_idformulario(prueba_var pb)
    {
        Cursor cursor=null;
        String sql = "SELECT MAX(id) FROM form";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIdform(cursor.getString(0));
            }while (cursor.moveToNext());
        }
    }


    public boolean verifica_numero(String telefono)
    {
        Cursor c=null;
        boolean existe = false;

        if(telefono.length()>0) {
            String sql = "SELECT * FROM " + Constans.TB_NAME + " WHERE " + Constans.USERNAME + " LIKE '%" + telefono + "%'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        return existe;
    }


    //************************************************************* the table user*************************************************************
    public void insert_tbuser(String id_user, String username, String estado){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO usuario VALUES (NULL,?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, id_user);
        statement.bindString(2, username);
        statement.bindString(3, estado);
        statement.executeInsert();
    }
    public void insert_tb_mov(String id_user, String android_id, String code,String id_mov){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO movilidad VALUES (NULL,?, ?, ?,?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, android_id);
        statement.bindString(2, code);
        statement.bindString(3, id_user);
        statement.bindString(4, id_mov);
        statement.executeInsert();
    }
    public void update_android_id( String android_id, String id_mov){
        int c = -1;
//        db =  helper.getWritableDatabase();
//        String sql = "UPDATE movilidad  SET android_id = ? where id_movilidad = ? ";
//
//        SQLiteStatement statement = db.compileStatement(sql);
//        statement.clearBindings();
//        statement.bindString(1, android_id);
//        statement.bindString(2, id_mov);
//        statement.executeUpdateDelete();
        ContentValues valores = new ContentValues();
        valores.put("android_id",android_id);

        String[] args = new String[]{id_mov};
        c = db.update("movilidad", valores, "id_movilidad = ?", args);

    }

    public boolean verifica_user(String username,String password)
    {
        Cursor c=null;
        boolean existe = false;

        if(username.length()>0 && password.length()>0 ) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
//            String sql = "SELECT * FROM usuario where username = '"+username+"' AND password = '"+password+"'"; //+ username + " AND password = " + password;
            String sql = "SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = '"+username+"' and movilidad.code = '"+password+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        return existe;
    }
    public void get_iduser_mobilidad_estado(prueba_var pb,String username,String password)
    {
        Cursor cursor=null;
//        String sql = "SELECT * FROM usuario where username = '"+username+"' AND password = '"+password+"'"; //+ username + " AND password = " + password;
        String sql = "SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = '"+username+"' and movilidad.code = '"+password+"'";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIduser_login(cursor.getString(1));
                pb.setMobilidad_login("1");
                pb.setEstado_login(cursor.getString(3));
            }while (cursor.moveToNext());
        }
    }

    public void get_iduser(prueba_var pb,String username,String password){
        Cursor cursor=null;
        String sql = "SELECT * FROM usuario where username = '"+username+"' AND password = '"+password+"'"; //+ username + " AND password = " + password;
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIduser_login(cursor.getString(1));
            }while (cursor.moveToNext());
        }
    }
    //trayec
    public void insert_trayect(String usuario, String lat, String lon,String fecha){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO trayectoria VALUES (NULL, ?, ?, ?,?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, usuario);
        statement.bindString(2, lat);
        statement.bindString(3, lon);
        statement.bindString(4, fecha);

        statement.executeInsert();
    }

    public void deletelisttrayectoria(int id) {
        db =  helper.getWritableDatabase();

        String sql = "DELETE FROM trayectoria WHERE id_trayectoria = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        db.close();
    }


}
