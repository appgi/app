package com.coders.location.sql;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDHelper extends SQLiteOpenHelper {

    public BDHelper(Context context) {
        super(context, Constans.DB_NAME, null, Constans.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(Constans.CREATE_TB);
            db.execSQL(Constans.CREATE_TB_USER);
            db.execSQL(Constans.CREATE_TB_POSITION);
            db.execSQL(Constans.CREATE_TB_TRAYECTORIA);
            db.execSQL(Constans.CREATE_TB_MOVILIDAD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_USER);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_POSITION);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_TRAYECTORIA);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_MOVILIDAD);
        onCreate(db);
    }
}