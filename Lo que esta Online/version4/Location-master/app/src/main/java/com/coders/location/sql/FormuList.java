package com.coders.location.sql;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.Formulario;
import com.coders.location.LoginPrefences;
import com.coders.location.MainActivity;
import com.coders.location.R;
import com.coders.location.Ruta;
import com.coders.location.Services.GPS_Service;
import com.coders.location.Services.Utils;
import com.coders.location.eventos.ListEvent;
import com.coders.location.help_coordenadas;
import com.coders.location.notificacion.ExampleService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FormuList extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{
    private MyReceiver myReceiver3;

    ListView gridView;
    ArrayList<formu> list;
    FormuListAdapter adapter = null;
    public static BDAdapter BD;
    String id_user;
    String var;
    ProgressDialog progreso;

    help_coordenadas help_coordenadas;

    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_registro);

        help_coordenadas = new help_coordenadas();

        //open database
        BD = new BDAdapter(this);
        BD.openDB();

        //**************************************************************************************
        myReceiver3 = new FormuList.MyReceiver();
        setContentView(R.layout.activity_list_registro);
        //**************************************************************************************

        //get idusuario mediante sharedpreferneces
        LoginPrefences p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
        }

        //Toast.makeText(FormuList.this,id_user + " / " + lati + " / " + longi,Toast.LENGTH_SHORT).show();

        gridView = findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new FormuListAdapter(this, R.layout.form_item, list);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener((adapterView, view, pos, l) -> {
            final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(FormuList.this);
            dialogDelete.setTitle("Alerta!!");
            dialogDelete.setMessage("Desea enviar este archivo?");
            dialogDelete.setPositiveButton("OK", (dialog, which) -> {
                try {
                    byte[] image, firma;
                    image = list.get(pos).getImage_c();
                    Bitmap bitmap_img = BitmapFactory.decodeByteArray(image, 0, image.length);
                    firma = list.get(pos).getFirma();
                    Bitmap bitmap_firma = BitmapFactory.decodeByteArray(firma, 0, firma.length);

                    progreso=new ProgressDialog(FormuList.this);
                    progreso.setMessage("Cargando...");
                    progreso.show();
                    RequestQueue queue = Volley.newRequestQueue(FormuList.this);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_form.php",
                            response -> {
                                if (response.trim().equalsIgnoreCase("registra")){
                                    get_id_form();
                                    Toast.makeText(FormuList.this,"Enviado satisfactoriamente",Toast.LENGTH_SHORT).show();
                                    BD.deleteData(list.get(pos).getId());
                                    updateList();
                                    BD.closeDB();
                                    progreso.hide();
                                }else{
                                    Toast.makeText(FormuList.this,"No se ha registrado",Toast.LENGTH_SHORT).show();
                                }
                            }, error -> {Toast.makeText(FormuList.this, "Conexion invalida",
                            Toast.LENGTH_LONG).show(); progreso.hide();
                            Intent intent = new Intent(FormuList.this, MainActivity.class);
                            intent.putExtra("valoruser", id_user);
                            startActivity(intent);
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            String imagen=convertirImgString(bitmap_img);
                            String firma=convertirImgString(bitmap_firma);

                            Map<String, String> params = new HashMap<>();
                            params.put("username",list.get(pos).getUsername());
                            params.put("cellphone",list.get(pos).getCellphone());
                            params.put("correo",list.get(pos).getCorreo());
                            params.put("imagen_c",imagen);
                            params.put("firma",firma);
                            params.put("nombre_img",list.get(pos).getName());
                            params.put("fecha",list.get(pos).getFecha());
                            return params;
                        }
                    };
                    queue.add(stringRequest);
                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }
            });

            dialogDelete.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            dialogDelete.show();
        });
        // get all data from sqlite
        Cursor cursor =  BD.getData("SELECT * FROM form");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String username = cursor.getString(1);
                String cellphone = cursor.getString(2);
                String correo = cursor.getString(3);
                byte[] image_c = cursor.getBlob(4);
                byte[] firma = cursor.getBlob(5);
                String name = cursor.getString(6);
                String fecha = cursor.getString(7);
                list.add(new formu(username,cellphone,correo,image_c,firma,name,fecha,id));
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.list));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }

    private String convertirImgString(Bitmap bitmap) {
        ByteArrayOutputStream array=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,array);
        byte[] imagenByte=array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);

        return imagenString;
    }
    private void updateList(){
        // get all data from sqlite
        Cursor cursor = BD.getData("SELECT * FROM form");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String username = cursor.getString(1);
                String cellphone = cursor.getString(2);
                String correo = cursor.getString(3);
                byte[] image_c = cursor.getBlob(4);
                byte[] firma = cursor.getBlob(5);
                String name = cursor.getString(6);
                String fecha = cursor.getString(7);
                list.add(new formu(username, cellphone, correo, image_c, firma, name, fecha,id));
            }
        }else{
            stopService();
        }
        adapter.notifyDataSetChanged();
    }

    public void get_id_form(){
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "get_id_form.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        guardarGps(objResultado.get("id").toString());
                        Intent intent = new Intent(FormuList.this, MainActivity.class);
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(FormuList.this, "ERROR EN LA CONEXION", Toast.LENGTH_LONG).show()) {
        };
        queue.add(stringRequest);
    }

    public void guardarGps(String idformu){
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "registrar_position.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            //var = "error";
                            Toast.makeText(FormuList.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(FormuList.this, "Guardado", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(FormuList.this, "Conexon invalida al enviar el Formulario", Toast.LENGTH_LONG).show()){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", id_user);
                params.put("id_formu", idformu);
                params.put("lat", help_coordenadas.getLat());
                params.put("lon", help_coordenadas.getLon());
                return params;
            }
        };
        queue.add(stringRequest);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver3);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver3,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(FormuList.this, Utils.getlatitud(location) + " /formu_list/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
                trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                help_coordenadas.setLat(Utils.getlatitud(location));
                help_coordenadas.setLon(Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectoria(id_user,lat, lon);
                        }
                        else {
                            Toast.makeText(FormuList.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,lat, lon);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon);
        }) {
        };
        queue.add(stringRequest);
    }
    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toast.makeText(FormuList.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(FormuList.this, "Guardando Trayectoria al Servidor - Lista formulario", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toast.makeText(FormuList.this, "ERROR EN LA CONEXION - TRAYECTORIA_USUARIO", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        try {
            BD.insert_trayect(iduser, c1, c2, fecha);
            Toast.makeText(FormuList.this, "Guardando Trayectoria localmente - Lista Formulario", Toast.LENGTH_LONG).show();
            BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
