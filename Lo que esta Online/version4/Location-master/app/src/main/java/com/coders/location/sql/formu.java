package com.coders.location.sql;

public class formu {
    private int id;
    private String username;
    private String cellphone;
    private String correo;
    private byte[] image_c;
    private byte[] firma;
    private String name;
    private String fecha;

    public formu(String username, String cellphone, String correo, byte[] image_c, byte[] firma, String name,String fecha,int id) {
        this.username = username;
        this.cellphone = cellphone;
        this.correo = correo;
        this.image_c = image_c;
        this.firma = firma;
        this.name = name;
        this.fecha = fecha;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public byte[] getImage_c() {
        return image_c;
    }

    public void setImage_c(byte[] image_c) {
        this.image_c = image_c;
    }

    public byte[] getFirma() {
        return firma;
    }

    public void setFirma(byte[] firma) {
        this.firma = firma;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}