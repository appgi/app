package com.coders.location.sql;

public class prueba_var {

    private String iduser_login;
    private String idform;
    private String mobilidad_login;
    private String estado_login;

    public String getIduser_login() {
        return iduser_login;
    }

    public void setIduser_login(String iduser_login) {
        this.iduser_login = iduser_login;
    }

    public String getIdform() {
        return idform;
    }

    public void setIdform(String idform) {
        this.idform = idform;
    }

    public String getMobilidad_login() {
        return mobilidad_login;
    }

    public void setMobilidad_login(String mobilidad_login) {
        this.mobilidad_login = mobilidad_login;
    }

    public String getEstado_login() {
        return estado_login;
    }

    public void setEstado_login(String estado_login) {
        this.estado_login = estado_login;
    }

}
