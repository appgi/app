<?php
require_once '../Conexion/Conexion.php';
    class app extends Conexion{
        private $username;
        private $password;
        private $id_user;
        private $id_formu;
        private $lat;
        private $lon;
        private $fecha;
        private $idevento;
        private $idformulario;
  
        private $cellphone;
        private $correo;
        private $imagen;
        private $firma;
        private $nombre_img;

        private $idemp;

        public function getIdemp() 
        { 
        return $this->idemp; 
        } 
        public function setIdemp($idemp) 
        { 
          $this->idemp = $idemp; return $this; 
        } 
        
        public function getCellphone() 
        { 
        return $this->cellphone; 
        } 
        public function setCellphone($cellphone) 
        { 
        $this->cellphone = $cellphone; return $this; 
        } 

        public function getCorreo() 
        { 
        return $this->correo; 
        } 
        public function setCorreo($correo) 
        { 
        $this->correo = $correo; return $this; 
        } 

        public function getImagen() 
        { 
        return $this->imagen; 
        } 
        public function setImagen($imagen) 
        { 
        $this->imagen = $imagen; return $this; 
        } 

        public function getFirma() 
        { 
        return $this->firma; 
        } 
        public function setFirma($firma) 
        { 
        $this->firma = $firma; return $this; 
        } 

        public function getNombre_img() 
        { 
        return $this->nombre_img; 
        } 
        public function setNombre_img($nombre_img) 
        { 
        $this->nombre_img = $nombre_img; return $this; 
        } 


        public function getidevento(){
          return $this->idevento;
        }
        public function setidevento($idevento){
          $this->idevento = $idevento;
          return $this;
        }

        public function getidformulario(){
          return $this->idformulario;
        }
        public function setidformulario($idformulario){
          $this->idformulario = $idformulario;
          return $this;
        }

        public function getfecha(){
          return $this->fecha;
        }
        public function setfecha($fecha){
          $this->fecha = $fecha;
          return $this;
        }
        public function getusername()
        {
            return $this->username;
        }
        
        public function setusername($username)
        {
            $this->username = $username;
            return $this;
        }
        public function getpassword()
        {
            return $this->password;
        }
        
        public function setpassword($password)
        {
            $this->password = $password;
            return $this;
        }
        public function getid_user()
        {
            return $this->id_user;
        }
        
        public function setid_user($id_user)
        {
            $this->id_user = $id_user;
            return $this;
        }

        public function getid_formu()
        {
            return $this->id_formu;
        }
        
        public function setid_formu($id_formu)
        {
            $this->id_formu = $id_formu;
            return $this;
        }

        public function getLat()
        {
            return $this->lat;
        }
        
        public function setLat($lat)
        {
            $this->lat = $lat;
            return $this;
        }
        public function getLon()
        {
            return $this->lon;
        }
        
        public function setLon($lon)
        {
            $this->lon = $lon;
        }
        
      public function verifica_user() {
          $user = $this->getusername();
          $pass = $this->getpassword();

          $sql = "SELECT * FROM usuario WHERE username = '" . $user . "' AND password = '" . $pass . "'";

          $sentencia = $this->dblink->prepare($sql);

          $sentencia->execute();
          $resultado = $sentencia->fetch();

          if ($resultado != FALSE){
              return TRUE;
          }else{
              return FALSE;
          }
      }

      public function login()
      {
          $user = $this->getusername();
          $pass = $this->getpassword();
          $sql = "SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = '".$user."' 
          and movilidad.code = '".$pass."'";
            $sentencia = $this->dblink->prepare($sql);

          $sentencia->execute();
          $count = $sentencia->rowCount();
          if ($count > 0) {
              foreach($sentencia as $row) {
                    $data["id_user"] = utf8_encode($row["id_user"]);
                    $data["username"] = utf8_encode($row["username"]);
                    $data["password"] = utf8_encode($row["password"]);
                    $data["id_movilidad"] = utf8_encode($row["id_movilidad"]);
                    $data["estado"] = utf8_encode($row["estado"]);
                    $data["android_id"] = utf8_encode($row["android_id"]);
                    $data["code"] = utf8_encode($row["code"]);      
					$data["idemp"] = utf8_encode($row["id_emp"]); 
              }
              $data["state"] = "Success";
          }
         else{
             $data['state'] ="No Encontrado"; 
         }
          
          
      
         return $data;
      }//getevent

        //se agrega la taryectoria del usuario
        public function agregar_trayectoriauser() {
              
              $id_user = $this->getid_user();
              $lat = $this->getLat();
              $lon = $this->getLon();
              $fecha = $this->getfecha();
              $idemp = $this->getIdemp();

              $sql = "INSERT INTO trayectoria(id_user,latitud,longitud,fecha,id_emp) 
              VALUES ('" . $id_user . "','" . $lat . "','" . $lon . "','" . $fecha . "', '" . $idemp . "')";

              $sentencia = $this->dblink->prepare($sql);
              $resultado = $sentencia->execute();

              if ($resultado != 1){
                  //ocurrio un error al insertar
                  return FALSE;
              }    
              //InsertĂ³ correctamente
              return TRUE;
          
        }

        //get fileds values mobilidad - estado -id_user => the table usuario
       public function get_mobilidad_estado_iduser(){
              $user = $this->getusername();
              $pass = $this->getpassword();

            //   $stmt = $this->dblink->prepare("SELECT * FROM usuario WHERE username = :username AND password = :password");
              $stmt = $this->dblink->prepare("SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = :username and movilidad.code = :password");

              $stmt->bindParam(":username", $user);
              $stmt->bindParam(":password", $pass);

              $stmt->execute();
              $stmt->bindColumn("id_user", $id_user);
              $stmt->bindColumn("username", $username);
              $stmt->bindColumn("estado", $estado);

              while ($fila = $stmt->fetch(PDO::FETCH_BOUND)){
                $respuesta = array();
                $respuesta["id_user"] = utf8_encode($id_user);
                $respuesta["username"] = utf8_encode($username);
                $respuesta["estado"] = utf8_encode($estado);
                $respuesta["code"] = utf8_encode($fila['code']);
                // $respuesta["username"] = utf8_encode($username);
                // $respuesta["estado"] = utf8_encode($estado);
                // $respuesta["id_user"] = utf8_encode($id_user);
                // $respuesta["username"] = utf8_encode($username);
                // $respuesta["estado"] = utf8_encode($estado);
              }
              return $respuesta;
        }

        public function verifica_internet(){
            $connected = @fsockopen("www.google.com", 80); //website, port  (try 80 or 443)
            $respuesta = array();
            if ($connected){
                 $respuesta["internet"] = utf8_encode("yes");
                 fclose($connected);
            }else{
                $respuesta["internet"] = utf8_encode("no");
            }
        return $respuesta;
      }

       public function insert_android_id($android_id,$id_mov)
      {
          $sql = "UPDATE `movilidad` SET `android_id`= :android WHERE id_movilidad = :ID";
          $sentencia = $this->dblink->prepare($sql);
          $sentencia->execute(array(":android" => $android_id, ":ID" => $id_mov));
          $count = $sentencia->rowCount();
          if ($count >0) {
              $data["state"] = "Success";
          }
          else{
            $data["state"] = "Wrong";
          }
          return $data;
      }

      public function Planner($idusuario){
        $data = [];
        $sql = "SELECT * FROM `events` where Id_usuario = :ID_USER AND estado != 'Cumplida'";
        $resultado = $this->dblink->prepare($sql);
        $resultado ->execute(array(":ID_USER" => $idusuario));
        $count = $resultado->rowCount();
        if ($count > 0) {
            foreach($resultado as $row) {
                $data[] = [
                    'id'              => utf8_encode($row["id"]),
                    'title'           => utf8_encode($row["title"]),
                    'descripcion'     => utf8_encode($row["descripcion"]),
                    'start'           => utf8_encode($row["start_event"]),
                    'end'             => utf8_encode($row["end_event"]),
                    'color'           => utf8_encode($row["color"]),
                    'text_color'      => utf8_encode($row["text_color"]),
                    'create_it'       => utf8_encode($row["create_it"]),
                    'create_by'       => utf8_encode($row["create_by"]),
                    'create_at'       => utf8_encode($row["create_at"]),
                    'update_by'       => utf8_encode($row["update_by"]),
                    'update_at'       => utf8_encode($row["update_at"]),
                    'detalles'        => utf8_encode($row["detalles"]),
                    'estado'          => utf8_encode($row["estado"]),
                ];
            }
        }
        if ($data) {
            $datos["state"] = 1;
            $datos["datos"] = $data;
    
            print json_encode($datos);
        } else {
            print json_encode(array(
                "estado" => 2,
                "mensaje" => "Ha ocurrido un error"
            ));
        }
        // return $data;
        }


        //se guarda el formulario
        public function guardar_formulario(){
              $username = $this->getusername();
              $cellphone = $this->getCellphone();
              $correo = $this->getCorreo();
              $imagen = $this->getImagen();
              $firma = $this->getFirma();
              $nombre_img = $this->getNombre_img();
              $fecha = $this->getfecha();

              $name_img = $username . "_" . $nombre_img;

              $path = "../webservices/img_cedula/$name_img.png";
              $url = "../webservices/img_cedula/".$name_img.".png";

              $path2 = "../webservices/img_firma/$name_img.png";
              $url2 = "../webservices/img_firma/".$name_img.".png";

              file_put_contents($path,base64_decode($imagen));
              $bytesArchivo=file_get_contents($path);

              file_put_contents($path2,base64_decode($firma));
              $bytesArchivo=file_get_contents($path2);

              $sql = "INSERT INTO formu(nombre,telefono,correo,imagen_c,firma,fecha) 
                VALUES ('" . $username . "', '" . $cellphone . "', '" . $correo . "', 
                '" . $url . "', '" . $url2 . "', '" . $fecha . "')";

              $sentencia = $this->dblink->prepare($sql);
              $resultado = $sentencia->execute();

              if ($resultado != 1){
                  //ocurrio un error al insertar
                  return FALSE;
              }    
              //InsertĂ³ correctamente
              return TRUE;
        }

        //se obtiene el ultimo id del formulario
        public function id_table_form(){
  
        $stmt = $this->dblink->prepare("SELECT MAX(id_formu) as id FROM formu");
        $stmt->execute();
        $stmt->bindColumn("id", $id_formu);
        while ($fila = $stmt->fetch(PDO::FETCH_BOUND)){
          $respuesta = array();
          $respuesta["id"] = utf8_encode($id_formu);
        }
        return $respuesta;
      }


      //se agrega la posicion
      public function agregar_position() {
                $stage = "Cumplida";
                $id_user = $this->getid_user();
                $id_formu = $this->getid_formu();
                $lat = $this->getLat();
                $lon = $this->getLon();
                $idevento = $this->getidevento();
                $idemp = $this->getIdemp();

                //se inserta en la tabla posicion
                $sql = "INSERT INTO posicion(id_user,id_formu,idevents,idemp,latitud,longitud) 
                VALUES ('" . $id_user . "','" . $id_formu . "','" . $idevento . "','" . $idemp . "','" . $lat . "', '" . $lon . "')";
                $sentencia = $this->dblink->prepare($sql);
                $resultado = $sentencia->execute();

                 //se actualiza el estado del evento en la tabla events
                $stmt = $this->dblink->prepare("UPDATE events SET estado = :stage where id = :idevento");
                $stmt->bindParam(":stage", $stage);
                $stmt->bindParam(":idevento", $idevento);
                $resultado2 = $stmt->execute();

                if ($resultado != 1){
                    //ocurrio un error al insertar
                    return FALSE;
                }    
                //Inserta correctamente
                return TRUE;   
        }
}
?>