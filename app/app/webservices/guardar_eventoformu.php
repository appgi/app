<?php
    if  ( ( !isset(  $_POST["idevento"]) ) || ( !isset(  $_POST["idformulario"]) ))
        {
            $respuesta = array(
                "estado"=>"error"
            );
            
            echo json_encode($respuesta);
            exit();
        }
        $idevento = $_POST["idevento"];
        $idformulario = $_POST["idformulario"];

        require_once '../datos/app.clase.php';
        $save_eventformu = new app();
        $save_eventformu->setidevento($idevento);
        $save_eventformu->setidformulario($idformulario);

        if ($save_eventformu->guardar_eventformu()==TRUE){
            $respuesta = array(
                "estado"=>"exito"
            );
        }else{
            $respuesta = array(
                "estado"=>"error",
                "datos"=>""
            );
        }
        echo json_encode($respuesta);

        // require_once '../datos/app.clase.php';
        // $veri = new app();
        // echo json_encode($veri->pb());
?>