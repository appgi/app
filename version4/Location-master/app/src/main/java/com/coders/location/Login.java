package com.coders.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;

import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.sql.BDAdapter;
import com.coders.location.sql.prueba_var;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

import static com.coders.location.Ruta.URL_WEB_SERVICE;

public class Login extends AppCompatActivity {
    public static final String REGEX_username = "^[a-zA-ZÀ-ÿ\\s]{1,40}$";
    public static int MILISEGUNDOS_ESPERA = 2500;
    TextInputLayout username, password;
    TextView mobilidad, state, txt_mod, txt_sta;
    Button btn_registrar;
    EditText campo1, campo2;
    ImageView img_logo;
    BDAdapter BD;
    static final Integer PHONESTATS = 0x1;
    String imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //base dedatos local - sqlite
        BD = new BDAdapter(this);
        BD.openDB();

        //base dedatos local - sharedpreferences
        if (!new LoginPrefences(this).isUserLogedOut()) {
            //user's email and password both are saved in preferences
            StartHome();
            Login.this.finish();
        }

        consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);

        username = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
        mobilidad = findViewById(R.id.text_modalidad);
        state = findViewById(R.id.text_estado);
        btn_registrar = findViewById(R.id.login);
        campo1 = findViewById(R.id.campo1);
        campo2 = findViewById(R.id.campo2);
        txt_mod = findViewById(R.id.text_modalidad2);
        txt_sta = findViewById(R.id.text_estado2);
        img_logo = findViewById(R.id.logo);

        //se agrego efecto al logo
        Animation traslacion = AnimationUtils.loadAnimation(this,R.anim.logo_traslacion);
        traslacion.setFillAfter(true);
        //scalacion.setFillBefore(true);
        traslacion.setRepeatMode(Animation.REVERSE);

        img_logo.startAnimation(traslacion);
        //********************************************************** verificacion de internet **********************************************************
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;
        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }
            if (tipoConexion1 || tipoConexion2) {
                //********************************* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                btn_registrar.setOnClickListener(v -> {
                    //se valida que los campos
                    if (!validateUsername() | !validatepassword()) {
                        return;
                    }
                    consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
                    verifica_internet();
                });
            }
        } else {
            //Toast.makeText(getApplicationContext(), "Favor encender el WIFI ó Datos Moviles", Toast.LENGTH_LONG).show();
            /* ******************************** No estas conectado a internet********************************  */
            // Toast.makeText(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
            btn_registrar.setOnClickListener(v -> {
                //se valida que los campos
                if (!validateUsername() | !validatepassword()) {
                    return;
                }
                send_local();
            });
        }
    }

    public void verifica_internet() {
        RequestQueue queue = Volley.newRequestQueue(Login.this); // # 1
        StringRequest stringRequest = new StringRequest(Request.Method.POST,"http://webser.nicaapps.com/app/webservices/verifica_internet.php",
//          StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_WEB_SERVICE+"verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if (objResultado.get("internet").toString().equals("yes")) {
//                       Toast.makeText(this, "esto es desde internet", Toast.LENGTH_LONG).show();
                            send_servidor();
                        } else {
                            Toasty.error(this, "No hay Acceso a Internet", Toast.LENGTH_LONG).show();
                            send_local();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Error en la Conexion - verifica internet", Toast.LENGTH_LONG).show()) {

        };

        queue.add(stringRequest);
    }

    private void send_local() {
        if (BD.verifica_user(username.getEditText().getText().toString().trim(), password.getEditText().getText().toString().trim())) {
            //Toast.makeText(getApplicationContext(), "existe", Toast.LENGTH_LONG).show();
            prueba_var pb = new prueba_var();
            BD.get_iduser_mobilidad_estado(pb, username.getEditText().getText().toString().trim(), password.getEditText().getText().toString().trim());
            view_text();
            mobilidad.setText(pb.getMobilidad_login());
            state.setText(pb.getEstado_login());

            valide(pb.getIduser_login(), username.getEditText().getText().toString().trim(), password.getEditText().getText().toString().trim(),
                    pb.getEstado_login(), "", "",pb.getIdemp());
        } else {
            Toasty.error(this, "Usuario No Valido", Toast.LENGTH_LONG).show();
        }
    }

    public void send_servidor() {
        consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
        obtenerIMEI();
//    Toast.makeText(Login.this, "response vacio", Toast.LENGTH_LONG).show();
        RequestQueue queue = Volley.newRequestQueue(Login.this);//# 2
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_login.php",
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_WEB_SERVICE  + "verifica_login.php",
                response -> {
                    try {
//                        Toast.makeText(Login.this, "response lleno ", Toast.LENGTH_LONG).show();
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("state").toString();
                        if (estadox == "No Encontrado") {
                            //limpiarcampos();
                            Toasty.error(this, "Usuario No Valido", Toast.LENGTH_LONG).show();
                            timewait(MILISEGUNDOS_ESPERA);
                        } else if (estadox == "ERROR") {
                            Toasty.error(this, "Faltan Datos Por Ingresar", Toast.LENGTH_LONG).show();
                        } else {
                            String id_user = objResultado.getString("id_user");
                            String username = objResultado.getString("username");
                            String id_mov = objResultado.getString("id_movilidad");
                            String code = objResultado.getString("code");
                            String estado = objResultado.getString("estado");
                            String android_id = objResultado.getString("android_id");
                            String emp = objResultado.getString("idemp");
                            //Toast.makeText(Login.this, "idempresa "+ emp, Toast.LENGTH_LONG).show();
                            view_text();
                            mobilidad.setText("1");
                            state.setText(objResultado.get("estado").toString());

                            if (android_id == null || android_id.equals("") ){
                                update_android_id(id_mov);
                                android_id = obtenerIMEI();
                                Toast.makeText(Login.this, "vacio", Toast.LENGTH_LONG).show();
                                valide(id_user, username, code, estado, android_id, id_mov,emp);
                            }
                            else{

                                String ime = obtenerIMEI();
                                if (!ime.equals(android_id)){
                                    Toasty.warning(this, "Tiene otra sesion activa, Contactese con su administrador", Toast.LENGTH_LONG).show();
                                    timewait(MILISEGUNDOS_ESPERA);
                                }else
                                {
                                    valide(id_user, username, code, estado, android_id, id_mov,emp);
                                }
                            }

                           // Toast.makeText(Login.this, "id user "+ imei, Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Erro en la Conexion - Login", Toast.LENGTH_LONG).show()) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", username.getEditText().getText().toString().trim());
                params.put("password", password.getEditText().getText().toString().trim());
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void valide(String id_user, String username, String code, String estado, String android_id, String id_mov,String emp) {
        if (BD.verifica_user(username, code)) {
//                Intent intent = new Intent(this, MainActivity.class);
//                //intent.putExtra("valor", id_user);
//                Bundle mibundle = new Bundle();
//                mibundle.putString("valor",id_user);
//                intent.putExtras(mibundle);
            saveLoginDetails(username, code, id_user, estado, android_id,emp);
            //                startActivity(intent);
            StartHome();
            Login.this.finish();

        } else {
            BD.insert_tbuser(id_user, username, estado,emp);
            BD.insert_tb_mov(id_user, android_id, code, id_mov);

            BD.update_android_id(android_id,id_mov);
            //****************************** se envia el idusuario al mainactivity ******************************
//                Intent intent = new Intent(this, MainActivity.class);
////                intent.putExtra("valor", id_user);
//                Bundle mibundle = new Bundle();
//                mibundle.putString("valor",id_user);
//                intent.putExtras(mibundle);
//                startActivity(intent);
            saveLoginDetails(username, code, id_user, estado, android_id,emp);
            StartHome();
            Login.this.finish();
        }

    }

    private void update_android_id(String id_mov) {
        String  ime = obtenerIMEI();
        RequestQueue queue = Volley.newRequestQueue(Login.this);//# 3
       StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/save_android_id.php",
       //StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_WEB_SERVICE + "save_android_id.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if (objResultado.get("state").toString().equals("Success")) {
                            Toasty.success(this, "Actualizado con Exito", Toast.LENGTH_LONG).show();
//                            send_servidor();
                        } else {
                            Toasty.info(this, "No se Actualizo" + id_mov, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show()) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", id_mov);
                params.put("android_id", ime);
                return params;
            }
        };
        queue.add(stringRequest);
    }


    //    validacion de campos
    private boolean validateUsername() {
        String usernameInput = username.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_username);

        if (usernameInput.isEmpty()) {
            username.setError("Campo vacio");
            return false;
        } else if (!patron.matcher(usernameInput).matches()) {
            username.setError("Nombre invalido");
            return false;
        } else {
            username.setError(null);
            return true;
        }
    }

    private boolean validatepassword() {
        String passinput = password.getEditText().getText().toString().trim();
        if (passinput.isEmpty()) {
            password.setError("Campo vacio");
            return false;
        } else if (passinput.length() < 2) {
            password.setError("Contraseña muy corto");
            return false;
        } else {
            password.setError(null);
            return true;
        }
    }
//    fin validacion de campos
    private void StartHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void limpiarcampos() {
        campo1.requestFocus();
        campo1.setText("");
        campo2.setText("");
        mobilidad.setText("");
        state.setText("");
        txt_mod.setVisibility(View.INVISIBLE);
        txt_sta.setVisibility(View.INVISIBLE);
    }

    public void view_text() {
        txt_mod.setVisibility(View.VISIBLE);
        txt_sta.setVisibility(View.VISIBLE);
    }

    public void timewait(int milisegundos) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // acciones que se ejecutan tras los milisegundos
                limpiarcampos();
            }
        }, milisegundos);
    }

    private void saveLoginDetails(String username, String code, String idUser, String estado, String android_id,String emp) {
        new LoginPrefences(this).saveLoginDetails(username, code, idUser, estado, android_id,emp);
    }

    private void consultarPermiso(String permission, Integer requestCode) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(Login.this, permission)) {
                ActivityCompat.requestPermissions(Login.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(Login.this, new String[]{permission}, requestCode);
            }
        } else {
            imei = obtenerIMEI();
//            Toast.makeText(this, permission + " El permiso a la aplicación esta concedido.", Toast.LENGTH_SHORT).show();
        }
    }
        @SuppressLint("HardwareIds")
    private String obtenerIMEI() {
        final TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Hacemos la validación de métodos, ya que el método getDeviceId() ya no se admite para android Oreo en adelante, debemos usar el método getImei()
//            return telephonyManager.getImei();
            String myIMEI = Settings.Secure.getString(Login.this.getContentResolver(), Settings.Secure.ANDROID_ID);
            return myIMEI;

        } else {
            consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(Login.this, Manifest.permission.READ_PHONE_STATE)) {
                    ActivityCompat.requestPermissions(Login.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PHONESTATS);
                } else {
                    ActivityCompat.requestPermissions(Login.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PHONESTATS);
                }
            }
            return telephonyManager.getDeviceId();
        }
    }

}

