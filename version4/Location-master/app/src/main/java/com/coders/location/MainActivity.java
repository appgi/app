package com.coders.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.Services.GPS_Service;
import com.coders.location.Services.Utils;
import com.coders.location.eventos.ListEvent;
import com.coders.location.notificacion.ExampleService;
import com.coders.location.sql.BDAdapter;
import com.coders.location.sql.FormuList;
import com.coders.location.sql.prueba_var;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import androidx.appcompat.widget.Toolbar;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.core.content.res.ResourcesCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.ByteArrayOutputStream;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;


public class MainActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{


    private static final String PACKAGE_NAME = "com.coders.location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    help_coordenadas help_coordenadas;
    private boolean isGPS = false;
    String id_user,nombre_user;
    BDAdapter BD;
    ImageButton btn_verlist,btn_evento;
    //CircleImageView image_url;
    ImageView image_url;
    TextView tvnombreuser,tvlista,tvevento,ttnombreuser;
    String var;
    private Menu menu2;

    //new capture gps ***********************************************************************************
    //services
    private static final String TAG = MainActivity.class.getSimpleName();

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    // A reference to the service used to get location updates.
    private GPS_Service mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    public static final String BroadcastStringForAction = "checkinternet";
    private IntentFilter nIntentFilter;
    BroadcastReceiver myReceiver_principal;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        BD = new BDAdapter(this);
        BD.openDB();

        help_coordenadas = new help_coordenadas();

        //get idusuario mediante sharedpreferneces
        LoginPrefences p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
            nombre_user = p.GetName();
//              Toast.makeText(this,"id- > " +p.Getid(),Toast.LENGTH_LONG).show();
        }


       // String emp = p.GetEmp();
//        Toast.makeText(this,"idempresa- > " + emp,Toast.LENGTH_LONG).show();

        //**************************************************************************************
        myReceiver = new MyReceiver();
        //setContentView(R.layout.activity_main);
        //**************************************************************************************

        btn_verlist = findViewById(R.id.verlista);
        btn_evento = findViewById(R.id.btn_eventos);
        tvnombreuser = findViewById(R.id.nombre_user);
        ttnombreuser = findViewById(R.id.ttnombreuser);
        tvlista = findViewById(R.id.txteventos);
        tvevento = findViewById(R.id.txtverlista);
        image_url = findViewById(R.id.imagen_url);


        //se cambia de fuentes fonts_franklin-light.ttf
        Typeface face=Typeface.createFromAsset(getAssets(),"fuentes/fonts_franklin_Bold.ttf");
        Typeface face2=Typeface.createFromAsset(getAssets(),"fuentes/fonts_franklin-light.ttf");

        ttnombreuser.setTypeface(face);
        tvnombreuser.setTypeface(face2);
//        tvlista.setTypeface(face);
//        tvevento.setTypeface(face);
        tvnombreuser.setText(nombre_user);

        
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 || tipoConexion2) {
                //********************************* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                 //Toast.makeText(getApplicationContext(), "hay internet ", Toast.LENGTH_SHORT).show();
                get_url_photo();
                verifica_internet_for_sendtrayec();
            }
        }
        else {
            //Toast.makeText(getApplicationContext(), "Favor encender el WIFI ó Datos Moviles", Toast.LENGTH_LONG).show();
            /* ******************************** No estas conectado a internet********************************  */
            //Toast.makeText(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
            Toasty.info(this, "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
            cargar_imguser_local();
        }

        btn_verlist.setOnClickListener(v -> {
            Intent intent4 = new Intent(MainActivity.this, FormuList.class);
            startActivity(intent4);
        });

        btn_evento.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ListEvent.class);
            startActivity(intent);
        });

        //si hay formularios pendientes se inicia la notificacion
        VerificaRegistroFormu();
    }

    //se verifica si el usuario tiene foto
    public void get_url_photo(){
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
       // StringRequest stringRequest = new StringRequest(Request.Method.POST,  Ruta.URL_WEB_SERVICE + "get_foto.php",
         StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/get_foto.php",
                response -> {
                    try {
                        //se define la ruta donde estan guardada las img
                        String ruta = "https://panel.nicaapps.com/img/users_client/";
                        //String ruta = "http://192.168.6.237/app2/webservices/img_cedula/";

                        //se obtiene el nombre de las img
                        JSONObject objResultado = new JSONObject(response);
                        String nombre_img_user = objResultado.get("imagen_Img").toString();

                        //url = url.replace(" ", "%20");
                        //si el campo del usuario tiene foto
                        if(!nombre_img_user.equals(" ")){
                            //se concatena y se busca en el dominio
                            String url = ruta + nombre_img_user;
                            //si existen espacios en blancos se subtitute
                            url = url.replace(" ", "%20");
                            //si la url no existe, entonces que la guarde localmente, de esta menera
                            //se valida si la imagen ya existe
                            if(!BD.verifica_url_user(url)){
                                image(url);
                                BD.update_url_user(url,id_user);
                            }else{
                                //si la url ya existe, entonces que se cargue la imagen de manera local
                                cargar_imguser_local();
                            }
                        }else{//si el usuario no tiene foto, entoces se le asigna otra x default de manera local
                            Bitmap img = BitmapFactory.decodeResource(getResources(), R.drawable.usuario);
                            image_url.setImageBitmap(img);
                            BD.update_img_user(imageViewToByte(image_url), id_user);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }// var = "error"
                }, error ->{ var = "error";//Toast.makeText(MainActivity.this,"sin foto el usuario",Toast.LENGTH_SHORT).show();

                 }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario", id_user);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void image(String url)
    {
        RequestQueue queue2 = Volley.newRequestQueue(MainActivity.this);
        ImageRequest imageRequest =new ImageRequest(url,
                response1 -> {
                    image_url.setImageBitmap(response1);
                    BD.update_img_user(imageViewToByte(image_url), id_user);
                },0,0, ImageView.ScaleType.CENTER_CROP,null,
                error -> {
                   // Toast.makeText(MainActivity.this,"Ha ocurrido un error",Toast.LENGTH_SHORT).show();
                    Toasty.error(this, "Ha ocurrido un error en establecer la imagen", Toast.LENGTH_LONG).show();
                   // image_url.setImageResource(R.drawable.ic_usuario);
                    //BD.update_img_user(imageViewToByte(image_url), id_user);
                    error.printStackTrace();
                });
        queue2.add(imageRequest);
    }
    public void cargar_imguser_local()
    {
        prueba_var pb = new prueba_var();
        BD.get_img_user(pb,id_user);

        byte[] imagen_user = pb.getImg_user();
        Bitmap bitmap = BitmapFactory.decodeByteArray(imagen_user, 0, imagen_user.length);
        image_url.setImageBitmap(bitmap);
    }

    public byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public void verifica_gps(){
        new GpsUtils(this).turnGPSOn(isGPSEnable -> {
            // turn on GPS
            isGPS = isGPSEnable;
        });
    }

    public void verifica_internet(String c1, String c2){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();

        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
//          StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //get_url_photo();
                            guardartrayectoria(id_user,c1, c2,emp);
                        }
                        else {
                            //Toast.makeText(MainActivity.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //cargar_imguser_local();
                            guardartrayectorialocal(id_user,c1, c2,emp);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                        //cargar_imguser_local();
                        guardartrayectorialocal(id_user,c1, c2,emp);
                }) {
        };
        queue.add(stringRequest);
    }
    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2, String emp){
        help_coordenadas.setLat(c1);
        help_coordenadas.setLon(c2);

        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            //Toast.makeText(MainActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            //var = "error2";
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);// comentariar cuando se envie al servidor
                params.put("idemp",emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2, String emp)
    {
        help_coordenadas.setLat(c1);
        help_coordenadas.setLon(c2);
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try{
            BD.insert_trayect(iduser,c1,c2,fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            BD.closeDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //si hay internet, los datos locales se envian al servidor
    public void verifica_internet_for_sendtrayec(){
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
//          StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            sendtrayec();
                        }
                        else {
                            //Toast.makeText(MainActivity.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            Toasty.info(this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            Toasty.error(this, "Error en la Conexion", Toast.LENGTH_LONG).show();
        }) {
        };
        queue.add(stringRequest);
    }
    //si hay trayectoria local, entonces se envia al servidor
    public void sendtrayec(){
        Cursor cursor =  BD.getData("SELECT * FROM trayectoria");
        if(cursor.getCount() > 0){
            if (cursor.moveToFirst()) {
                do {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_user", cursor.getString(1));
                    params.put("lat", cursor.getString(2));
                    params.put("lon", cursor.getString(3));
                    params.put("fecha", cursor.getString(4));
                    params.put("idemp", cursor.getString(5));
                    processp(params);
                    BD.deletelisttrayectoria(cursor.getInt(0));
                } while (cursor.moveToNext());
            }
        }
//        else{
//            Toast.makeText(MainActivity.this, "Datos locales se enviaron satisfactoriamente", Toast.LENGTH_LONG).show();
//        }
    }

    public void processp(final Map<String, String> parameters){
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
//                            Toast.makeText(MainActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                                Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            //var = "error2";
                            Toasty.success(this, "Subiendo Trayectoria al Servidor....", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }
        };
        queue.add(stringRequest);
    }

    public void VerificaRegistroFormu(){
        Cursor cursor =  BD.getData("SELECT * FROM form where idusuario = '"+id_user+"'");
        if(cursor.getCount() > 0){
            startService();
        }
    }

    //notificaciones
    public void startService() {
        String input = "Formularios Pendientes";
        Intent serviceIntent = new Intent(this, ExampleService.class);
        serviceIntent.putExtra("inputExtra", input);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }
    //***************************************************************************************************************
    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.menuprincipal));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        nIntentFilter = new IntentFilter();
        nIntentFilter.addAction(BroadcastStringForAction);
        Intent serviceIntent1 = new Intent(this,Service_GPS.class);
        startService(serviceIntent1);

        myReceiver_principal = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(Objects.equals(intent.getAction(), BroadcastStringForAction)){
                    if(intent.getStringExtra("verifica_gps").equals("true")){
                        if(intent.getStringExtra("verifica_permiso").equals("true")){
                            // Toast.makeText(getApplicationContext(), "Permisos activado", Toast.LENGTH_SHORT).show();
                            mService.requestLocationUpdates();
                            //semana(intent.getStringExtra("verifica_hora").equals("true"),intent.getStringExtra("verifica_hora_sab").equals("true"));
                        }else{
                            checkLocationPermission();
                        }
                    }else{
                        verifica_gps();//se verifica el status del gps
                    }
                }
            }
        };
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    public void semana(boolean status_h,boolean status_h_sab){
        //dia de la semana
        Calendar now2 = Calendar.getInstance();
        int numD = now2.get(Calendar.DAY_OF_WEEK);

        String hora_actual = Get_Hours.getHora("hh:mm a");
       // Toast.makeText(getApplicationContext(), hora_actual, Toast.LENGTH_SHORT).show();
        String hora_inicial = "07:15 a. m.";
        String hora_final2 = "09:01 a. m.";

        if(numD == Calendar.SUNDAY) {
            statusitem(true);
        }
        else if(numD == Calendar.MONDAY) {
            if(status_h) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
            }else{
                statusitem(true);
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();

//                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
//                   // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                    if (Logout()){
//                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        unregisterReceiver(myReceiver_principal);
//                        Intent intent = new Intent(MainActivity.this,Login.class);
//                        startActivity(intent);
//                        stopService();
//                        mService.removeLocationUpdates();
//                        this.finish();
//                    }
//                }
//                else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.TUESDAY) {
            if(status_h) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
            }
            else{
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                statusitem(true);
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
//                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                    if (Logout()){
//                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        unregisterReceiver(myReceiver_principal);
//                        Intent intent = new Intent(MainActivity.this,Login.class);
//                        startActivity(intent);
//                        stopService();
//                        mService.removeLocationUpdates();
//                        this.finish();
//                    }
//                }else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.WEDNESDAY) {
            if(status_h) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
            }else{
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                statusitem(true);
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
//                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                    if (Logout()){
//                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        unregisterReceiver(myReceiver_principal);
//                        Intent intent = new Intent(MainActivity.this,Login.class);
//                        startActivity(intent);
//                        stopService();
//                        mService.removeLocationUpdates();
//                        this.finish();
//                    }
//                }else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.THURSDAY) {
            if(status_h) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
            }else{
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                statusitem(true);
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
//                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                    if (Logout()){
//                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        unregisterReceiver(myReceiver_principal);
//                        Intent intent = new Intent(MainActivity.this,Login.class);
//                        startActivity(intent);
//                        stopService();
//                        mService.removeLocationUpdates();
//                        this.finish();
//                    }
//                }else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.FRIDAY) {
            if(status_h) {
               // Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
            }else{
//                Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                statusitem(true);
                statusitem(true);
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
//                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                    if (Logout()){
//                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        unregisterReceiver(myReceiver_principal);
//                        Intent intent = new Intent(MainActivity.this,Login.class);
//                        startActivity(intent);
//                        stopService();
//                        mService.removeLocationUpdates();
//                        this.finish();
//                    }
//                }else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.SATURDAY) {
            if(status_h_sab) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
            }else{
                statusitem(true);
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
//                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                    if (Logout()){
//                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
//                        unregisterReceiver(myReceiver_principal);
//                        Intent intent = new Intent(MainActivity.this,Login.class);
//                        startActivity(intent);
//                        stopService();
//                        mService.removeLocationUpdates();
//                        this.finish();
//                    }
//                }else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        registerReceiver(myReceiver_principal,nIntentFilter);
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
        registerReceiver(myReceiver_principal,nIntentFilter);
    }
    @Override
    protected void onStop() {
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mService.requestLocationUpdates();
            }
//            else {
//                Toast.makeText(getApplicationContext(), "Se requieren Permisos", Toast.LENGTH_SHORT).show();
//            }
        }
    }

    private void save_cood(String lat, String lon){
        new LoginPrefences(this).save_cood(lat, lon);
    }

    /**
     * Receiver for broadcasts sent by {@link GPS_Service}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                //dia de la semana
//                Calendar now2 = Calendar.getInstance();
//                int numD = now2.get(Calendar.DAY_OF_WEEK);
//                //Toast.makeText(getApplicationContext(), now2.getTime().toString(), Toast.LENGTH_LONG).show();
//
//                //String dia = "";
                verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                save_cood(Utils.getlatitud(location), Utils.getlongitud(location));

//
//                if(numD == Calendar.SUNDAY) {
//                   // dia = "DOMINGO";
//                    statusitem(true);
//                  // horario_trabajo();
//                }
//                else if(numD == Calendar.MONDAY) {
//                   // dia = "LUNES";
//                    horario_trabajo();
//                }
//                else if(numD == Calendar.TUESDAY) {
//                    //dia = "MARTES";
//                    horario_trabajo();
//                }
//                else if(numD == Calendar.WEDNESDAY) {
//                    //dia = "MIERCOLES";
//                    horario_trabajo();
//                }
//                else if(numD == Calendar.THURSDAY) {
//                   // dia = "JUEVES";
//                    horario_trabajo();
//                }
//                else if(numD == Calendar.FRIDAY) {
//                    //dia = "VIERNES";
//                    horario_trabajo();
//                }
//                else if(numD == Calendar.SATURDAY) {
//                   // dia = "SABADO";
//                    horario_trabajo_sab();
//                }
               // Toast.makeText(getApplicationContext(), dia, Toast.LENGTH_LONG).show();

            }
        }
    }
    //*******************************************************************************************************
    //horarios de Lunes a Viernes
    public void horario_trabajo(){
//        h se utiliza para las horas AM / PM (1-12).
//        H se utiliza durante 24 horas (1-24).
//        a es el marcador AM / PM
        //se establece la hora
        String hora_actual = Get_Hours.getHora("hh:mm a");
        String hora_inicial = "10:06 a. m.";// 7:30 am
        String hora_final   = "10:07 a. m.";// 6:00 pm
        String hora_final2 = "10:01 a. m.";
        //Toast.makeText(getApplicationContext(),Get_Hours.getHora("hh:mm a"),Toast.LENGTH_LONG).show();

        System.out.println("Hora:  " + Get_Hours.getHora("hh:mm a"));
        if(isHourInInterval(hora_actual,hora_inicial,hora_final)){
            //System.out.println("\n\n RANGO \n\n");
            Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
            statusitem(false);
        }else {
            Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
            // System.out.println("\n\n FUERA \n\n");
            statusitem(true);

//            if(isHourInInterval2(hora_actual,hora_final2)){
//                //Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                if (Logout()){
//                    Toast.makeText(getApplicationContext(), "Cession Cerrada", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(MainActivity.this,Login.class);
//                    startActivity(intent);
//                    stopService();
//                    mService.removeLocationUpdates();
//                    this.finish();
//                }
//            }
        }
    }
    public void horario_trabajo_sab(){
        //se establece la hora para el dia sabado
        String hora_actual = Get_Hours.getHora("hh:mm a");
        String hora_inicial = "08:44 am";//7:30 am
        String hora_final   = "08:45 am"; // 1:00 pm
        if(isHourInInterval(hora_actual,hora_inicial,hora_final)){
            //System.out.println("\n\n RANGO \n\n");
            Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
            statusitem(false);
        }
        else {
            Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
            // System.out.println("\n\n FUERA \n\n");
            statusitem(true);
        }
    }
    //se compara la hora actual con la hora inicio y final
    public static boolean isHourInInterval(String target, String start, String end) {
        return ((target.compareTo(start) >= 0)&& (target.compareTo(end) <= 0));
    }

    public static boolean isHourInInterval2(String target, String end) {
        return ((target.compareTo(end) >= 0));
    }

    public static boolean isHourInInterval3(String target, String start) {
        return ((target.compareTo(start) <= 0));
    }

    public void statusitem(boolean status) {
        if (menu2 != null) {
            menu2.findItem(R.id.logout).setVisible(status);
        }
    }

    //*******************************************************************************************************
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    //*************************menu*************************
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        this.menu2 = menu;
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent intent2 = new Intent(this, GPS_Service.class);
        intent2.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, false);

        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent2,
                PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        if (id == R.id.logout)
        {
            if (Logout()){
                Toasty.error(this, "Sesión Cerrada", Toast.LENGTH_LONG).show();
                unregisterReceiver(myReceiver_principal);
                servicePendingIntent.cancel();
                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivity(intent);
                stopService();
                mService.removeLocationUpdates();
                this.finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean Logout(){
        SharedPreferences sp = this.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}