package com.coders.location;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import androidx.core.app.ActivityCompat;

public class Service_GPS  extends Service {
    LocationManager locationManager ;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intencion, int flags, int idArranque) {
        handler.post(timeupdate);

        return START_NOT_STICKY;
    }

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean CheckGpsStatus(Context c){
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    //****************************************************************************
    public boolean horario_trabajo(Context c){
        String hora_actual = Get_Hours.getHora("hh:mm a");
        String hora_inicial = "07:30 a. m.";// 7:30 am
        String hora_final   = "10:00 a. m.";// 6:00 pm
        return isHourInInterval(hora_actual, hora_inicial, hora_final);
    }

    public boolean horario_trabajo_sab(Context c){
        String hora_actual = Get_Hours.getHora("hh:mm a");
        String hora_inicial = "7:30 a. m.";//7:30 am
        String hora_final   = "01:00 p. m."; // 1:00 pm
        return isHourInInterval(hora_actual, hora_inicial, hora_final);
    }
    public static boolean isHourInInterval(String target, String start, String end) {
        return ((target.compareTo(start) >= 0)&& (target.compareTo(end) <= 0));
    }
    //****************************************************************************
    Handler handler = new Handler();
    private final Runnable timeupdate = new Runnable() {
        @Override
        public void run() {

            handler.postDelayed(timeupdate, 5000 - SystemClock.elapsedRealtime()%5000);
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(MainActivity.BroadcastStringForAction);
            broadcastIntent.putExtra("verifica_gps", ""+CheckGpsStatus(Service_GPS.this));
            broadcastIntent.putExtra("verifica_permiso", ""+checkPermission(Service_GPS.this));
            broadcastIntent.putExtra("verifica_hora", ""+horario_trabajo(Service_GPS.this));
            broadcastIntent.putExtra("verifica_hora_sab", ""+horario_trabajo_sab(Service_GPS.this));
            sendBroadcast(broadcastIntent);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;// throw new UnsupportedOperationException("not yet implement");//return null;
    }
}
