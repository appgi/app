package com.coders.location;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.Services.GPS_Service;
import com.coders.location.Services.Utils;
import com.coders.location.eventos.ListAdapter;
import com.coders.location.eventos.ListElements;
import com.coders.location.eventos.ListEvent;
import com.coders.location.sql.BDAdapter;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class Ver_evento extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{

    TextView titulo,descripcion,fecha_inicio,fecha_final,detalle,estado;
    String idevento, id_user;

    public static BDAdapter BD;

    private MyReceiver myReceiver2;
    private Gson gson = new Gson();

    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_evento);

        BD = new BDAdapter(this);
        BD.openDB();

        //se obtiene el id del evento
        Bundle mibundle = this.getIntent().getExtras();
        if(mibundle!=null){
            idevento = mibundle.getString("idevento");
        }

        LoginPrefences p = new LoginPrefences(getApplicationContext());
        if (!p.isEmpty()){
            id_user = p.Getid();
        }

        //**************************************************************************************
        myReceiver2 = new MyReceiver();
        //**************************************************************************************

        titulo = findViewById(R.id.txt_tituloevento);
        descripcion = findViewById(R.id.txt_descripcionevento);
        fecha_inicio = findViewById(R.id.txt_fechastar);
        fecha_final = findViewById(R.id.txt_fechaend);
        detalle = findViewById(R.id.txt_detalleevento);
        estado = findViewById(R.id.txt_estadoevento);

        //ver_evento();
        ver_info_evento();
//        titulo_.setText("xd");
//        estado_.setText("xd // ");
    }

    public void ver_info_evento(){
        help_ver_evento pb = new help_ver_evento();
        BD.ver_info_evento(pb, idevento);

        titulo.setText(pb.getTitle());
        descripcion.setText(pb.getDescripcion());
        fecha_inicio.setText("- " + pb.getStart_event());
        fecha_final.setText("- " +pb.getEnd_event());
        if(pb.getDetalles() == null || pb.getDetalles().isEmpty())  detalle.setText("No Posee Detalles");
        else  detalle.setText(pb.getDetalles());
        estado.setText(pb.getEstado());
    }

    public void ver_evento(){
        Cursor cursor =  BD.getData("SELECT * FROM evento where id = '"+idevento+"'");
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                //int id = cursor.getInt(0);
//                String title = cursor.getString(3);
//                String description = cursor.getString(4);
//                String start_event = cursor.getString(5);
//                String end_event = cursor.getString(6);
//                String detalles = cursor.getString(14);
//                String stage = cursor.getString(15);
//                Toasty.error(this, title + " / " + description + " / " + start_event + " / " + end_event
//                                + " / " + detalles + " / " +stage
//                        , Toast.LENGTH_LONG).show();
//                titulo.setText(cursor.getString(3));
//                descripcion.setText(cursor.getString(4));
//                fecha_inicio.setText(cursor.getString(5));
//                fecha_final.setText(cursor.getString(6));
//                detalle.setText(cursor.getString(14));
//                estado.setText(cursor.getString(15));

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.verevento));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver2);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver2,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
                Toast.makeText(Ver_evento.this, Utils.getlatitud(location) + " /event/ " + Utils.getlongitud(location),
                        Toast.LENGTH_SHORT).show();
                //trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();

        RequestQueue queue = Volley.newRequestQueue(Ver_evento.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
//         StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //si hay internet las coordenadas se guardaran en el servidor
                            guardartrayectoria(id_user,lat, lon,emp);
                        }
                        else {
                            //Toast.makeText(ListEvent.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //si no hay internet las coordenadas se guardaran localmente
                            guardartrayectorialocal(id_user,lat, lon,emp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon,emp);
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2,String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        RequestQueue queue = Volley.newRequestQueue(Ver_evento.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//         StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2, String emp) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Ver_evento.this, ListEvent.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }
}