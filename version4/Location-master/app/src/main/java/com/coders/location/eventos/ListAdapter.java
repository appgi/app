package com.coders.location.eventos;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.coders.location.Formulario;
import com.coders.location.Get_Hours;
import com.coders.location.LoginPrefences;
import com.coders.location.R;
import com.coders.location.Ver_evento;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<ListElements> mData;
    private final LayoutInflater mInflater;
    private Context context;
    private String nombre;

    public ListAdapter(List<ListElements> itemList,Context context){
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mData = itemList;
        setItems(itemList);
    }
    @Override
    public int getItemCount(){
        return mData.size();
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = mInflater.inflate(R.layout.list_elements,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ListAdapter.ViewHolder holder,final int position){
        holder.binData(mData.get(position));
        LoginPrefences p = new LoginPrefences(context);
        if (!p.isEmpty()){
            nombre = p.GetName();
            //Toast.makeText(this,"id"+p.Getid(),Toast.LENGTH_SHORT).show();
        }
        holder.itemView.setOnClickListener(view -> {
//                Toast.makeText(
//                        context,
//                        holder.title + " id: "+ holder.id,
//                        Toast.LENGTH_LONG).show();

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Opciones");
            builder.setMessage("Hola " + nombre + "!, Eliga una Opción: ");        // add the buttons
            builder.setPositiveButton("Ver Evento", (dialog1, which) -> {
                Intent intent = new Intent(context, Ver_evento.class);
                Bundle mibundle = new Bundle();
                mibundle.putString("idevento",holder.id);
                intent.putExtras(mibundle);
                context.startActivity(intent);
            });
            builder.setNegativeButton("Completar Formulario", (dialog1, which) -> {
                Intent intent = new Intent(context,Formulario.class);
                Bundle mibundle = new Bundle();
                mibundle.putString("idevento",holder.id);
                intent.putExtras(mibundle);
                context.startActivity(intent);
            });
            builder.setNeutralButton("Cancelar", (dialog1, which) -> {
                dialog1.dismiss();
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        });
//        holder.Cv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(
//                        context,
//                        ,
//                        Toast.LENGTH_LONG).show();
//            }
//        });
    }
    public void setItems(List<ListElements> items){
        mData = items;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView Cv;
        LinearLayout linear;
        ImageView iconImage;
        TextView titulo,fecha,fecha2,estatus;
        String title = "",id = "",titulo_completo = "";
        Context context2;
        int cantidad_espacio = 0;

        @SuppressLint("UseCompatLoadingForDrawables")
        ViewHolder(View itemView){
            super(itemView);
            Cv =  itemView.findViewById(R.id.cv);
            linear = itemView.findViewById(R.id.bk);
            //linear2 = itemView.findViewById(R.id.bk2);
            iconImage = itemView.findViewById(R.id.imgview);
            titulo = itemView.findViewById(R.id.txtTitulo);
            fecha = itemView.findViewById(R.id.txtDescripcion);
            fecha2 = itemView.findViewById(R.id.txtDescripcion2);
            estatus = itemView.findViewById(R.id.txt_estado);

            context2 = itemView.getContext();
        }

        void binData(final ListElements item){
            iconImage.setColorFilter(Color.parseColor(item.getcolor()), PorterDuff.Mode.SRC_IN);
           // linear.setBackgroundColor(Color.parseColor(item.getcolor()));
            for (int i = 0; i < item.getTitle().length(); i++) {
                // Si el carácter en [i] es un espacio (' ') aumentamos el contador
                if (item.getTitle().charAt(i) == ' ') cantidad_espacio++;
            }
            if(cantidad_espacio >= 6){
                String[] result = item.getTitle().split(" ", 6);
                String a = result[0] + " " +result[1] + " " + result[2] + " " + result[3] + " " +result[4];
                String t = result[5];
                titulo.setText(a + " - Ver mas...");
            }else{
                titulo.setText(item.getTitle());
            }
           // titulo.setTextColor(Color.parseColor(item.text_color)); //se cambia el color al textview
            fecha.setText(item.getStart_event());
            fecha2.setText(item.getend_event());
            estatus.setText(item.getEstado());
            title = item.getTitle();
            id = item.getId();
            ((GradientDrawable)linear.getBackground()).setColor(Color.parseColor(item.getcolor()));

            titulo.setOnClickListener(view -> {
               // Toasty.info(context2, item.getTitle(), Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context2);
                builder1.setMessage(item.getTitle());
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Aceptar",
                        (dialog, id) -> dialog.cancel());

                AlertDialog alert11 = builder1.create();
                alert11.show();
            });
        }
    }

}
