package com.coders.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.Toast;

public class hora_service extends Service {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intencion, int flags, int idArranque) {
        handler.post(timeupdate);
        return START_STICKY;
    }

    public boolean horario_trabajo(Context c){
        String hora_actual = Get_Hours.getHora("hh:mm");
        String hora_inicial = "17:04";
        String hora_final   = "17:05";
        return !isHourInInterval(hora_actual, hora_inicial, hora_final);
    }
    public static boolean isHourInInterval(String target, String start, String end) {
        return ((target.compareTo(start) >= 0)&& (target.compareTo(end) <= 0));
    }

    Handler handler = new Handler();
    private final Runnable timeupdate = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(timeupdate, 5000 - SystemClock.elapsedRealtime()%5000);
            Intent broadcastIntent = new Intent();
           //broadcastIntent.setAction(Login.BroadcastStringForAction);
            broadcastIntent.putExtra("online_status", ""+horario_trabajo(hora_service.this));
            sendBroadcast(broadcastIntent);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;// throw new UnsupportedOperationException("not yet implement");//return null;
    }
}