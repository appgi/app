package com.coders.location.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.coders.location.help_ver_evento;

import java.sql.Blob;

public class BDAdapter {
    Context c;
    SQLiteDatabase db;
    BDHelper helper;

    public BDAdapter(Context c) {
        this.c = c;
        helper=new BDHelper(c);
    }

    //Abrir Base de Datos
    public void openDB()
    {
        try
        {
            db=helper.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //Cerrar Base de Datos
    public void closeDB()
    {
        try
        {
            helper.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    //************************************************************* the table formulario*************************************************************
    public void insertData(String username, String cellphone, String correo, byte[] image_c, byte[] firma, String name, String fecha,String idevento, String idusuario){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO form VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, username);
        statement.bindString(2, cellphone);
        statement.bindString(3, correo);
        statement.bindBlob(4, image_c);
        statement.bindBlob(5, firma);
        statement.bindString(6, name);
        statement.bindString(7, fecha);
        statement.bindString(8, idevento);
        statement.bindString(9, idusuario);

        statement.executeInsert();
    }

    public void deleteData(int id) {
        db =  helper.getWritableDatabase();

        String sql = "DELETE FROM form WHERE id = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        db.close();
    }

    public Cursor getData(String sql){
        db =  helper.getReadableDatabase();
        return db.rawQuery(sql, null);
    }
    public void get_idformulario(prueba_var pb)
    {
        Cursor cursor=null;
        String sql = "SELECT MAX(id) FROM form";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIdform(cursor.getString(0));
            }while (cursor.moveToNext());
        }
    }

    public void ver_info_evento(help_ver_evento pb, String idevento)
    {
        Cursor cursor=null;
        String sql = "SELECT * FROM evento where id = '"+idevento+"'";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setTitle(cursor.getString(3));
                pb.setDescripcion(cursor.getString(4));
                pb.setStart_event(cursor.getString((5)));
                pb.setEnd_event(cursor.getString(6));
                pb.setDetalles(cursor.getString(14));
                pb.setEstado(cursor.getString(15));
            }while (cursor.moveToNext());
        }
    }

    public boolean verifica_numero(String telefono)
    {
        Cursor c=null;
        boolean existe = false;

        if(telefono.length()>0) {
            String sql = "SELECT * FROM " + Constans.TB_NAME + " WHERE " + Constans.USERNAME + " LIKE '%" + telefono + "%'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        return existe;
    }


    //************************************************************* the table user*************************************************************
    public void insert_tbuser(String id_user, String username, String estado, String emp){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO usuario VALUES (NULL,?, ?, ?,?,NULL,NULL)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, id_user);
        statement.bindString(2, username);
        statement.bindString(3, estado);
        statement.bindString(4, emp);
        statement.executeInsert();
    }
    public void insert_tb_mov(String id_user, String android_id, String code,String id_mov){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO movilidad VALUES (NULL,?, ?, ?,?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, android_id);
        statement.bindString(2, code);
        statement.bindString(3, id_user);
        statement.bindString(4, id_mov);
        statement.executeInsert();
    }
    public void update_android_id( String android_id, String id_mov){
//        db =  helper.getWritableDatabase();
//        String sql = "UPDATE movilidad  SET android_id = ? where id_movilidad = ? ";
//
//        SQLiteStatement statement = db.compileStatement(sql);
//        statement.clearBindings();
//        statement.bindString(1, android_id);
//        statement.bindString(2, id_mov);
//        statement.executeUpdateDelete();
        ContentValues valores = new ContentValues();
        valores.put("android_id",android_id);

        String[] args = new String[]{id_mov};
        db.update("movilidad", valores, "id_movilidad = ?", args);
    }


    public boolean verifica_user(String username,String password)
    {
        Cursor c=null;
        boolean existe = false;

        if(username.length()>0 && password.length()>0 ) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
//            String sql = "SELECT * FROM usuario where username = '"+username+"' AND password = '"+password+"'"; //+ username + " AND password = " + password;
            String sql = "SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = '"+username+"' and movilidad.code = '"+password+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        return existe;
    }
    public void get_iduser_mobilidad_estado(prueba_var pb,String username,String password)
    {
        Cursor cursor=null;
//        String sql = "SELECT * FROM usuario where username = '"+username+"' AND password = '"+password+"'"; //+ username + " AND password = " + password;
        String sql = "SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = '"+username+"' and movilidad.code = '"+password+"'";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIduser_login(cursor.getString(1));
                pb.setMobilidad_login("1");
                pb.setEstado_login(cursor.getString(3));
                pb.setIdemp(cursor.getString(4));
            }while (cursor.moveToNext());
        }
    }

    public void get_iduser(prueba_var pb,String username,String password){
        Cursor cursor=null;
        String sql = "SELECT * FROM usuario where username = '"+username+"' AND password = '"+password+"'"; //+ username + " AND password = " + password;
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIduser_login(cursor.getString(1));
            }while (cursor.moveToNext());
        }
    }
    //trayec
    public void insert_trayect(String usuario, String lat, String lon,String fecha,String emp){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO trayectoria VALUES (NULL, ?, ?, ?,?,?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, usuario);
        statement.bindString(2, lat);
        statement.bindString(3, lon);
        statement.bindString(4, fecha);
        statement.bindString(5, emp);

        statement.executeInsert();
    }

    public void deletelisttrayectoria(int id) {
        db =  helper.getWritableDatabase();

        String sql = "DELETE FROM trayectoria WHERE id_trayectoria = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        db.close();
    }

    //************************************************ guardar evento local*****
//    public boolean verifica_evento(String id, String id_usuario,String title,
//                                   String descripcion, String start,
//                                   String end, String color,
//                                   String text_color, String create_it,
//                                   String create_by, String create_at,
//                                   String update_by, String update_at,
//                                   String detalles, String estado, String estado_local)
//    {
//        Cursor c=null;
//        boolean existe = false;
//
//        if(id.length()>0 && id_usuario.length()>0 && title.length()>0 &&
//                descripcion.length()>0 && start.length()>0 &&
//                end.length()>0 && color.length()>0 &&
//                text_color.length()>0 && create_it.length()>0 &&
//                create_by.length()>0 && create_at.length()>0 &&
//                update_by.length()>0 && update_at.length()>0 &&
//                detalles.length()>0 && estado.length()>0 && estado_local.length()>0
//        ) {
//            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
//            String sql = "SELECT * FROM evento where id = '"+id+"' AND id_usuario = '"+id_usuario+"' AND title = '"+title+"' AND descripcion = '"+descripcion+"' AND start = '"+start+"' AND end = '"+end+"' AND color = '"+color+"' AND text_color = '"+text_color+"' AND create_it = '"+create_it+"' AND create_by = '"+create_by+"' AND create_at = '"+create_at+"' AND update_by = '"+update_by+"' AND update_at = '"+update_at+"' AND detalles = '"+detalles+"' AND estado = '"+estado+"' AND estado_local = '"+estado_local+"'";
//            c = db.rawQuery(sql, null);
//            if (c.getCount() > 0) {
//                existe = true;
//            }
//        }
//        return existe;
//    }

    //************************************************ guardar evento local*****
    public boolean verifica_evento(String id, String id_usuario,String title,
                                   String descripcion)
    {
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0 && id_usuario.length()>0 && title.length()>0 &&
                descripcion.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM evento where id = '"+id+"' AND id_usuario = '"+id_usuario+"' AND title = '"+title+"' AND descripcion = '"+descripcion+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        return existe;
    }

    public boolean verifica_evento_local_ser(String id,String stage){
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM evento where id = '"+id+"' AND estado_local = '"+stage+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        return existe;
    }

    public void guardar_evento(String id, String id_usuario,String title,
                               String descripcion, String start,
                               String end, String color,
                               String text_color, String create_it,
                               String create_by, String create_at,
                               String update_by, String update_at,
                               String detalles, String estado,String estado_local){
        db =  helper.getWritableDatabase();
        String sql = "INSERT INTO evento VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, id);
        statement.bindString(2, id_usuario);
        statement.bindString(3, title);
        statement.bindString(4, descripcion);
        statement.bindString(5, start);
        statement.bindString(6, end);
        statement.bindString(7, color);
        statement.bindString(8, text_color);
        statement.bindString(9, create_it);
        statement.bindString(10, create_by);
        statement.bindString(11, create_at);
        statement.bindString(12, update_by);
        statement.bindString(13, update_at);
        statement.bindString(14, detalles);
        statement.bindString(15, estado);
        statement.bindString(16, estado_local);

        statement.executeInsert();
    }

//    public void actualizar_eventos(String idevento){
//        String stage = "Cumplida";
//        db =  helper.getWritableDatabase();
//        ContentValues valores = new ContentValues();
//        valores.put("estado",stage);
//
//        String[] args = new String[]{idevento};
//        db.update("evento", valores, "id = ?", args);
//    }

    public void actualizar_eventos(String idevento){
        db =  helper.getWritableDatabase();

        String[] args = new String[]{idevento};
        db.delete("evento", "id = ?", args);
    }

    public void update_stage_local(String idevento){
        String stage = "Cumplida";
        db =  helper.getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put("estado_local",stage);

        String[] args = new String[]{idevento};
        db.update("evento", valores, "id = ?", args);
    }

    public void get_img_user(prueba_var pb, String idusuario){
        Cursor cursor=null;
        String sql = "SELECT * FROM usuario WHERE id_user= '"+idusuario+"'";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setImg_user(cursor.getBlob(5));
            }while (cursor.moveToNext());
        }
    }

    public boolean verifica_url_user(String url){
        Cursor c=null;
        boolean existe = false;

        if(url.length()>0) {
            String sql = "SELECT * FROM usuario where url_img = '"+url+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        return existe;
    }

    public void update_url_user(String url,String iduser){
        db =  helper.getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put("url_img",url);

        String[] args = new String[]{iduser};
        db.update("usuario", valores, "id_user = ?", args);
    }

    public void update_img_user(byte[] img_user,String iduser){
        db =  helper.getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put("img_user",img_user);

        String[] args = new String[]{iduser};
        db.update("usuario", valores, "id_user = ?", args);
    }

}
