package com.coders.location.sql;

public class Constans {
    //Columnas
    static final String ROW_ID="id";
    static final String USERNAME="username";
    static final String CELLPHONE="cellphone";
    static final String CORREO="correo";
    static final String IMAGE_C="image_c";
    static final String FIRMA="firma";
    static final String NAME="name";

    //tabla usuario
    static final String USERNAME_USER="username";
    static final String PASSWORD_USER="password";

    //DB
    static final String DB_NAME="AppDB";
    static final String TB_NAME="form";
    static final String TB_NAME_USER="usuario";
    static final String TB_NAME_POSITION="position";
    static final String TB_NAME_TRAYECTORIA="trayectoria";
    static final String TB_NAME_MOVILIDAD="movilidad";
    static final String TB_NAME_EVENTO="evento";
    static final int DB_VERSION=1;

    //CREATE Tabla
    static final String CREATE_TB="CREATE TABLE form (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " username TEXT NOT NULL, " +
            " cellphone TEXT NOT NULL, " +
            " correo TEXT NOT NULL, " +
            " image_c BLOB NOT NULL, " +
            " firma BLOB NOT NULL, " +
            " name TEXT NOT NULL, " +
            " fecha TEXT NOT NULL, " +
            " idevento TEXT NOT NULL, " +
            " idusuario TEXT NOT NULL);";
    static final String CREATE_TB_USER="CREATE TABLE usuario (iduserlocal INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " id_user TEXT NOT NULL, " +
            " username TEXT NOT NULL, " +
            " estado TEXT NOT NULL, " +
            " id_emp TEXT NOT NULL, " +
            " img_user BLOB NULL, " +
            " url_img TEXT NULL);";
    static final String CREATE_TB_POSITION="CREATE TABLE position (id_posicion INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " id_user TEXT NOT NULL, " +
            " id_formu TEXT NOT NULL, " +
            " latitud TEXT NOT NULL, " +
            " longitud TEXT NOT NULL);";
    static final String CREATE_TB_TRAYECTORIA="CREATE TABLE trayectoria (id_trayectoria INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " id_user TEXT NOT NULL, " +
            " latitud TEXT NOT NULL, " +
            " longitud TEXT NOT NULL, " +
            " fecha TEXT NOT NULL, " +
            " emp TEXT NOT NULL);";
    static final String CREATE_TB_MOVILIDAD="CREATE TABLE movilidad (id_movilidad_local INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "android_id  TEXT NOT NULL, " +
            " code TEXT NOT NULL, " +
            " id_usuario TEXT NOT NULL, " +
            "id_movilidad  TEXT NOT NULL);";
    static final String CREATE_TB_EVENTO="CREATE TABLE evento (id_evento_local INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id  TEXT NOT NULL, " + //id del evento
            "id_usuario  TEXT NOT NULL, " +
            "title TEXT NOT NULL, " +
            "descripcion TEXT NOT NULL, " +
            "start  TEXT NOT NULL, " +
            "end TEXT NOT NULL, " +
            "color TEXT NOT NULL, " +
            "text_color  TEXT NOT NULL, " +
            "create_it TEXT NOT NULL, " +
            "create_by TEXT NOT NULL, " +
            "create_at  TEXT NOT NULL, " +
            "update_by TEXT NOT NULL, " +
            "update_at TEXT NOT NULL, " +
            "detalles TEXT NOT NULL, " +
            "estado TEXT NOT NULL, " +
            "estado_local  TEXT NOT NULL);";
}
