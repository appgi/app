package com.coders.location.sql;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.coders.location.Formulario;
import com.coders.location.LoginPrefences;
import com.coders.location.MainActivity;
import com.coders.location.R;
import com.coders.location.Ruta;
import com.coders.location.Services.GPS_Service;
import com.coders.location.Services.Utils;
import com.coders.location.eventos.ListEvent;
import com.coders.location.help_coordenadas;
import com.coders.location.notificacion.ExampleService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class FormuList extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{
    private MyReceiver myReceiver3;

    ListView gridView;
    ArrayList<formu> list;
    FormuListAdapter adapter = null;
    public static BDAdapter BD;
    String id_user;
    String var;
    ProgressDialog progreso;
    String emp;

    Bitmap bitmap_img,bitmap_firma;
    LoginPrefences p;
    help_coordenadas help_coordenadas;

    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_registro);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        help_coordenadas = new help_coordenadas();

        //open database
        BD = new BDAdapter(this);
        BD.openDB();

        //**************************************************************************************
        myReceiver3 = new FormuList.MyReceiver();
        setContentView(R.layout.activity_list_registro);
        //**************************************************************************************

        //get idusuario mediante sharedpreferneces
        p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
        }

        emp = p.GetEmp();//se obtiene el id de la empresa

        //Toast.makeText(FormuList.this,emp,Toast.LENGTH_SHORT).show();

        gridView = findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new FormuListAdapter(this, R.layout.form_item, list);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener((adapterView, view, pos, l) -> {
            final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(FormuList.this);
            dialogDelete.setTitle("Alerta!!");
            dialogDelete.setMessage("Desea enviar este archivo?");
            dialogDelete.setPositiveButton("OK", (dialog, which) -> {
                try {
                    byte[] image, firma;
                    image = list.get(pos).getImage_c();
                    bitmap_img = BitmapFactory.decodeByteArray(image, 0, image.length);
                    bitmap_img = redimensionarImagen(bitmap_img);
                    firma = list.get(pos).getFirma();
                    bitmap_firma = BitmapFactory.decodeByteArray(firma, 0, firma.length);

                    progreso=new ProgressDialog(FormuList.this);
                    progreso.setMessage("Cargando...");
                    progreso.show();
                    RequestQueue queue = Volley.newRequestQueue(FormuList.this);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_form.php",
                    //StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_form.php",
                            response -> {
                                try {
                                    JSONObject objResultado = new JSONObject(response);
                                    String estadox = objResultado.get("estado").toString();
                                    if(!estadox.contains("exito")){
                                        Toasty.error(this, "No se ha registrado", Toast.LENGTH_LONG).show();
                                    }else{
                                        get_id_form(list.get(pos).getIdevento());
                                        Toasty.success(this, "Enviado satisfactoriamente", Toast.LENGTH_LONG).show();
                                        BD.deleteData(list.get(pos).getId());
                                        updateList();
                                        BD.closeDB();
                                    }
                                    progreso.hide();
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }, error -> {Toast.makeText(FormuList.this, "Conexion invalida",
                            Toast.LENGTH_LONG).show(); progreso.hide();
                            Intent intent = new Intent(FormuList.this, MainActivity.class);
                            intent.putExtra("valoruser", id_user);//ojo
                            startActivity(intent);
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            String imagen=convertirImgString(bitmap_img);
                            String firma=convertirImgString(bitmap_firma);

                            Map<String, String> params = new HashMap<>();
                            params.put("username",list.get(pos).getUsername());
                            params.put("cellphone",list.get(pos).getCellphone());
                            params.put("correo",list.get(pos).getCorreo());
                            params.put("imagen_c",imagen);
                            params.put("firma",firma);
                            params.put("nombre_img",list.get(pos).getName());
                            params.put("id_user",id_user);
                            params.put("fecha",list.get(pos).getFecha());
                            params.put("idemp",emp);
                            return params;
                        }
                    };
                   // stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queue.add(stringRequest);
                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }
            });
            dialogDelete.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            dialogDelete.show();
        });
        // se obtiene los formularios registrado para un usuario
        Cursor cursor =  BD.getData("SELECT * FROM form WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(0);
                String username = cursor.getString(1);
                String cellphone = cursor.getString(2);
                String correo = cursor.getString(3);
                byte[] image_c = cursor.getBlob(4);
                byte[] firma = cursor.getBlob(5);
                String name = cursor.getString(6);
                String fecha = cursor.getString(7);
                String idevento = cursor.getString(8);
                list.add(new formu(username,cellphone,correo,image_c,firma,name,fecha,id,idevento));
            }
        }else{
            Toasty.info(this, "No hay Formularios Pendientes", Toast.LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
    }

    private Bitmap redimensionarImagen(Bitmap bitmap) {

        int ancho=bitmap.getWidth();
        int alto=bitmap.getHeight();

        if(ancho> (float) 600 || alto> (float) 800){
            float escalaAncho= (float) 600 /ancho;
            float escalaAlto= (float) 800 /alto;

            Matrix matrix=new Matrix();
            matrix.postScale(escalaAncho,escalaAlto);

            return Bitmap.createBitmap(bitmap,0,0,ancho,alto,matrix,false);

        }else{
            return bitmap;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.list));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }

    private String convertirImgString(Bitmap bitmap) {
        ByteArrayOutputStream array=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,array);
        byte[] imagenByte=array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);

        return imagenString;
    }
    //se actualiza la lista del listview
    private void updateList(){
        // get all data from sqlite
        Cursor cursor = BD.getData("SELECT * FROM form WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String username = cursor.getString(1);
                String cellphone = cursor.getString(2);
                String correo = cursor.getString(3);
                byte[] image_c = cursor.getBlob(4);
                byte[] firma = cursor.getBlob(5);
                String name = cursor.getString(6);
                String fecha = cursor.getString(7);
                String idevento = cursor.getString(8);
                list.add(new formu(username, cellphone, correo, image_c, firma, name, fecha,id,idevento));
            }
        }else{
            stopService();
        }
        adapter.notifyDataSetChanged();
    }

    public void get_id_form(String ideventolocal){//se obtiene el id del ultimo formulario
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/get_id_form.php",
        //StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "get_id_form.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        guardarGps(objResultado.get("id").toString(), ideventolocal);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Error en la Conexion", Toast.LENGTH_LONG).show()) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void guardarGps(String idformu, String ideventolocal)
    {
//        Toast.makeText(FormuList.this, id_user + " / " + idformu + " / " + help_coordenadas.getLat() + " / " +
//                help_coordenadas.getLon() + " / " + ideventolocal + " / " + emp, Toast.LENGTH_LONG).show();
        if(help_coordenadas.getLat() == null || help_coordenadas.getLat().isEmpty() && help_coordenadas.getLon() == null || help_coordenadas.getLon().isEmpty())
            savegps(idformu, ideventolocal, p.Getlat(), p.Getlon());
        else
            savegps(idformu, ideventolocal, help_coordenadas.getLat(), help_coordenadas.getLon());

        //Toast.makeText(FormuList.this,p.Getlat() + " / " +p.Getlon(),Toast.LENGTH_SHORT).show();
    }
    public void savegps(String idformu, String ideventolocal, String lat, String lon){
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/registrar_position.php",
                // StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "registrar_position.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            //var = "error";
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            BD.actualizar_eventos(ideventolocal);
                            Toasty.success(this, "Guardado", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(FormuList.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida al Enviar la Posicion", Toast.LENGTH_LONG).show()){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", id_user);
                params.put("id_formu", idformu);
                params.put("lat", lat);
                params.put("lon", lon);
                params.put("idevento", ideventolocal);
                params.put("idemp", emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver3);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver3,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(FormuList.this, Utils.getlatitud(location) + " /formu_list/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
                trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                help_coordenadas.setLat(Utils.getlatitud(location));
                help_coordenadas.setLon(Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
        //StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectoria(id_user,lat, lon,emp);
                        }
                        else {
//                            Toast.makeText(FormuList.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,lat, lon,emp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon,emp);
        }) {
        };
        queue.add(stringRequest);
    }
    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2,String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
       // StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2,String emp) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
