package com.coders.location.sql;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.coders.location.R;

import java.util.ArrayList;

public class FormuListAdapter extends BaseAdapter {
    private Context context;
    private  int layout;
    private ArrayList<formu> formuList;

    public FormuListAdapter(Context context, int layout, ArrayList<formu> formuList) {
        this.context = context;
        this.layout = layout;
        this.formuList = formuList;
    }

    @Override
    public int getCount() {
        return formuList.size();
    }

    @Override
    public Object getItem(int position) {
        return formuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView username,cellphone,correo;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.username = (TextView) row.findViewById(R.id.nombre);
            holder.cellphone = (TextView) row.findViewById(R.id.telefono);
            holder.correo = (TextView) row.findViewById(R.id.correo);
            holder.imageView = (ImageView) row.findViewById(R.id.imgc);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder) row.getTag();
        }

        formu formulario = formuList.get(position);

        holder.username.setText(formulario.getUsername());
        holder.cellphone.setText(formulario.getCellphone());
        if(formulario.getCorreo() == null || formulario.getCorreo().isEmpty()) holder.correo.setText("No Posee Correo");
        else holder.correo.setText(formulario.getCorreo());


        byte[] formuImage = formulario.getImage_c();
        Bitmap bitmap = BitmapFactory.decodeByteArray(formuImage, 0, formuImage.length);
        holder.imageView.setImageBitmap(bitmap);

        return row;
    }
}