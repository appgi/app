package com.coders.location.sql;

import java.sql.Blob;

public class prueba_var {

    private String iduser_login;
    private String idform;
    private String mobilidad_login;
    private String estado_login;
    private String idemp;
    private byte[] img_user;

    public byte[] getImg_user() {
        return img_user;
    }

    public void setImg_user(byte[] img_user) {
        this.img_user = img_user;
    }

    public String getIduser_login() {
        return iduser_login;
    }

    public void setIduser_login(String iduser_login) {
        this.iduser_login = iduser_login;
    }

    public String getIdform() {
        return idform;
    }

    public void setIdform(String idform) {
        this.idform = idform;
    }

    public String getMobilidad_login() {
        return mobilidad_login;
    }

    public void setMobilidad_login(String mobilidad_login) {
        this.mobilidad_login = mobilidad_login;
    }

    public String getEstado_login() {
        return estado_login;
    }

    public void setEstado_login(String estado_login) {
        this.estado_login = estado_login;
    }

    public String getIdemp() {
        return idemp;
    }

    public void setIdemp(String idemp) {
        this.idemp = idemp;
    }
}
