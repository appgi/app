package com.gicapp.gicapp;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Get_Hours {
    public static String getHora(String strFormato) {

        Calendar objCalendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormato);

        String strHora = simpleDateFormat.format(objCalendar.getTime());
        return strHora;

    }
}
