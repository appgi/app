package com.gicapp.gicapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;

import com.android.volley.DefaultRetryPolicy;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.sql.BDAdapter;
import com.gicapp.gicapp.sql.prueba_var;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class Login extends AppCompatActivity {
    public static final String REGEX_username = "^[a-zA-ZÀ-ÿ\\s]{1,40}$";
    public static int MILISEGUNDOS_ESPERA = 2500;
    public static int TIEMPO_ESPERA = 3500;
    TextInputLayout username, password;
    TextView mobilidad, state, txt_mod, txt_sta;
    Button btn_registrar;
    EditText campo1, campo2;
    ImageView img_logo;
    BDAdapter BD;
    static final Integer PHONESTATS = 0x1;
    String imei;

    //volley
    StringRequest stringRequest;
    ProgressDialog progreso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        //base dedatos local - sqlite
        BD = new BDAdapter(this);
        BD.openDB();

        //base dedatos local - sharedpreferences
        if (!new LoginPrefences(this).isUserLogedOut()) {
            //user's email and password both are saved in preferences
            StartHome();
            Login.this.finish();
        }

        progreso =new ProgressDialog(Login.this);

        username = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
        mobilidad = findViewById(R.id.text_modalidad);
        state = findViewById(R.id.text_estado);
        btn_registrar = findViewById(R.id.login);
        campo1 = findViewById(R.id.campo1);
        campo2 = findViewById(R.id.campo2);
        txt_mod = findViewById(R.id.text_modalidad2);
        txt_sta = findViewById(R.id.text_estado2);
        img_logo = findViewById(R.id.logo);

        //se agrego efecto al logo
        Animation traslacion = AnimationUtils.loadAnimation(this,R.anim.logo_traslacion);
        traslacion.setFillAfter(true);
        //scalacion.setFillBefore(true);
        traslacion.setRepeatMode(Animation.REVERSE);

        img_logo.startAnimation(traslacion);

        consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
        //********************************************************** verificacion de internet **********************************************************
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;
        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }
            if (tipoConexion1 || tipoConexion2) {
                //********************************* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                btn_registrar.setOnClickListener(v -> {
                    //se valida que los campos
                    if (!validateUsername() | !validatepassword()) {
                        return;
                    }
                    //consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
                    verifica_internet();
                });
            }
        } else {
            // Toast.makeText(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
            btn_registrar.setOnClickListener(v -> {
                //se valida que los campos
                if (!validateUsername() | !validatepassword()) {
                    return;
                }
                send_local();
            });
        }
    }

    public void verifica_internet() {
        try {
            stringRequest = new StringRequest(Request.Method.POST,"http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                    response -> {
                        try {
                            JSONObject objResultado = new JSONObject(response);
                            if (objResultado.get("internet").toString().equals("yes")) {
                                verifica_user();
                            } else {
                                Toasty.error(this, "No hay Acceso a Internet", Toast.LENGTH_LONG).show();
                                send_local();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> Toasty.error(this, "Verifique su Conexion a Internet", Toast.LENGTH_LONG).show()) {
            };
            // queue.add(stringRequest);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION VERIFICA INTERNET");
        }
    }

    private void send_local() {
        progreso.setMessage("Cargando...");
        progreso.show();
        if (BD.verifica_user(username.getEditText().getText().toString().trim(), password.getEditText().getText().toString().trim())) {
            //Toast.makeText(getApplicationContext(), "existe", Toast.LENGTH_LONG).show();
            prueba_var pb = new prueba_var();
            BD.get_iduser_mobilidad_estado(pb, username.getEditText().getText().toString().trim(), password.getEditText().getText().toString().trim());

            view_text();
            mobilidad.setText(pb.getMobilidad_login());
            state.setText(pb.getEstado_login());

            valide(pb.getIduser_login(), username.getEditText().getText().toString().trim(), password.getEditText().getText().toString().trim(),
                    pb.getEstado_login(), "", "",pb.getIdemp(),"","","");// OJO
        } else {
            Toasty.error(this, "Usuario No Valido", Toast.LENGTH_LONG).show();
        }
        progreso.hide();
    }

    public void verifica_user(){
        try {
            progreso.setMessage("Cargando...");
            progreso.show();
            stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_usuario.php",
                    response -> {
                        try {
                            JSONObject objResultado = new JSONObject(response);
                            String verifica_user = objResultado.get("estado").toString();

                            System.out.println("==> hora: " + Get_Hours.getHora("hh:mm a"));

                            if(verifica_user.equals("exito")){
                                //se obtiene la hora actual
                                String hora_actual = Get_Hours.getHora("hh:mm a");
                                //se define el horario
                                String hora_inicial = "07:20 a. m.";
                                String hora_final2 = "18:00 p. m.";

                                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
                                    Toasty.error(this, "No Puede Iniciar Sesión, No Se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                                }else{
                                    send_servidor();
                                }

                            }else{
                                Toasty.error(this, "Usuario No Valido!!!", Toast.LENGTH_LONG).show();
                            }
                             progreso.hide();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {Toasty.error(this, "Error en la Conexio - Login", Toast.LENGTH_LONG).show();progreso.hide();}) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<>();
                    params.put("username", username.getEditText().getText().toString().trim());
                    params.put("password", password.getEditText().getText().toString().trim());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);

        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION VERIFICA_USER");
        }
    }

    public static boolean isHourInInterval2(String target, String end) {
        return ((target.compareTo(end) >= 0));
    }

    public static boolean isHourInInterval3(String target, String start) {
        return ((target.compareTo(start) <= 0));
    }

    public void send_servidor() {
        try {
            stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_login.php",
                    response -> {
                        try {
                            JSONObject objResultado = new JSONObject(response);
                            String estadox = objResultado.get("state").toString();
                            if (estadox.equals("No Encontrado")) {
                                timewait(MILISEGUNDOS_ESPERA);
                                System.out.println("====================> NO VALIDO" );

                            } else if (estadox.equals("ERROR")) {
                                Toasty.error(this, "Faltan Datos Por Ingresar", Toast.LENGTH_LONG).show();
                                //System.out.println("====================> FALTAN DATOS" );
                            } else {
                                String id_user = objResultado.getString("id_user");
                                String username = objResultado.getString("username");
                                String id_mov = objResultado.getString("id_movilidad");
                                String code = objResultado.getString("code");
                                String estado = objResultado.getString("estado");
                                String android_id = objResultado.getString("android_id");
                                String emp = objResultado.getString("idemp");
                                String alert_mobil = objResultado.getString("alert_mobil");
                                String alert_mail = objResultado.getString("alert_mail");
                                String telefono;

//                                if(objResultado.getString("alert_mobil").equals("") || objResultado.getString("alert_mail").equals("")){
//                                    alert_mobil = "85513923"; alert_mail="jean.rocha@oportucredit.com.ni";
//                                }else{
//                                    alert_mobil = objResultado.getString("alert_mobil");//"85513923";
//                                    alert_mail = objResultado.getString("alert_mail");//"jean.rocha@oportucredit.com.ni";
//                                }
                                telefono = alert_mobil;

                                view_text();
                                mobilidad.setText("1");
                                state.setText(objResultado.get("estado").toString());

                                System.out.println("=======> " + android_id + " <");

                                if (android_id == null || android_id.equals(""))
                                {
                                    update_android_id(id_mov);
                                    android_id = obtenerIMEI();
                                    //Toast.makeText(Login.this, "vacio", Toast.LENGTH_LONG).show();
                                    valide(id_user, username, code, estado, android_id, id_mov,emp,telefono,alert_mobil,alert_mail);
                                }
                                else{
                                    String ime = obtenerIMEI();
                                    if (!ime.equals(android_id)){
                                        Toasty.warning(this, "Tiene otra sesion activa, Contactese con su administrador", Toast.LENGTH_LONG).show();
                                        timewait(MILISEGUNDOS_ESPERA);
                                    }else
                                    {
                                        valide(id_user, username, code, estado, android_id, id_mov,emp,telefono,alert_mobil,alert_mail);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> Toasty.error(this, "Erro en la Conexion - Login", Toast.LENGTH_LONG).show()) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<>();
                    params.put("username", username.getEditText().getText().toString().trim());
                    params.put("password", password.getEditText().getText().toString().trim());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);

        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION SEND_SERVIDOR");
        }
    }

    private void valide(String id_user, String username, String code, String estado, String android_id,
                        String id_mov,String emp, String telefono,String alert_mobil, String alert_mail) {
        try {
            if (BD.verifica_user(username,code)) {
                saveLoginDetails(username, code, id_user, estado, android_id,emp,telefono,alert_mobil,alert_mail);//se guarda en el SharedPreferences
                StartHome();
                Login.this.finish();
            } else {
                BD.insert_tbuser(id_user, username, estado,emp);
                BD.insert_tb_mov(id_user, android_id, code, id_mov,alert_mobil,alert_mail);

                BD.update_android_id(android_id,id_mov);
                saveLoginDetails(username, code, id_user, estado, android_id,emp,telefono,alert_mobil,alert_mail);
                StartHome();
                Login.this.finish();
            }
        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION VALIDE");
        }
    }

    private void update_android_id(String id_mov) {
        String  ime2 = obtenerIMEI();
       stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/save_android_id.php",
       //StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_WEB_SERVICE + "save_android_id.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if (objResultado.get("state").toString().equals("Success")) {
                            //Toasty.success(this, "Actualizado con Exito", Toast.LENGTH_LONG).show();
                            System.out.println("Actualizado con Exito");
                        } else {
                            Toasty.info(this, "No se Actualizo", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show()) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("id", id_mov);
                params.put("android_id", ime2);
                return params;
            }
        };
       // queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //    validacion de campos
    private boolean validateUsername()
    {
        String usernameInput = username.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_username);

        if (usernameInput.isEmpty()){
            username.setError("Campo vacio");
            Toasty.error(this, "Campo Nombre Vacio", Toast.LENGTH_LONG).show();
            return false;
        } else if (!patron.matcher(usernameInput).matches()) {
            username.setError("Nombre invalido");
            Toasty.error(this, "Nombre Invalido", Toast.LENGTH_LONG).show();
            return false;
        } else {
            username.setError(null);
            return true;
        }
    }

    private boolean validatepassword() {
        String passinput = password.getEditText().getText().toString().trim();
        if (passinput.isEmpty()) {
            password.setError("Campo vacio");
            Toasty.error(this, "Campo Contraseña Vacio", Toast.LENGTH_LONG).show();
            return false;
        } else if (passinput.length() < 2) {
            password.setError("Contraseña muy corto");
            Toasty.error(this, "Contraseña muy corto", Toast.LENGTH_LONG).show();
            return false;
        } else {
            password.setError(null);
            return true;
        }
    }
//    fin validacion de campos


    private void StartHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void limpiarcampos() {
        campo1.requestFocus();
        campo1.setText("");
        campo2.setText("");
        mobilidad.setText("");
        state.setText("");
        txt_mod.setVisibility(View.INVISIBLE);
        txt_sta.setVisibility(View.INVISIBLE);
    }

    public void view_text() {
        txt_mod.setVisibility(View.VISIBLE);
        txt_sta.setVisibility(View.VISIBLE);
    }

    public void timewait(int milisegundos) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // acciones que se ejecutan tras los milisegundos
                limpiarcampos();
            }
        }, milisegundos);
    }

    private void saveLoginDetails(String username, String code, String idUser, String estado, String android_id,String emp,String telefono,String alert_mobil, String alert_mail) {
        new LoginPrefences(this).saveLoginDetails(username, code, idUser, estado, android_id,emp,telefono,alert_mobil,alert_mail);
    }

    private void consultarPermiso(String permission, Integer requestCode) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(Login.this, permission)) {
                ActivityCompat.requestPermissions(Login.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(Login.this, new String[]{permission}, requestCode);
            }
        } else {
            imei = obtenerIMEI();
           // timewait2(2000);
//            Toast.makeText(this, permission + " El permiso a la aplicación esta concedido.", Toast.LENGTH_SHORT).show();
        }
    }
        @SuppressLint("HardwareIds")
    private String obtenerIMEI() {
        final TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Hacemos la validación de métodos, ya que el método getDeviceId() ya no se admite para android Oreo en adelante, debemos usar el método getImei()
//            return telephonyManager.getImei();
            String myIMEI = Settings.Secure.getString(Login.this.getContentResolver(), Settings.Secure.ANDROID_ID);
            return myIMEI;

        } else {
            consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(Login.this, Manifest.permission.READ_PHONE_STATE)) {
                    ActivityCompat.requestPermissions(Login.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PHONESTATS);
                } else {
                    ActivityCompat.requestPermissions(Login.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PHONESTATS);
                }
            }
            return telephonyManager.getDeviceId();
        }
    }

    public void timewait2(int milisegundos) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                checkSMSStatePermission();
            }
        }, milisegundos);
    }

    //permiso para los mensaje de texto
    private void checkSMSStatePermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.SEND_SMS);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i("Mensaje", "No se tiene permiso para enviar SMS.");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 225);
        } else {
            Log.i("Mensaje", "Se tiene permiso para enviar SMS!");
        }
    }

}

