package com.gicapp.gicapp;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginPrefences {
    Context context;

    public LoginPrefences(Context context) {
        this.context = context;
    }

    public void saveLoginDetails(String email, String password, String iduser, String estado, String android_id, String empr, String telefono, String alert_mobil, String alert_mail){
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id", iduser);
        editor.putString("Name", email);
        editor.putString("Password", password);
        editor.putString("Estado", estado);
        editor.putString("Android_id", android_id);
        editor.putString("Emp",empr);
        editor.putString("telefono",telefono);
        editor.putString("alert_mobil",alert_mobil);
        editor.putString("alert_mail",alert_mail);
        editor.apply();
    }

    public void save_cood(String lat, String lon){
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("lat", lat);
        editor.putString("lon", lon);
        editor.apply();
    }

    public void update_mobil(String movil){
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("alert_mobil", movil);
        editor.apply();
        editor.commit();
    }
    public void update_mail(String mail){
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("alert_mail", mail);
        editor.apply();
        editor.commit();
    }

    public String Getlat() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("lat", "");
    }
    public String Getlon() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("lon", "");
    }

    public String Getid() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("id", "");
    }
    public String GetName() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Name", "");
    }

    public String Getpassword() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Password", "");
    }
    public String GetAndroid_id() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Android_id", "");
    }
    public String GetEmp() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Emp", "");
    }

    public String GetTelefono() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("telefono", "");
    }

    public String GetAlert_mobil() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("alert_mobil", "");
    }

    public String GetAlert_mail() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("alert_mail", "");
    }

    public boolean isUserLogedOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        boolean isEmailEmpty = sharedPreferences.getString("Name", "").isEmpty();
        boolean isPasswordEmpty = sharedPreferences.getString("Password", "").isEmpty();
        return isEmailEmpty || isPasswordEmpty;
    }
    public boolean isEmpty() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        boolean ValorEmpty = sharedPreferences.getString("id", "").isEmpty();
        return ValorEmpty;
    }

}
