package com.gicapp.gicapp;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.cliente.ListCliente;
import com.gicapp.gicapp.eventos.ListEvent;
import com.gicapp.gicapp.notificacion.ExampleService;
import com.gicapp.gicapp.notificacion.NotificationHelper;
import com.gicapp.gicapp.sql.BDAdapter;
import com.gicapp.gicapp.sql.FormuList;
import com.gicapp.gicapp.sql.prueba_var;
import com.gicapp.gicapp.visita_guarda.Visita_cliente;
import com.gicapp.gicapp.visita_guarda.visitacliente_list;

import androidx.appcompat.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import android.telephony.SmsManager;

import static com.gicapp.gicapp.notificacion.NotificationHelper.CHANNEL_SERVICE_ID;


public class MainActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{

   // public static int MILISEGUNDOS_ESPERA = 6000;
    private static final String PACKAGE_NAME = "com.gicapp.gicapp";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    help_coordenadas help_coordenadas;
    private boolean isGPS = false;
    String id_user,nombre_user,telefono_user,android_id_user,password_user,id_emp,mobil_alert,mail_alert;
    BDAdapter BD;
    ImageButton btn_verlist,btn_evento,btn_cliente,btn_vercliente,btn_visita_cliente,btn_list_visita_cliente;
    //CircleImageView image_url;
    ImageView image_url;
    TextView tvnombreuser,tvlista,tvevento,ttnombreuser;
    String var,mess;
    prueba_var prueba_var;
    private Menu menu2;
    Bitmap img;
    boolean flag_internet = false;

    //new capture gps ***********************************************************************************
    //services
    private static final String TAG = MainActivity.class.getSimpleName();

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    // The BroadcastReceiver used to listen from broadcasts from the service.
   private MyReceiver myReceiver;///

    // A reference to the service used to get location updates.
    private GPS_Service mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    public static final String BroadcastStringForAction = "checkinternet";
    public static final String BroadcastStringForAction2 = "checkubicacion";
    private IntentFilter nIntentFilter;
    BroadcastReceiver myReceiver_principal,myReceiver_principal_ubicacion;

    private IntentFilter nIntentFilter_ubicacion;

    int contador = 0;
    LoginPrefences p;
    //volley
    StringRequest stringRequest;

    GpsUtils gpsUtils;
    boolean verificagps =  false;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        BD = new BDAdapter(this);
        BD.openDB();

        help_coordenadas = new help_coordenadas();
        prueba_var = new prueba_var();
        //get idusuario mediante sharedpreferneces
        p = new LoginPrefences(this);

        if (!p.isEmpty()){
            id_user = p.Getid();
            nombre_user = p.GetName();
            telefono_user = p.GetTelefono();
            android_id_user = p.GetAndroid_id();
            password_user = p.Getpassword();
            id_emp  = p.GetEmp();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
        }

        //**************************************************************************************
         myReceiver = new MyReceiver();/////
//        myReceiver_ubicacion = new MyReceiver();////
        setContentView(R.layout.activity_main);
        //**************************************************************************************

        btn_verlist = findViewById(R.id.verlista);
        btn_evento = findViewById(R.id.btn_eventos);
        btn_cliente = findViewById(R.id.btn_cliente);
        tvnombreuser = findViewById(R.id.nombre_user);
        ttnombreuser = findViewById(R.id.ttnombreuser);
        tvlista = findViewById(R.id.txteventos);
        tvevento = findViewById(R.id.txtverlista);
        image_url = findViewById(R.id.imagen_url);
        btn_vercliente = findViewById(R.id.btn_ver_cliente);
        btn_visita_cliente = findViewById(R.id.btn_visitacliente);
        btn_list_visita_cliente = findViewById(R.id.btn_list_visitacliente);

        //se cambia de fuentes fonts_franklin-light.ttf
        Typeface face=Typeface.createFromAsset(getAssets(),"fuentes/fonts_franklin_Bold.ttf");
        Typeface face2=Typeface.createFromAsset(getAssets(),"fuentes/fonts_franklin-light.ttf");

        ttnombreuser.setTypeface(face);
        tvnombreuser.setTypeface(face2);
//        tvlista.setTypeface(face);
//        tvevento.setTypeface(face);
        tvnombreuser.setText(nombre_user);

        //Permisos SMS
        checkSMSStatePermission();


        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 || tipoConexion2) {
                //********************************* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                 //Toast.makeText(getApplicationContext(), "hay internet ", Toast.LENGTH_SHORT).show();
                verificaIntenet_noti();
                verificaIntenet_session();
//                //get_url_photo();
                verifica_internet_for_sendtrayec();
            }
        }
        else {
            //Toast.makeText(getApplicationContext(), "Favor encender el WIFI ó Datos Moviles", Toast.LENGTH_LONG).show();
            /* ******************************** No estas conectado a internet********************************  */
            //Toast.makeText(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
            Toasty.info(this, "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_SHORT).show();
            cargar_imguser_local();
        }

        btn_verlist.setOnClickListener(v -> {
            Intent intent4 = new Intent(MainActivity.this, FormuList.class);
            startActivity(intent4);
        });

        btn_evento.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ListEvent.class);
            startActivity(intent);
        });

        btn_cliente.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, Registrar_cliente.class);
            startActivity(intent);
        });

        btn_vercliente.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ListCliente.class);
            startActivity(intent);
        });

        btn_visita_cliente.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, Visita_cliente.class);
            startActivity(intent);
        });

        btn_list_visita_cliente.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, visitacliente_list.class);
            startActivity(intent);
        });

       // si hay formularios pendientes, se inicia la notificacion
         VerificaRegistroFormu();

//        //si hay clientes Pendientes, se inicia la notificacion
        VerificaRegistrocliente();
////        //si hay Registro de Visita clientes pendientes, se inicia la notificacion
        VerificaRegistroVisitaCliente();
    }

    //notificacion de eventos asigando desde la web
    public void notificacion_eventos()
    {
        // RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/get_id_evento.php",
                response -> {
                    try {
                        //se obtiene el nombre de las img
                        JSONObject objResultado = new JSONObject(response);
                        //System.out.println("======> "+ objResultado.get("idplanner").toString());
                        //si el evento no existe, se crea una alerta
                        if(!BD.verifica_evento_mainactivity(objResultado.get("idplanner").toString()))
                        {
                            alert();
                        }else{
                            //si el evento existe, que se quite la notificacion
                            delete_noti();
                        }
                        System.out.println("========================> "+ objResultado.get("idplanner").toString());
//                        else {Toasty.success(this, "Ya existe", Toast.LENGTH_LONG).show();}
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }// var = "error"
                }, error ->{ var = "error";

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idevento", id_user);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //Se Obtiene La Imagen de los Usuarios
    public void get_url_photo(){
       // StringRequest stringRequest = new StringRequest(Request.Method.POST,  Ruta.URL_WEB_SERVICE + "get_foto.php",
         stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/get_foto.php",
                response -> {
                    try {
                        //se define la ruta donde estan guardada las img
                        String ruta = "https://panel.nicaapps.com/img/users_client/";
                        //String ruta = "http://192.168.6.237/app2/webservices/img_cedula/";

                        //se obtiene el nombre de las img
                        JSONObject objResultado = new JSONObject(response);
                        String nombre_img_user = objResultado.get("imagen_Img").toString();

                        //url = url.replace(" ", "%20");
                        //si el campo del usuario tiene foto
                        if(!nombre_img_user.equals(" ")){
                            //se concatena y se busca en el dominio
                            String url = ruta + nombre_img_user;
                            //si existen espacios en blancos se subtitute
                            url = url.replace(" ", "%20");
                            //si la url no existe, entonces que la guarde localmente, de esta menera
                            //se valida si la imagen ya existe
                            if(!BD.verifica_url_user(url)){
                                BD.update_url_user(url,id_user);
                                image(url);
                            }else{
                                //si la url ya existe, entonces que se cargue la imagen de manera local
                                cargar_imguser_local();
                            }
                        }else{//si el usuario no tiene foto, entoces se le asigna otra x default de manera local
                            img = BitmapFactory.decodeResource(getResources(), R.drawable.usuario);
                            img = redimensionarImagen(img);
                            image_url.setImageBitmap(img);
                            BD.update_img_user(imageViewToByte(image_url), id_user);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }// var = "error"
                }, error ->{ var = "error";//Toast.makeText(MainActivity.this,"sin foto el usuario",Toast.LENGTH_SHORT).show();
                        //cargar_imguser_local();
                         img = BitmapFactory.decodeResource(getResources(), R.drawable.usuario);
                         img = redimensionarImagen(img);
                         image_url.setImageBitmap(img);
                         BD.update_img_user(imageViewToByte(image_url), id_user);
                 }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario", id_user);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void image(String url)
    {
        ImageRequest imageRequest =new ImageRequest(url,
                response1 -> {
                    img = redimensionarImagen(response1);
                    image_url.setImageBitmap(img);
                    BD.update_img_user(imageViewToByte(image_url), id_user);
                },0,0, ImageView.ScaleType.CENTER_CROP,null,
                error -> {
                   // Toast.makeText(MainActivity.this,"Ha ocurrido un error",Toast.LENGTH_SHORT).show();
                    Toasty.error(this, "Ha ocurrido un error en establecer la imagen", Toast.LENGTH_LONG).show();
                   // image_url.setImageResource(R.drawable.ic_usuario);
                    //BD.update_img_user(imageViewToByte(image_url), id_user);
                    img = BitmapFactory.decodeResource(getResources(), R.drawable.usuario);
                    img = redimensionarImagen(img);
                    image_url.setImageBitmap(img);
                    BD.update_img_user(imageViewToByte(image_url), id_user);
                    error.printStackTrace();
                });
        //queue2.add(imageRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(imageRequest);
    }
    //se redimenciona la imagen, para mostrarla en el imageview
    private Bitmap redimensionarImagen(Bitmap bitmap) {
        int ancho=bitmap.getWidth();
        int alto=bitmap.getHeight();

        if(ancho> (float) 600 || alto> (float) 800){
            float escalaAncho= (float) 600 /ancho;
            float escalaAlto= (float) 800 /alto;

            Matrix matrix=new Matrix();
            matrix.postScale(escalaAncho,escalaAlto);

            return Bitmap.createBitmap(bitmap,0,0,ancho,alto,matrix,false);

        }else{
            return bitmap;
        }
    }
    //se carga la imagen de manera local
    public void cargar_imguser_local()
    {
        prueba_var pb = new prueba_var();
        BD.get_img_user(pb,id_user);

        byte[] imagen_user = pb.getImg_user();
        Bitmap bitmap = BitmapFactory.decodeByteArray(imagen_user, 0, imagen_user.length);
        image_url.setImageBitmap(bitmap);
    }
    //se convierte la imagen a byte
    public byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
    //se valida la instancia gps(ventana de permiso de encender el gps) para que
    //no se muestra a cada rato
    public void verifica_gps(){
        if (gpsUtils == null){
            gpsUtils = new GpsUtils(this);
            gpsUtils.turnGPSOn(isGPSEnable -> isGPS = isGPSEnable);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.GPS_REQUEST) {
                isGPS = true; // flag maintain before get location
                verificagps = true;
            }
        }else{
            gpsUtils = null;
        }
    }

    public void alert(){
        Intent notificationIntent = new Intent(this, Login.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);//.setContentIntent(pendingIntent)

        contador++;

        if(contador == 1){
            //Se Activa el Sonido de la Notificacion
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            MediaPlayer mp = MediaPlayer. create (getApplicationContext(), alarmSound);
            mp.start();
        }

        NotificationHelper notificationHelper = new NotificationHelper(this);
        notificationHelper.createChannels();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_SERVICE_ID)
                .setSmallIcon(R.drawable.ic_today_black_24dp)
                .setContentTitle("Evento")
                .setContentText("Hola "+ nombre_user+", Se Ha Registrado un Nuevo Evento")
                .setContentIntent(pendingIntent)
                .setVibrate(new long[]{250,250,250,250})
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
//        notification.sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tu_sonido);
//        notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, builder.build());
    }

    public void verificaIntenet_noti(){
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        flag_internet = objResultado.get("internet").toString().equals("yes");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                System.out.println("ERROR EN LA CONEXION");
            }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //Se Quita la Notificacion -> EVENTOS!!
    public void delete_noti(){
        contador = 0;
        NotificationHelper notificationHelper = new NotificationHelper(this);
        notificationHelper.delete_ch();
    }
    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2, String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            //Toast.makeText(MainActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            //var = "error2";
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);// comentariar cuando se envie al servidor
                params.put("idemp",emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2, String emp)
    {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try{
            BD.insert_trayect(iduser,c1,c2,fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
           // BD.closeDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //si hay internet, los datos locales se envian al servidor
    public void verifica_internet_for_sendtrayec(){
       // RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            sendtrayec();
                            consultar_cliente();
                            consultar_doc2();//***********
                            get_url_photo();
                            //verifica_login();
                        }
                        else {
                            Toasty.info(this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            cargar_imguser_local();//se Carga la imagen local
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                    Toasty.info(this, "No tiene Acceso a Internet", Toast.LENGTH_LONG).show();
                    cargar_imguser_local();
                 }) {
        };
        ///queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //Si Existen Registro de trayectoria localmente, entonces se sube al servidor
    public void sendtrayec(){
        Cursor cursor =  BD.getData("SELECT * FROM trayectoria");
        if(cursor.getCount() > 0){
            if (cursor.moveToFirst()) {
                do {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_user", cursor.getString(1));
                    params.put("lat", cursor.getString(2));
                    params.put("lon", cursor.getString(3));
                    params.put("fecha", cursor.getString(4));
                    params.put("idemp", cursor.getString(5));
                    processp(params);
                    BD.deletelisttrayectoria(cursor.getInt(0));
                } while (cursor.moveToNext());
            }
        }
//        else{
//            Toast.makeText(MainActivity.this, "Datos locales se enviaron satisfactoriamente", Toast.LENGTH_LONG).show();
//        }
    }

    public void processp(final Map<String, String> parameters){
        //RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                                Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Subiendo Trayectoria al Servidor....", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //se verifica si hay registro pendientes, se activa la notificacion
    public void VerificaRegistroFormu()
    {
        Cursor cursor =  BD.getData("SELECT * FROM form where idusuario = '"+id_user+"'");
        if(cursor.getCount() > 0){
            startService();//se inicia el servicio
        }else{
            stopService();//se detiene el servico
        }
    }

    public void VerificaRegistrocliente(){
        Cursor cursor =  BD.getData("SELECT * FROM cliente where idusuario = '"+id_user+"'");
        if(cursor.getCount() > 0){
            startService2(); // se inicia el servico
        }else{
            stopService();//se detien el servicio
        }
    }

    public void VerificaRegistroVisitaCliente()
    {
        Cursor cursor = BD.getData("SELECT * FROM inv_visita_cliente where idusuario = '"+id_user+"'");
        if(cursor.getCount() > 0) {
            startService_visitacliente();//se inicia el servicio
        }else{
            stopService();// se detiene el servico
        }
    }

    //si hay formularios pendientes de manera local, se muestra la Notificacion
    public void startService() {
        String input = "Formularios Pendientes";
        Intent serviceIntent = new Intent(this, ExampleService.class);
        serviceIntent.putExtra("inputExtra", input);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void startService2() {
        String input = "Clientes Pendientes";
        Intent serviceIntent = new Intent(this, ExampleService.class);
        serviceIntent.putExtra("inputExtra", input);
        ContextCompat.startForegroundService(this, serviceIntent);
    }
    public void startService_visitacliente() {
        String input = "Visita Clientes Pendientes";
        Intent serviceIntent = new Intent(this, ExampleService.class);
        serviceIntent.putExtra("inputExtra", input);
        ContextCompat.startForegroundService(this, serviceIntent);
    }
    //se detiene el servicio
    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }

    public void verifica_login(){
        try {
            stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_login.php",
                    response -> {
                        try {
                            JSONObject objResultado = new JSONObject(response);
                            String estadox = objResultado.get("state").toString();
                            if (estadox.equals("No Encontrado")) {
                                System.out.println("====================> NO VALIDO" );
                            } else if (estadox.equals("ERROR")) {
                                Toasty.error(this, "Faltan Datos Por Ingresar", Toast.LENGTH_LONG).show();
                                //System.out.println("====================> FALTAN DATOS" );
                            } else {

//                                String alert_mobil = objResultado.getString("alert_mobil");
//                                String alert_mail = objResultado.getString("alert_mail");
                                if(!objResultado.getString("alert_mail").equals(""))
                                    if(!objResultado.getString("alert_mail").equals(mail_alert))
                                        update_mail(objResultado.getString("alert_mail"));


                                if(!objResultado.getString("alert_mobil").equals(""))
                                    if(!objResultado.getString("alert_mobil").equals(mobil_alert))
                                        update_mobil(objResultado.getString("alert_mobil"));


//                                if(!objResultado.getString("alert_mobil").equals(""))
//                                    update_mobil(objResultado.getString("alert_mobil"));
                                System.out.println("=UPDATE==> " + nombre_user + " / " + password_user + " / " + mobil_alert + " / " + mail_alert);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> Toasty.error(this, "Error en la Conexion - Login", Toast.LENGTH_LONG).show()) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<>();
                    params.put("username", nombre_user);
                    params.put("password", password_user);
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);

        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION VERIFICA_LOGIN");
        }
    }

    private void update_mobil(String mobil){
        new LoginPrefences(this).update_mobil(mobil);
    }

    private void update_mail(String mail){
        new LoginPrefences(this).update_mail(mail);
    }

    //***************************************************************************************************************
    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.menuprincipal));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        nIntentFilter = new IntentFilter();
        nIntentFilter.addAction(BroadcastStringForAction);
        Intent serviceIntent1 = new Intent(this,Service_GPS.class);
        startService(serviceIntent1);
        //segundo plano - se verifica si el status del gps [on / off], Permiso del gps en la app y el horario de trabajo[7.30 a 18.00]
        myReceiver_principal = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(Objects.equals(intent.getAction(), BroadcastStringForAction)) {
                    if(intent.getStringExtra("verifica_gps").equals("true")){
                        if(intent.getStringExtra("verifica_permiso").equals("true")){
                            mService.requestLocationUpdates();
                            semana(intent.getStringExtra("verifica_hora").equals("true"),intent.getStringExtra("verifica_hora_sab").equals("true"));

//                            if(!p.Getlat().isEmpty() && !p.Getlon().isEmpty())
//                                veri_mi_ubicacion();
                        }else{
                           checkLocationPermission();//se verifica los permiso del gps en la app
                        }
                    }else{
                        if(verificagps){
                            isGPS = false;
                            gpsUtils = null;
                            verificagps = false;
                        }
                        verifica_gps();//se verifica el status del gps
                    }
                }
            }
        };

        //************** segundo plano - ubicacion ****************************
//        nIntentFilter_ubicacion = new IntentFilter();
//        nIntentFilter_ubicacion.addAction(BroadcastStringForAction2);
//        Intent serviceIntent1_ubi = new Intent(this, Servicio_Ubicacion.class);
//        startService(serviceIntent1_ubi);

//        myReceiver_principal_ubicacion = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                ///
//                Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
//                if (location != null)
//                {
//                    //verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
//                    //save_cood(Utils.getlatitud(location), Utils.getlongitud(location));
//                    System.out.println("=====================> " + Utils.getlatitud(location)+" ||| "+Utils.getlongitud(location));
//                }else{
//
//                }
//                System.out.println("=====================>  xd");
//            }
//        };


        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }


    public void semana(boolean status_h,boolean status_h_sab){
        //dia de la semana
        Calendar now2 = Calendar.getInstance();
        int numD = now2.get(Calendar.DAY_OF_WEEK);

        String hora_actual = Get_Hours.getHora("hh:mm a");
        //Toast.makeText(getApplicationContext(), "hora actual =>" + hora_actual, Toast.LENGTH_SHORT).show();
       // System.out.println("hora actual =====>" + hora_actual);
        String hora_inicial = "07:20 a. m.";
        String hora_final2 = "18:02 p. m.";

        String hora_inicial_sab = "07:20 a. m.";
        String hora_final2_sab = "14:02 p. m.";

        //statusitem(false); => si esta en su periodo laboral no se puede salir de la app, se oculta el boton => cerrar session
        //statusitem(true); => si esta fuera de su periodo laboral, se habilita el boton para que pueda cerrar sesion

        if(numD == Calendar.SUNDAY) {//DOMINGO
            statusitem(true);
            if (Logout()){
                //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                unregisterReceiver(myReceiver_principal);
                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivity(intent);
                stopService();
                mService.removeLocationUpdates();
                this.finish();
            }
        }
        else if(numD == Calendar.MONDAY) {//LUNES
            if(status_h) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
                notificacion_eventos();
            }else{
                statusitem(true);
                notificacion_eventos();

                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();

                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
                   // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
                    if (Logout()){
                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        unregisterReceiver(myReceiver_principal);
                        Intent intent = new Intent(MainActivity.this,Login.class);
                        startActivity(intent);
                        stopService();
                        mService.removeLocationUpdates();
                        this.finish();
                    }
                }
            }
        }
        else if(numD == Calendar.TUESDAY) {//MARTES
            if(status_h) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
                notificacion_eventos();
            }
            else{
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                statusitem(true);
                notificacion_eventos();
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
                    if (Logout()){
                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        unregisterReceiver(myReceiver_principal);
                        Intent intent = new Intent(MainActivity.this,Login.class);
                        startActivity(intent);
                        stopService();
                        mService.removeLocationUpdates();
                        this.finish();
                    }
                }
//                else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.WEDNESDAY) {//MIERCOLES
            if(status_h) {
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
                notificacion_eventos();
            }else{
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                statusitem(true);
                notificacion_eventos();
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
                    if (Logout()){
                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        unregisterReceiver(myReceiver_principal);
                        Intent intent = new Intent(MainActivity.this,Login.class);
                        startActivity(intent);
                        stopService();
                        mService.removeLocationUpdates();
                        this.finish();
                    }
                }
//                else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.THURSDAY) {//JUEVES
            if(status_h) {
                //.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
                notificacion_eventos();
            }else{
              //  Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                statusitem(true);
                notificacion_eventos();

                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
                    if (Logout()){
                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        unregisterReceiver(myReceiver_principal);
                        Intent intent = new Intent(MainActivity.this,Login.class);
                        startActivity(intent);
                        stopService();
                        mService.removeLocationUpdates();
                        this.finish();
                    }
                }
//                else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.FRIDAY) {//VIERNES
            if(status_h) {
               // Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
                notificacion_eventos();
            }else{
//                Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                statusitem(true);
                statusitem(true);
                notificacion_eventos();
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                if((isHourInInterval2(hora_actual,hora_final2)) || (isHourInInterval3(hora_actual,hora_inicial))){
                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
                    if (Logout()){
                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        unregisterReceiver(myReceiver_principal);
                        Intent intent = new Intent(MainActivity.this,Login.class);
                        startActivity(intent);
                        stopService();
                        mService.removeLocationUpdates();
                        this.finish();
                    }
                }
//                else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
        else if(numD == Calendar.SATURDAY) {//SABADO
            if(status_h_sab) {//el horario cambia
                //Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
                statusitem(false);
                notificacion_eventos();
//                verificaInternet();
            }else{
                statusitem(true);
                notificacion_eventos();
                //Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
                if((isHourInInterval2(hora_actual,hora_final2_sab)) || (isHourInInterval3(hora_actual,hora_inicial_sab))){
                    // Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
                    if (Logout()){
                        //Toast.makeText(getApplicationContext(), "Cession Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        Toasty.error(this, "Sesión Cerrada, No se Encuentra en su Periodo Laboral", Toast.LENGTH_LONG).show();
                        unregisterReceiver(myReceiver_principal);
                        Intent intent = new Intent(MainActivity.this,Login.class);
                        startActivity(intent);
                        stopService();
                        mService.removeLocationUpdates();
                        this.finish();
                    }
                }
//                else{
//                    Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
//                }
            }
        }
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        registerReceiver(myReceiver_principal,nIntentFilter);
       // registerReceiver(myReceiver_principal_ubicacion,nIntentFilter_ubicacion);
    }

    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver_principal_ubicacion);//segundo plano
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));

//        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver_principal_ubicacion,
//                new IntentFilter(GPS_Service.ACTION_BROADCAST));

        registerReceiver(myReceiver_principal,nIntentFilter);
        //registerReceiver(myReceiver_principal_ubicacion,nIntentFilter_ubicacion);

    }
    @Override
    protected void onStop() {
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mService.requestLocationUpdates();
            }
//            else {
//                Toast.makeText(getApplicationContext(), "Se requieren Permisos", Toast.LENGTH_SHORT).show();
//            }
        }
    }

    private void save_cood(String lat, String lon){
        new LoginPrefences(this).save_cood(lat, lon);
    }

    /**
     * Receiver for broadcasts sent by {@link GPS_Service}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
                //verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                //save_cood(Utils.getlatitud(location), Utils.getlongitud(location));
//                help_coordenadas.setLat(Utils.getlatitud(location));
//                help_coordenadas.setLon(Utils.getlongitud(location));
                System.out.println("=====================> " + Utils.getlatitud(location)+" /*/ "+Utils.getlongitud(location) + " / " + Get_Hours.getHora("hh:mm a"));
            }
        }
    }
    //se verifica si tiene acceso a internet
    public void verifica_internet(String c1, String c2){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();

        // RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
//          StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //get_url_photo();
                            verifica_mi_ubicacion(c1, c2);//se verifica mi ubicacion
                            guardartrayectoria(id_user,c1, c2,emp);
                        }
                        else {
                            //Toast.makeText(MainActivity.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //cargar_imguser_local();
                            guardartrayectorialocal(id_user,c1, c2,emp);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            //cargar_imguser_local();
            guardartrayectorialocal(id_user,c1, c2,emp);
        }) {
        };
        //queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
//    public void veri_mi_ubicacion(){
//        if(help_coordenadas.getLat().isEmpty() || help_coordenadas.getLat() == null && help_coordenadas.getLon().isEmpty() || help_coordenadas.getLon() == null) {
//            System.out.println("/////// " + help_coordenadas.getLat() +" ---- "+ help_coordenadas.getLon());
//            System.out.println("/////// " + p.Getlat() +" ---- "+ p.Getlon() + "=== " + Get_Hours.getHora("hh:mm a"));
//            //mi_ubicacion(p.Getlat(),p.Getlon());
//        }
//        else{
//            System.out.println("//////// 2 " + help_coordenadas.getLat() +" ---- "+ help_coordenadas.getLon());
//            System.out.println("///////// 2" + p.Getlat() +" ---- "+ p.Getlon() + "=== " + Get_Hours.getHora("hh:mm a"));
//           // mi_ubicacion(help_coordenadas.getLat(), help_coordenadas.getLon());
//        }
//    }
//    public void mi_ubicacion(String latitud, String longitud){
////        System.out.println("/////// " + help_coordenadas.getLat() +" ---- "+ help_coordenadas.getLon());
////        System.out.println("/////// " + p.Getlat() +" ---- "+ p.Getlon() + "=== " + Get_Hours.getHora("hh:mm a"));
//        //se verifica la posicion actual
//        //  verifica_mi_ubicacion(p.Getlat(), p.Getlon());
//        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
//                response -> {
//                    try {
//                        JSONObject objResultado = new JSONObject(response);
//                        if(objResultado.get("internet").toString().equals("yes")) {
//                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
//                            verifica_mi_ubicacion(latitud,longitud);
//                        }
//                        else {
//                            //Toast.makeText(MainActivity.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
//                          System.out.println("No hay Acceso a Intenert");
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }, error -> {System.out.println("No hay Acceso a Intenert");}) {
//        };
//
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
//    }
    //*******************************************************************************************************
    //horarios de Lunes a Viernes
    public void horario_trabajo(){
//        h se utiliza para las horas AM / PM (1-12).
//        H se utiliza durante 24 horas (1-24).
//        a es el marcador AM / PM
        //se establece la hora
        String hora_actual = Get_Hours.getHora("hh:mm a");
        String hora_inicial = "10:06 a. m.";// 7:30 am
        String hora_final   = "10:07 a. m.";// 6:00 pm
        String hora_final2 = "10:01 a. m.";
        //Toast.makeText(getApplicationContext(),Get_Hours.getHora("hh:mm a"),Toast.LENGTH_LONG).show();

        System.out.println("Hora:  " + Get_Hours.getHora("hh:mm a"));
        if(isHourInInterval(hora_actual,hora_inicial,hora_final)){
            //System.out.println("\n\n RANGO \n\n");
            Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
            statusitem(false);
        }else {
            Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
            // System.out.println("\n\n FUERA \n\n");
            statusitem(true);

//            if(isHourInInterval2(hora_actual,hora_final2)){
//                //Toast.makeText(getApplicationContext(), "......Fuera........", Toast.LENGTH_SHORT).show();
//                if (Logout()){
//                    Toast.makeText(getApplicationContext(), "Cession Cerrada", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(MainActivity.this,Login.class);
//                    startActivity(intent);
//                    stopService();
//                    mService.removeLocationUpdates();
//                    this.finish();
//                }
//            }
        }
    }
    public void horario_trabajo_sab(){
        //se establece la hora para el dia sabado
        String hora_actual = Get_Hours.getHora("hh:mm a");
        String hora_inicial = "08:44 am";//7:30 am
        String hora_final   = "08:45 am"; // 1:00 pm
        if(isHourInInterval(hora_actual,hora_inicial,hora_final)){
            //System.out.println("\n\n RANGO \n\n");
            Toast.makeText(getApplicationContext(), "RANGO", Toast.LENGTH_SHORT).show();
            statusitem(false);
        }
        else {
            Toast.makeText(getApplicationContext(), "FUERA DE RANGO", Toast.LENGTH_SHORT).show();
            // System.out.println("\n\n FUERA \n\n");
            statusitem(true);
        }
    }
    //se compara la hora actual con la hora inicio y final
    public static boolean isHourInInterval(String target, String start, String end) {
        return ((target.compareTo(start) >= 0)&& (target.compareTo(end) <= 0));
    }

    public static boolean isHourInInterval2(String target, String end) {
        return ((target.compareTo(end) >= 0));
    }

    public static boolean isHourInInterval3(String target, String start) {
        return ((target.compareTo(start) <= 0));
    }

    public void statusitem(boolean status) {
        if (menu2 != null) {
            menu2.findItem(R.id.logout).setVisible(status);
        }
    }

    //*******************************************************************************************************
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    //*************************menu*************************
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        this.menu2 = menu;
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent intent2 = new Intent(this, GPS_Service.class);
        intent2.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, false);

        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent2,
                PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        if (id == R.id.logout)
        {
            if (Logout()){
                update_android_id();
                Toasty.error(this, "Sesión Cerrada", Toast.LENGTH_LONG).show();
                unregisterReceiver(myReceiver_principal);
                servicePendingIntent.cancel();
                BD.closeDB();
                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivity(intent);
                stopService();
                mService.removeLocationUpdates();
                this.finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }
    //
    public void update_android_id(){
        BD.get_idmovilidad(prueba_var);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/save_android_id.php",
            response -> {
                try {
                    JSONObject objResultado = new JSONObject(response);
                    if (objResultado.get("state").toString().equals("Success")) {
                        //Toasty.success(this, "Actualizado con Exito", Toast.LENGTH_LONG).show();
                        System.out.println("Actualizado con Exito");
                    }
                    else {
                        Toasty.info(this, "No se Actualizo", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }, error -> Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show()) {
        @Override
        protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<>();
            params.put("id", prueba_var.getId_movilidad());
            params.put("android_id", "");
            return params;
        }
    };
    // queue.add(stringRequest);
    stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    public boolean Logout(){
        SharedPreferences sp = this.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
//        editor.commit();//delete all in sharedpreferences
        return true;
    }

    public boolean Logout2(){
        SharedPreferences sp = this.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
        editor.commit();//delete all in sharedpreferences
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    //se valida la ubicacion actuual
    public void verifica_mi_ubicacion(String latitud, String longitud){
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    if(valida.equals("Fuera")){
                                        send_correo(latitud,longitud);
                                        //enviar_mensaje(latitud,longitud);
                                    }
                                    System.out.println("========= > "+ valida + " //// "+Get_Hours.getHora("hh:mm a"));
                                    i++;
                                }
                                break;
                            case "2":
                                String mensaje2 = objResultado.getString("mensaje");
                                Toasty.error(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toasty.error(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.info(this, "Conexion Invalida - Alerta", Toast.LENGTH_LONG).show()){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);//Change
                params.put("latitud", latitud);//12.119639
                params.put("longitud", longitud);//-86.311440
                params.put("idemp", id_emp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //se envia el correo
    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                        System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
                //params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola " + nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(mobil_alert, null, text , null, null);
    }
    //permiso para los mensaje de texto
    private void checkSMSStatePermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.SEND_SMS);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i("Mensaje", "No se tiene permiso para enviar SMS.");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 225);
        } else {
            Log.i("Mensaje", "Se tiene permiso para enviar SMS!");
        }
    }
    //permiso del GPS en la APP
    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
        }
    }
//    public void timewait(int milisegundos) {
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                // acciones que se ejecutan tras los milisegundos
//                checkLocationPermission2();
//            }
//        }, milisegundos);
//    }
//    public void checkLocationPermission2() {
//        if (ContextCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED &&
//                ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.ACCESS_COARSE_LOCATION)
//                        != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(MainActivity.this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
//                    99); }
//        else {
//            // No explanation needed, we can request the permission.
//            Log.i("Mensaje", "Se tiene permiso para el Gps!");
//        }
//    }

    public void verificaIntenet_session(){
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            valida_session_android_id();
                        }else {
                            System.out.println("NO HAY ACCESO A INTERTET");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                    System.out.println("ERROR EN LA CONEXION");
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //se verifica la session del android id
    public void valida_session_android_id()
    {
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_login.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("state").toString();
                        if (estadox.equals("No Encontrado")) {
                            System.out.println("====================> NO VALIDO" );
                        } else if (estadox.equals("ERROR")) {
                            Toasty.error(this, "Faltan Datos Por Ingresar", Toast.LENGTH_LONG).show();
                        } else {
                            String android_id = objResultado.getString("android_id");
                            //si el android_id no es igual al de local
                            if(!android_id_user.equals(android_id))
                            {
                                Intent intent2 = new Intent(this, GPS_Service.class);
                                intent2.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, false);

                                PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent2,
                                        PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);
                                if(Logout2())
                                {
                                    Toasty.error(this, "Sesión Cerrada", Toast.LENGTH_LONG).show();
                                    BD.closeDB();
                                    unregisterReceiver(myReceiver_principal);
                                    servicePendingIntent.cancel();
                                    Intent intent = new Intent(MainActivity.this,Login.class);
                                    startActivity(intent);
                                    stopService();
                                    mService.removeLocationUpdates();
                                    this.finish();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> System.out.println("ERROR")) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<>();
                    params.put("username", nombre_user);
                    params.put("password", password_user);
                    return params;
                }
        };
        //queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void consultar_cliente()
    {
        //se obtiene el id y nombre de los clientes
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/get_id_cliente.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        // Obtener atributo "estado"
                        String state = objResultado.getString("state");
                        switch (state)
                        {
                            case "1": // EXITO
//                                // Obtener array "metas" Json
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;

                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String id =object1.getString("Id_cliente");

                                    //StandardCharsets -> Permite palabras con  Acentos
                                    String p_nombre = new String(object1.getString("first_name").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                    String s_nombre = new String(object1.getString("second_name").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                    String p_apellido = new String(object1.getString("surname1").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                    String s_apellido = new String(object1.getString("surname2").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);


                                    String nombre_completo = p_nombre + " " + s_nombre  + " " + p_apellido  + " " + s_apellido;

                                   if(BD.verifica_namecliente(id)){
                                       BD.update_name_cliente(id,nombre_completo);
                                   }else{
                                       if(!BD.verifica_visitacliente(id,nombre_completo)){
                                           BD.insert_tb_visitacliente(id,nombre_completo);
                                       }
                                   }
                                    i++;
                                }
                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "No Tiene Acceso a Intenet - GET ID CLIENTE", Toast.LENGTH_LONG).show()){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("idemp",id_emp);
                //params.put("idusuario",id_user);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void consultar_doc2(){
        try {
            stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/id_type_doc.php",
                    response -> {
                        try {
                            JSONObject objResultado = new JSONObject(response);
                            // Obtener atributo "estado"
                            String state = objResultado.getString("state");
                            switch (state) {
                                case "1": // EXITO
//                                // Obtener array "metas" Json
                                    JSONArray array = objResultado.getJSONArray("respuesta");
                                    int i = 0;

                                    while (i<array.length())
                                    {
                                        JSONObject object1=array.getJSONObject(i);
                                        String id =object1.getString("idtype");
                                        //String nombre =object1.getString("name");

                                        String nombre = new String(object1.getString("name").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

                                        System.out.println("Guardando ======> " + nombre);

                                        try {
                                            if(BD.verifica_doc_1(id)){
                                                BD.update_typedoc(id,nombre);
                                            }else{
                                                if(!BD.verifica_doc(id,nombre)){
                                                    BD.insert_tb_doc(id,nombre);
                                                }
                                            }
                                        }catch (NullPointerException e){
                                            throw new  IllegalStateException("ERROR EN LA FUNCION Consultar_doc2 - verifica_doc");
                                        }

                                        i++;
                                    }

                                    break;
                                case "2": // FALLIDO
                                    String mensaje2 = objResultado.getString("mensaje");
                                    Toast.makeText(
                                            this,
                                            mensaje2,
                                            Toast.LENGTH_LONG).show();
                                    break;
                                default:
                                    Toast.makeText(
                                            this,
                                            "el valor de state es: " + state,
                                            Toast.LENGTH_LONG).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> Toasty.warning(this, "Verifique su Conexion a internet", Toast.LENGTH_LONG).show()) {
            };
            //queue.add(stringRequest);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION Consultar_doc2");
        }
    }
}