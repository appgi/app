package com.gicapp.gicapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.sql.BDAdapter;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import es.dmoral.toasty.Toasty;

public class Registrar_cliente extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    TextInputLayout input_cedula,input_ruc,p_nombre,s_nombre,p_apellido,s_apellido,telefono1,telefono2,mail,direccion,input_lat,input_lon;
    EditText campo_latitud,campo_longitud,campo_s_nombre,campo_p_apellido,campo_s_apellido;
    String id_user,emp,idtype,nombre_user,telefono_user,mobil_alert,mail_alert;
    MaskedEditText campocedula,camporuc;
    LoginPrefences p;
    Button btn_registrar;

    //spinner local
    ArrayList<String> listaPersonas;
    ArrayList<tipo_doc> personasList;

    //spinner
    ArrayList<String> listado;//funciona
    private JSONArray result;
    Spinner spinner;


    public static final String REGEX_EMAIL ="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
    public static final String REGEX_clientename ="^[a-zA-ZÀ-ÿ\\s]{1,40}$";
    private final String LETRAS = "ABCDEFGHJKLMNPQRSTUVWXY";

    RadioGroup radioGroup;
    RadioButton radioButton;

    help_coordenadas help_coordenadas;
    ValidarCedula validarCedula;
    boolean valida_doc,verifica_tipo;
    ProgressDialog progreso;
    //****************************************************
    public static BDAdapter BD;

    private MyReceiver myReceiver2;
    private Gson gson = new Gson();

    private boolean mBound = false;
    private GPS_Service mService = null;

    //volley
    StringRequest stringRequest;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_cliente);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        //*****************
        BD = new BDAdapter(this);
        BD.openDB();
        myReceiver2 = new MyReceiver();

        p = new LoginPrefences(getApplicationContext());
        if (!p.isEmpty()){
            id_user = p.Getid();
            emp = p.GetEmp();
            nombre_user = p.GetName();
            telefono_user = p.GetTelefono();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
        }


        listado =new ArrayList<String>();

        validarCedula = new ValidarCedula();
        help_coordenadas = new help_coordenadas();
        //********** Casting
        radioGroup = findViewById(R.id.radioGroup);
        input_cedula = findViewById(R.id.input_natural);
        input_ruc = findViewById(R.id.input_juridica);
        p_nombre = findViewById(R.id.input_pnombre);
        s_nombre = findViewById(R.id.input_snombre);
        p_apellido = findViewById(R.id.input_papeellido);
        s_apellido = findViewById(R.id.input_s_apellido);
        mail = findViewById(R.id.input_mail);
        direccion = findViewById(R.id.input_address);
        telefono1 = findViewById(R.id.input_telefono1);
        telefono2 = findViewById(R.id.input_telefono2);
        campo_latitud = findViewById(R.id.lat_cliente);
        campo_longitud = findViewById(R.id.lon_cliente);
        input_lat = findViewById(R.id.input_lat_cliente);
        input_lon = findViewById(R.id.input_lon_cliente);

        campo_s_nombre = findViewById(R.id.s_nombre);
        campo_p_apellido = findViewById(R.id.p_apellido);
        campo_s_apellido = findViewById(R.id.s_apellido);

        btn_registrar = findViewById(R.id.btn_registar);

        //se oculta el input juridico
        input_ruc.setVisibility(View.GONE);
        spinner = findViewById(R.id.spinner1);

        //consultar_doc2();
        consultar_id_documento();

        if(help_coordenadas.getLat() == null || help_coordenadas.getLat().isEmpty() && help_coordenadas.getLon() == null || help_coordenadas.getLon().isEmpty()){
            campo_latitud.setText(p.Getlat());
            campo_longitud.setText(p.Getlon());
        }
        else{
            campo_latitud.setText(help_coordenadas.getLat());
            campo_longitud.setText(help_coordenadas.getLon());
        }
        campo_latitud.setEnabled(false);
        campo_longitud.setEnabled(false);
        input_lat.setVisibility(View.GONE);
        input_lon.setVisibility(View.GONE);

        //*************************** verifica internet *************************************
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 || tipoConexion2) {
                //********************************* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                //Toast.makeText(getApplicationContext(), "hay internet ", Toast.LENGTH_SHORT).show();
                //*******************************************
                btn_registrar.setOnClickListener(view -> {
                    int radioId = radioGroup.getCheckedRadioButtonId();
                    radioButton = findViewById(radioId);

                    if(radioButton.getText().equals("Cliente Natural")){//ruc
                        if(!validate_p_nombre() | !validate_s_nombre() | !validate_p_apellido() | !validate_s_apellido()){
                            return;
                        }
                        verifica_tipo = true;
                    }else if(radioButton.getText().equals("Cliente Juridico")){//cedula
                        if(!validate_p_nombre()){
                            return;
                        }
                        verifica_tipo = false;
                    }

                    //se valida que el spinner este seleccionado
                    if(!valida_spinner())
                        return;

                    if(valida_doc){
                        if(!validate_ruc()){
                            return;
                        }
                    }else{
                        if(!validate_cedula()){
                            return;
                        }
                    }
                    if(!validate_direccion() | !validateEmail() | !validatecellphone() | !validatecellphone2()){
                        return;
                    }
                    verifica_internet();
                    //Toasty.success(this, "enviado", Toast.LENGTH_LONG).show();
                });
            }
        }
        else {
            //Toast.makeText(getApplicationContext(), "Favor encender el WIFI ó Datos Moviles", Toast.LENGTH_LONG).show();
            /* ******************************** No estas conectado a internet********************************  */
            //Toast.makeText(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_LONG).show();
            Toasty.info(this, "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_SHORT).show();
            btn_registrar.setOnClickListener(view -> {
                int radioId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(radioId);

                if(radioButton.getText().equals("Cliente Natural")){//ruc
                    if(!validate_p_nombre() | !validate_s_nombre() | !validate_p_apellido() | !validate_s_apellido()){
                        return;
                    }
                    verifica_tipo = true;
                }else if(radioButton.getText().equals("Cliente Juridico")){//cedula
                    if(!validate_p_nombre()){
                        return;
                    }
                    verifica_tipo = false;
                }
                //se valida que el spinner este seleccionado
                if(!valida_spinner())
                    return;

                if(valida_doc){
                    if(!validate_ruc()){
                        return;
                    }
                }else{
                    if(!validate_cedula()){
                        return;
                    }
                }
                if(!validate_direccion() | !validateEmail() | !validatecellphone() | !validatecellphone2()){
                    return;
                }
                guarda_cliente_local();
                //Toasty.success(this, "enviado", Toast.LENGTH_LONG).show();
            });


        }

        //Collections.sort(listaPersonas, String.CASE_INSENSITIVE_ORDER);
        ArrayAdapter<CharSequence> adaptador=new ArrayAdapter(this, R.layout.item_spinner,listaPersonas);
        spinner.setAdapter(adaptador);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long idl) {

                if (position!=0){
                    //System.out.println("======> "+ personasList.get(position-1).getId() + " / " +personasList.get(position-1).getNombre());
                    if(personasList.get(position-1).getNombre().equals("Cedula")){
                        input_cedula.setVisibility(View.VISIBLE);
                        input_ruc.setVisibility(View.GONE);
                        valida_doc = false;
                        idtype = personasList.get(position-1).getId();
                        System.out.println("=====>" + personasList.get(position-1).getId() + " / / " +personasList.get(position-1).getNombre());
                    }else if(personasList.get(position-1).getNombre().equals("RUC")){
                        input_cedula.setVisibility(View.GONE);
                        input_ruc.setVisibility(View.VISIBLE);
                        valida_doc = true;
                        idtype = personasList.get(position-1).getId();
                        System.out.println("=====>" + personasList.get(position-1).getId() + " // " +personasList.get(position-1).getNombre());
                    }else if(personasList.get(position-1).getNombre().equals("Cédula")){
                        input_cedula.setVisibility(View.VISIBLE);
                        input_ruc.setVisibility(View.GONE);
                        valida_doc = false;
                        idtype = personasList.get(position-1).getId();
                        System.out.println("=====>" + personasList.get(position-1).getId() + " / / " +personasList.get(position-1).getNombre());
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void checkButton(View v) {
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);

        if(radioButton.getText().equals("Cliente Natural")){
            p_nombre.setVisibility(View.VISIBLE);
            s_nombre.setVisibility(View.VISIBLE);
            p_apellido.setVisibility(View.VISIBLE);
            s_apellido.setVisibility(View.VISIBLE);
        }else if(radioButton.getText().equals("Cliente Juridico")){
            s_nombre.setVisibility(View.GONE);
            p_apellido.setVisibility(View.GONE);
            s_apellido.setVisibility(View.GONE);
//                campo_s_nombre.setEnabled(false);
//                campo_p_apellido.setEnabled(false);
//                campo_s_apellido.setEnabled(false);
        }
    }

    //***************************************** spinner local
    public void consultar_doc2(){
        try {
            stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/id_type_doc.php",
                    response -> {
                        try {
                            JSONObject objResultado = new JSONObject(response);
                            // Obtener atributo "estado"
                            String state = objResultado.getString("state");
                            switch (state) {
                                case "1": // EXITO
//                                // Obtener array "metas" Json
                                    JSONArray array = objResultado.getJSONArray("respuesta");
                                    int i = 0;

                                    while (i<array.length())
                                    {
                                        JSONObject object1=array.getJSONObject(i);
                                        String id =object1.getString("idtype");
                                        //String nombre =object1.getString("name");

                                        String nombre = new String(object1.getString("name").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

                                        System.out.println("Guardando ======> " + nombre);

                                        try {
                                            if(BD.verifica_doc_1(id)){
                                                BD.update_typedoc(id,nombre);
                                            }else{
                                                if(!BD.verifica_doc(id,nombre)){
                                                    BD.insert_tb_doc(id,nombre);
                                                }
                                            }
                                        }catch (NullPointerException e){
                                            throw new  IllegalStateException("ERROR EN LA FUNCION Consultar_doc2 - verifica_doc");
                                        }

                                        i++;
                                    }

                                    break;
                                case "2": // FALLIDO
                                    String mensaje2 = objResultado.getString("mensaje");
                                    Toast.makeText(
                                            this,
                                            mensaje2,
                                            Toast.LENGTH_LONG).show();
                                    break;
                                default:
                                    Toast.makeText(
                                            this,
                                            "el valor de state es: " + state,
                                            Toast.LENGTH_LONG).show();
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> Toasty.warning(this, "Verifique su Conexion a internet", Toast.LENGTH_LONG).show()) {
            };
            //queue.add(stringRequest);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION Consultar_doc2");
        }
    }

//    public void consultar_doc2(){
//       // RequestQueue queue = Volley.newRequestQueue(Registrar_cliente.this);
//        //StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE2 + "id_type_doc.php",
//        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/id_type_doc.php",
//                response -> {
//                    try {
//                        JSONObject objResultado = new JSONObject(response);
//                        // Obtener atributo "estado"
//                        String state = objResultado.getString("state");
//                        switch (state) {
//                            case "1": // EXITO
////                                // Obtener array "metas" Json
//                                JSONArray array = objResultado.getJSONArray("respuesta");
//                                int i = 0;
//
//                                while (i<array.length())
//                                {
//                                    JSONObject object1=array.getJSONObject(i);
//                                    String id =object1.getString("idtype");
//                                    //String nombre =object1.getString("name");
//
//                                    String nombre = new String(object1.getString("name").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
//
//                                    System.out.println("Guardando ======> " + nombre);
//
//                                    if(!BD.verifica_doc(id,nombre)){
//                                        BD.insert_tb_doc(id,nombre);
//                                    }
//                                    i++;
//                                }
//
//                                break;
//                            case "2": // FALLIDO
//                                String mensaje2 = objResultado.getString("mensaje");
//                                Toast.makeText(
//                                        this,
//                                        mensaje2,
//                                        Toast.LENGTH_LONG).show();
//                                break;
//                            default:
//                                Toast.makeText(
//                                        this,
//                                        "el valor de state es: " + state,
//                                        Toast.LENGTH_LONG).show();
//                                break;
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }, error -> Toasty.warning(this, "Verifique su Conexion a internet", Toast.LENGTH_LONG).show()) {
//        };
//        //queue.add(stringRequest);
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
//    }

    public void consultar_id_documento(){
        tipo_doc persona=null;
        personasList =new ArrayList<tipo_doc>();

        Cursor cursor =  BD.getData("SELECT * FROM type_doc");
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                persona=new tipo_doc();

                persona.setId(cursor.getString(1));
                persona.setNombre(cursor.getString(2));

                System.out.println("=======> "+cursor.getString(1)+" / "+cursor.getString(2));

                personasList.add(persona);
            }
        }

        obtenerLista();
    }
    private void obtenerLista() {
        listaPersonas=new ArrayList<String>();
        listaPersonas.add("Seleccione");

       // Collections.sort(listaPersonas);

        for(int i=0;i<personasList.size();i++){
           // Collections.sort(listaPersonas, String.CASE_INSENSITIVE_ORDER);
            listaPersonas.add(personasList.get(i).getNombre());
        }

    }

    //****************************************** validacion campos  ******************************************

    public boolean valida_spinner(){
        if(spinner.getSelectedItemPosition() == 0){
            Toasty.error(this, "Seleccione un Tipo Dcocumento", Toast.LENGTH_LONG).show();
            return false;
        }else{
            return true;
        }
    }

    public boolean validate_cedula(){
        validarCedula.setCedula(input_cedula.getEditText().getText().toString().trim());

        String cedulaInput = input_cedula.getEditText().getText().toString().trim();

        if (cedulaInput.isEmpty()) {
            input_cedula.setError("Campo vacio");
            Toasty.error(this, "Campo Cedula Vacio", Toast.LENGTH_LONG).show();
            return false;
        }else if(cedulaInput.equals("###--#####-#-###")){
            input_cedula.setError("Campo vacio");
            Toasty.error(this, "Campo Cedula Vacio", Toast.LENGTH_LONG).show();
            return false;
        }else if(!validarCedula.isCedulaValida()){
            input_cedula.setError("Cedula Incorrecta");
            Toasty.error(this, "Cedula Incorrecta", Toast.LENGTH_LONG).show();
            return false;
        }else {
            input_cedula.setError(null);
            return true;
        }
    }

    public boolean validate_ruc(){
        String rucInput = input_ruc.getEditText().getText().toString().trim();
        // validarCedula.setCedula(cedulaInput);
        if (rucInput.isEmpty()) {
            input_ruc.setError("Campo vacio");
            Toasty.error(this, "Campo Ruc Vacio", Toast.LENGTH_LONG).show();
            return false;
        }else if(rucInput.equals("##############")){
            input_ruc.setError("Campo vacio");
            Toasty.error(this, "Campo Ruc Vacio", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            input_ruc.setError(null);
            return true;
        }
    }

    private boolean validate_p_nombre() {
        String clienteInput = p_nombre.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_clientename);

        if (clienteInput.isEmpty()) {
            p_nombre.setError("Campo vacio");
            Toasty.error(this, "Campo Primer Nombre vacio", Toast.LENGTH_LONG).show();
            return false;
        }else if(!patron.matcher(clienteInput).matches()){
            p_nombre.setError("Primer Nombre invalido");
            Toasty.error(this, "Primer Nombre invalido", Toast.LENGTH_LONG).show();
            return false;
        } else {
            p_nombre.setError(null);
            return true;
        }
    }
    private boolean validate_s_nombre() {
        String clienteInput = s_nombre.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_clientename);

        if (clienteInput.isEmpty()) {
            s_nombre.setError("Campo vacio");//**
            Toasty.error(this, "Campo Segundo Nombre vacio", Toast.LENGTH_LONG).show();//**
            return false;
        }else if(!patron.matcher(clienteInput).matches()){
            s_nombre.setError("Segundo Nombre invalido");
            Toasty.error(this, "Segundo Nombre invalido", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            s_nombre.setError(null);
            return true;
        }
    }

    private boolean validate_p_apellido() {
        String clienteInput = p_apellido.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_clientename);

        if (clienteInput.isEmpty()) {
            p_apellido.setError("Campo vacio");
            Toasty.error(this, "Campo Primer Apellido vacio", Toast.LENGTH_LONG).show();//**
            return false;
        }else if(!patron.matcher(clienteInput).matches()){
            p_apellido.setError("Primer Apellido invalido");
            Toasty.error(this, "Primer Apellido invalido", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            p_apellido.setError(null);//***
            return true;
        }
    }

    private boolean validate_s_apellido() {
        String clienteInput = s_apellido.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_clientename);

        if (clienteInput.isEmpty()) {
            s_apellido.setError("Campo vacio");//**
            Toasty.error(this, "Campo Apellido vacio", Toast.LENGTH_LONG).show();//**
            return false;
        }else if(!patron.matcher(clienteInput).matches()){
            s_apellido.setError("Apellido invalido");
            Toasty.error(this, "Apellido invalido", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            p_apellido.setError(null);//***
            return true;
        }
    }

    private boolean validate_direccion() {
        String direcionInput = direccion.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_clientename);

        if (direcionInput.isEmpty()) {
            direccion.setError("Campo vacio");//**
            Toasty.error(this, "Campo Dirección Vacio", Toast.LENGTH_LONG).show();//**
            return false;
        }else if(!patron.matcher(direcionInput).matches())
        {
            direccion.setError("Dirección Invalida");
            Toasty.error(this, "Dirección Invalida", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            direccion.setError(null);//***
            return true;
        }
    }

    private boolean validateEmail() {
        String emailInput = mail.getEditText().getText().toString().trim();
        Pattern patron = Pattern.compile(REGEX_EMAIL);

        if(emailInput.isEmpty()){
            mail.setError(null);
            return true;
        }
        else if(!patron.matcher(emailInput).matches()){
            mail.setError("Correo Invalido");
            Toasty.error(this, "Correo Invalido", Toast.LENGTH_LONG).show();
            return false;
        }else {
            mail.setError(null);
            return true;
        }
    }

    private boolean validatecellphone()
    {
        String cellphoneInput = telefono1.getEditText().getText().toString().trim();

        if (cellphoneInput.isEmpty()) {
            telefono1.setError("Campo vacio");
            Toasty.error(this, "Complete el Campo Teléfono", Toast.LENGTH_LONG).show();
            return false;
        }else if (cellphoneInput.length() > 8) {
            telefono1.setError("Numero de Teléfono muy LARGO");
            Toasty.error(this, "Numero de Teléfono muy Largo", Toast.LENGTH_LONG).show();
            return false;
        }else if (cellphoneInput.length() <= 7) {
            telefono1.setError("Numero de Teléfono muy CORTO");
            Toasty.error(this, "Numero de Teléfono muy Corto", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!cellphoneInput.startsWith("8") && !cellphoneInput.startsWith("7") && !cellphoneInput.startsWith("5") && !cellphoneInput.startsWith("2")){
            telefono1.setError("Numero de Teléfono No valido");
            Toasty.error(this, "Numero de Teléfono No valido", Toast.LENGTH_LONG).show();
            return false;
        }else {
            telefono1.setError(null);
            return true;
        }
    }

    private boolean validatecellphone2()
    {
        String cellphoneInput = telefono2.getEditText().getText().toString().trim();
        if(cellphoneInput.isEmpty()){
            telefono2.setError(null);
            return true;
        }
        else if (cellphoneInput.length() > 8)
        {
            telefono2.setError("Numero de Teléfono muy LARGO");
            Toasty.error(this, "Numero de Teléfono 2 muy Largo", Toast.LENGTH_LONG).show();
            return false;
        }else if (cellphoneInput.length() <= 7)
        {
            telefono2.setError("Numero de Teléfono muy CORTO");
            Toasty.error(this, "Numero de Teléfono 2 muy Corto", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!cellphoneInput.startsWith("8") && !cellphoneInput.startsWith("7") && !cellphoneInput.startsWith("5") && !cellphoneInput.startsWith("2"))
        {
            telefono2.setError("Numero de Teléfono 2 No valido");
            Toasty.error(this, "Numero de Teléfono 2 No valido", Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            telefono2.setError(null);
            return true;
        }
    }
    //******************************************************************************************************************
    public void verifica_internet(){
        //RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                //  StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(Login.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            send_cliente();
                        }
                        else {
                            guarda_cliente_local();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error ->  guarda_cliente_local()){//Toasty.error(this, "Error en la Conexion - Internet", Toast.LENGTH_LONG).show()) {
        };
        //queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //******************************************************************************************************************
    public void send_cliente(){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        String tipo_documento,tipo, second_name,surn1,surn2;

        if(valida_doc){//ruc
            tipo = "J";
            tipo_documento = input_ruc.getEditText().getText().toString().trim();
        }else{//cedula
            tipo = "N";
            tipo_documento = input_cedula.getEditText().getText().toString().trim();
        }

        if(s_nombre.getEditText().getText().toString().trim().isEmpty())
            second_name = "";
        else second_name = s_nombre.getEditText().getText().toString().trim();

        if(p_apellido.getEditText().getText().toString().trim().isEmpty())
            surn1 = "";
        else surn1 = p_apellido.getEditText().getText().toString().trim();

        if(s_apellido.getEditText().getText().toString().trim().isEmpty())
            surn2 = "";
        else surn2 = s_apellido.getEditText().getText().toString().trim();

//        System.out.println(idtype +" / "+ emp + " / " + tipo +" / "+ tipo_documento +" / "+
//                p_nombre.getEditText().getText().toString().trim() + " / " + surn1 + " / " + surn2 + " / " +  direccion.getEditText().getText().toString().trim() +" / "+
//                mail.getEditText().getText().toString().trim() +" / "+ telefono1.getEditText().getText().toString().trim() +" / "+
//                input_lat.getEditText().getText().toString().trim() +" / "+ input_lon.getEditText().getText().toString().trim() + " / "+id_user +" / "+ fecha
//        );

        progreso=new ProgressDialog(Registrar_cliente.this);
        progreso.setMessage("Cargando...");
        progreso.show();
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/guardar_cliente.php",
                // stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE2 + "guardar_cliente.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "No se pudo registrar", Toast.LENGTH_LONG).show();
                        }else{
                            //get_id_form();
                            Toasty.success(this, "Se ha Registrado con Exito", Toast.LENGTH_LONG).show();
                            //retorna al main activity
                            Intent intent = new Intent(Registrar_cliente.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        //barra de progreso
                       progreso.hide();
                    }catch (JSONException e){
                        e.printStackTrace();
                       progreso.hide();
                    }
                }, error -> { guarda_cliente_local();}) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idemp",emp);
                params.put("idtype", idtype);//new
                params.put("tipo", tipo);
                params.put("documento",tipo_documento);

                params.put("p_nombre",p_nombre.getEditText().getText().toString().trim());
                params.put("s_nombre",second_name);
                params.put("surname1",surn1);
                params.put("s_apellido", surn2);

                params.put("direccion",direccion.getEditText().getText().toString().trim());
                params.put("mail",mail.getEditText().getText().toString().trim());
                params.put("telefono1",telefono1.getEditText().getText().toString().trim());
                params.put("telefono2",telefono2.getEditText().getText().toString().trim());

                params.put("lat",input_lat.getEditText().getText().toString().trim());
                params.put("lon",input_lon.getEditText().getText().toString().trim());

                params.put("state","Activo");
                params.put("idusuario",id_user);
                params.put("fecha",fecha);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    
    ///guardar cliente local
    public void guarda_cliente_local(){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        String tipo_documento,tipo,s_name="",surn1="",surn2="",t2="",m="";
        if(valida_doc){//ruc
            tipo = "J";
            tipo_documento = input_ruc.getEditText().getText().toString().trim();
        }else{//cedula
            tipo = "N";
            tipo_documento = input_cedula.getEditText().getText().toString().trim();
        }

        s_name = s_nombre.getEditText().getText().toString().trim();
        surn1 = p_apellido.getEditText().getText().toString().trim();
        surn2 = s_apellido.getEditText().getText().toString().trim();
        m = mail.getEditText().getText().toString().trim();
        t2 = telefono2.getEditText().getText().toString().trim();

//        System.out.println("====> "+ idtype + " / "+
//                tipo + " / "+
//                tipo_documento + " / "+
//                p_nombre.getEditText().getText().toString().trim()+ " / "+
//                s_name + " / "+
//                surn1 + " / "+
//                surn2 + " / "+
//                direccion.getEditText().getText().toString().trim() + " / "+
//                m + " / "+
//                telefono1.getEditText().getText().toString().trim() + " / "+
//                t2 + " / "+
//                input_lat.getEditText().getText().toString().trim() + " / "+
//                input_lon.getEditText().getText().toString().trim() + " / "+
//                emp + " / "+
//                id_user + " / "+
//                fecha);

        //se guarda los datos localmente
        BD.guardar_cliente(
                idtype,
                tipo,
                tipo_documento,
                p_nombre.getEditText().getText().toString().trim(),
                s_name,
                surn1,
                surn2,
                direccion.getEditText().getText().toString().trim(),
                m,
                telefono1.getEditText().getText().toString().trim(),
                t2,
                input_lat.getEditText().getText().toString().trim(),
                input_lon.getEditText().getText().toString().trim(),
                emp,
                id_user,
                fecha
        );

       // BD.closeDB();
        Toasty.success(this, "EL Cliente se ha Guardado Localmente", Toast.LENGTH_LONG).show();
        //retorna al main activity
        Intent intent = new Intent(Registrar_cliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }



    //******************************************************************************************************************
    //segundo plano
    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.cliente));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver2);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver2,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    @Override
    public boolean onSupportNavigateUp() {
//        onBackPressed();
        Intent intent = new Intent(Registrar_cliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
        return true;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(Registrar_cliente.this, Utils.getlatitud(location) + " /event/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
               // trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                help_coordenadas.setLat(Utils.getlatitud(location));
                help_coordenadas.setLon(Utils.getlongitud(location));

                System.out.println("==Registrar_cliente===> " + Utils.getlatitud(location)+" /*/ "+Utils.getlongitud(location) + " / " + Get_Hours.getHora("hh:mm a"));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){

        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes"))
                        {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //si hay internet las coordenadas se guardaran en el servidor
                            verifica_mi_ubicacion(lat, lon);
                            veri_trayectoria(lat, lon);
                        }
                        else {
                            //Toast.makeText(ListEvent.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //si no hay internet las coordenadas se guardaran localmente
                            veri_trayectoria_local(lat, lon);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            veri_trayectoria_local(lat, lon);
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //se valida que hay una posicion
    public void veri_trayectoria(String c1, String c2){
        if(c1 == null || c1.isEmpty() && c2 == null || c2.isEmpty())
            guardartrayectoria(p.Getlat(), p.Getlon());
        else
            guardartrayectoria(c1, c2);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String c1, String c2){
        //se obtiene la fecha actual
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//         StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", id_user);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //se valida que hay una posicion
    public void veri_trayectoria_local(String c1, String c2){
        if(c1 == null || c1.isEmpty() && c2 == null || c2.isEmpty())
            guardartrayectorialocal( p.Getlat(), p.Getlon());
        else
            guardartrayectorialocal(c1, c2);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String c1, String c2) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            BD.insert_trayect(id_user, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
           // BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifica_mi_ubicacion(String latitud, String longitud){
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    System.out.println("Respues ======> " + valida);

                                    if(valida.equals("Fuera")){
//                                        Toasty.error(this, "Fuera de su Trayectoria!!!!", Toast.LENGTH_LONG).show();
                                        enviar_mensaje(latitud,longitud);
                                        send_correo(latitud,longitud);
                                    }
                                    i++;
                                }

                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.info(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);
                params.put("latitud", latitud);//12.119639
                params.put("longitud", longitud);//-86.311440
                params.put("idemp", emp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola " + nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(mobil_alert, null, text , null, null);
        //Toasty.success(Formulario.this, "Mensaje enviado", Toast.LENGTH_LONG).show();
    }

    //se envia el correo
    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                    System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
//                params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Registrar_cliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }
}