package com.gicapp.gicapp.Services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Get_Hours;
import com.gicapp.gicapp.Login;
import com.gicapp.gicapp.LoginPrefences;
import com.gicapp.gicapp.R;
import com.gicapp.gicapp.VolleySingleton;
import com.gicapp.gicapp.sql.BDAdapter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class GPS_Service extends Service {
    private static final String PACKAGE_NAME = "com.gicapp.gicapp";
    private static final String TAG = GPS_Service.class.getSimpleName();
    private static final String CHANNEL_ID = "channel_01";

    public static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

    public static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";

    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    private final IBinder mBinder = new LocalBinder();

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
//    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
//    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
//            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 12345678;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;
    StringRequest stringRequest;
    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;

    /**
     * The current location.
     */
    private Location mLocation;
    int cont = 0;
    LoginPrefences p;
    String id_user,nombre_user,telefono_user,android_id_user,password_user,id_emp,mobil_alert,mail_alert;
    BDAdapter BD;
    String m_alert,sms_alert;
    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        BD = new BDAdapter(this);
        BD.openDB();

        p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
            nombre_user = p.GetName();
            telefono_user = p.GetTelefono();
            android_id_user = p.GetAndroid_id();
            password_user = p.Getpassword();
            id_emp  = p.GetEmp();
//            mobil_alert = p.GetAlert_mobil();
//            mail_alert = p.GetAlert_mail();
        }

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };

        createLocationRequest();
        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");
        boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
                false);

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates();
            stopSelf();
        }
        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()");
        stopForeground(true);
        mChangingConfiguration = false;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfiguration = false;
        super.onRebind(intent);
    }
    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && Utils.requestingLocationUpdates(this)) {
            Log.i(TAG, "Starting foreground service");

            startForeground(NOTIFICATION_ID, getNotification());
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        mServiceHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    @NonNull
    public void requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates");
        Utils.setRequestingLocationUpdates(this, true);
        startService(new Intent(getApplicationContext(), GPS_Service.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, false);
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Log.i(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            Utils.setRequestingLocationUpdates(this, false);
            stopSelf();
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, true);
            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, GPS_Service.class);

        CharSequence text = Utils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, Login.class), 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .addAction(R.drawable.ic_launcher_foreground, getString(R.string.launch_activity),
                        activityPendingIntent)
//                .addAction(R.drawable.ic_launcher_background, getString(R.string.remove_location_updates),
//                        servicePendingIntent)
                .setContentText(text)
                .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLocation = task.getResult();
                            } else {
                                Log.w(TAG, "Failed to get location.");
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }
    }

    private void onNewLocation(Location location) {
        Log.i(TAG, "New location: " + location);
        mLocation = location;

        BD.openDB();
       // System.out.println("====>>>> xd");

        if(isonline()) {
            if (location != null) {
                verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                save_cood(Utils.getlatitud(location), Utils.getlongitud(location));
            }else{
                System.out.println("====>>>> sad");
            }
        }
        else {
            if (location != null) {
                verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
                save_cood(Utils.getlatitud(location), Utils.getlongitud(location));
            }else{
                System.out.println("====>>>> sad");
            }
        }

        // Notify anyone listening for broadcasts about the new location.
        Intent intent = new Intent(ACTION_BROADCAST);
        intent.putExtra(EXTRA_LOCATION, location);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
            mNotificationManager.notify(NOTIFICATION_ID, getNotification());
        }
    }
    public boolean isonline(){
        ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager1.getActiveNetworkInfo();

        return mWifi != null && mWifi.isConnectedOrConnecting();
    }

    private void save_cood(String lat, String lon){
        new LoginPrefences(this).save_cood(lat, lon);
    }

    public void verifica_internet(String c1, String c2){

        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();

                            verifica_login();

                            verifica_mi_ubicacion(c1, c2);//se verifica mi ubicacion
                            guardartrayectoria(id_user,c1, c2,id_emp);
                        }
                        else {
                            //System.out.println("***** xd");
                            //Toast.makeText(MainActivity.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //cargar_imguser_local();
                            guardartrayectorialocal(id_user,c1, c2,id_emp);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> { System.out.println("*********************** error ****************************");
                    guardartrayectorialocal(id_user,c1, c2,id_emp);
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void verifica_login(){

        LoginPrefences p1 = new LoginPrefences(this);
        m_alert = p1.GetAlert_mail();
        sms_alert = p1.GetAlert_mobil();

        try {
            stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_login.php",
                    response -> {
                        try {
                            JSONObject objResultado = new JSONObject(response);
                            String estadox = objResultado.get("state").toString();
                            if (estadox.equals("No Encontrado")) {
                                System.out.println("====================> NO VALIDO" );
                            } else if (estadox.equals("ERROR")) {
                                Toasty.error(this, "Faltan Datos Por Ingresar", Toast.LENGTH_LONG).show();
                                //System.out.println("====================> FALTAN DATOS" );
                            } else {

//                                String alert_mobil = objResultado.getString("alert_mobil");
//                                String alert_mail = objResultado.getString("alert_mail");
                                if(!objResultado.getString("alert_mail").equals(""))
                                    if(!objResultado.getString("alert_mail").equals(m_alert))
                                        update_mail(objResultado.getString("alert_mail"));


                                if(!objResultado.getString("alert_mobil").equals(""))
                                    if(!objResultado.getString("alert_mobil").equals(sms_alert))
                                        update_mobil(objResultado.getString("alert_mobil"));


//                                if(!objResultado.getString("alert_mobil").equals(""))
//                                    update_mobil(objResultado.getString("alert_mobil"));
                                System.out.println("=UPDATE==> " + nombre_user + " / " + password_user + " / " + m_alert + " / " + sms_alert);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> Toasty.error(this, "Error en la Conexion - Login", Toast.LENGTH_LONG).show()) {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<>();
                    params.put("username", nombre_user);
                    params.put("password", password_user);
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);

        }catch (NullPointerException e){
            throw new  IllegalStateException("ERROR EN LA FUNCION VERIFICA_LOGIN");
        }
    }

    private void update_mobil(String mobil){
        new LoginPrefences(this).update_mobil(mobil);
    }

    private void update_mail(String mail){
        new LoginPrefences(this).update_mail(mail);
    }


    public void guardartrayectoria(String iduser,String c1, String c2, String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            //Toast.makeText(MainActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            //var = "error2";
                            System.out.println("Guardando Trayectoria en el Servidor..");
                            //Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> System.out.println("**************** error -st ***************")){//Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp",emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void guardartrayectorialocal(String iduser,String c1, String c2, String emp)
    {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try{
            BD.insert_trayect(iduser,c1,c2,fecha,emp);
            //Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            System.out.println("Guardando Trayectoria Localmente...");
            //BD.closeDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // ENVIO DE MENSAJE [ CORREO Y SMS]
    public void verifica_mi_ubicacion(String latitud, String longitud){
        LoginPrefences p2 = new LoginPrefences(this);
        mail_alert = p2.GetAlert_mail();
        mobil_alert = p2.GetAlert_mobil();

        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    if(valida.equals("Fuera")){
                                        if(!mail_alert.equals(""))
                                            send_correo(latitud,longitud);
                                        else
                                            System.out.println("======)) CORREO NO ENVIADO => "+mail_alert);

                                        if(!mobil_alert.equals(""))
                                            enviar_mensaje(latitud,longitud);
                                        else
                                            System.out.println("======)) SMS NO ENVIADO => "+ mobil_alert);
                                    }
                                   // enviar_mensaje(latitud,longitud);
                                    System.out.println("========= > "+ valida + " //// "+ Get_Hours.getHora("hh:mm a"));
                                    i++;
                                }
                                break;
                            case "2":
                                String mensaje2 = objResultado.getString("mensaje");
                                Toasty.error(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toasty.error(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.info(this, "Conexion Invalida - Alerta", Toast.LENGTH_LONG).show()){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);//Change
                params.put("latitud", latitud);//12.119639
                params.put("longitud", longitud);//-86.311440
                params.put("idemp", id_emp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //se envia el correo
    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                    System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
                params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola, El Usuario "+ nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(mobil_alert, null, text , null, null);
        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> parts = sms.divideMessage(text);
        sms.sendMultipartTextMessage(mobil_alert, null, parts, null, null);
//        System.out.println("===> "+parts);
    }

    /**
     * Sets the location request parameters.
     */

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(275 * 1000);
        mLocationRequest.setFastestInterval(270 * 1000); //360s equivalente a 6 minutos
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public GPS_Service getService() {
            return GPS_Service.this;
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }
}
