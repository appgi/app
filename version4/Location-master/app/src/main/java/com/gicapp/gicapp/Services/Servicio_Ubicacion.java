package com.gicapp.gicapp.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import com.gicapp.gicapp.MainActivity;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

public class Servicio_Ubicacion extends Service {
    LocationManager locationManager;
    String lat,lgn;
    private LocationCallback mLocationCallback;
    @Override
    public void onCreate() {
        super.onCreate();

    }

    public String obtengo_lat(Context c){
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                lat = Utils.getlatitud(locationResult.getLastLocation());
            }
        };
        return lat;
    }
    public String obtengo_lon(Context c){
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                lgn = Utils.getlongitud(locationResult.getLastLocation());
            }
        };
        return lgn;
    }

    @Override
    public int onStartCommand(Intent intencion, int flags, int idArranque) {
        handler.post(timeupdate);

        return START_NOT_STICKY;
    }

    Handler handler = new Handler();
    private final Runnable timeupdate = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(timeupdate, 10000 - SystemClock.elapsedRealtime() % 10000);//tiempo de ejecucion
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(MainActivity.BroadcastStringForAction2);
            broadcastIntent.putExtra("verifica_ubi", ""+obtengo_lat(Servicio_Ubicacion.this));
            broadcastIntent.putExtra("verifica_ubi2", ""+obtengo_lon(Servicio_Ubicacion.this));
            sendBroadcast(broadcastIntent);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;// throw new UnsupportedOperationException("not yet implement");//return null;
    }

}
