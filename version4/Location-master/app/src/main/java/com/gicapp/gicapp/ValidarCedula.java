package com.gicapp.gicapp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidarCedula {
    private final String LETRAS = "ABCDEFGHJKLMNPQRSTUVWXY";
    private String cedula;

//
//    public String getCedula() {
//        return cedula;
//    }

//    public void setCedula(String cedula) {
//        this.cedula = cedula;
//    }

    public boolean isCedulaValida(){
        if(!isPrefijoValido())
            return false;

        if(!isFechaValida())
            return false;

        if(!isSufijoValido())
            return false;

        if(!isLetraValida())
            return false;

        return true;
    }

    /**
     * Retorna true si el prefijo de la c&eacute;dula es v&aacute;lida
     * false en caso contrario
     *
     * @return
     */
    public boolean isPrefijoValido(){
        String prefijo = this.getPrefijoCedula();

        if(prefijo==null) return false;

        Pattern p = Pattern.compile("^[0-9]{3}$");
        Matcher m = p.matcher(prefijo);
        if (!m.find())
            return false;

        return true;
    }

    /**
     * Retorna true si la fecha de la c&eacute;dula es v&aacute;lida
     * false en caso contrario
     *
     * @return
     */
    public boolean isFechaValida(){
        String fecha = this.getFechaCedula();

        if(fecha == null) return false;

        Pattern p = Pattern.compile("^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2})$");
        Matcher m = p.matcher(fecha);
        if (!m.find())
            return false;

        // verificar si la fecha existe en el calendario
        try {
            DateFormat df = new SimpleDateFormat("ddMMyy");
            if(!fecha.equals(df.format(df.parse(fecha)))){
                return false;
            }
        } catch (ParseException ex) {
            return false;
        }

        return true;
    }

    /**
     * Retorna true si el sufijo de la c&eacute;dula es v&aacute;lida
     * false en caso contrario
     *
     * @return
     */
    public boolean isSufijoValido(){
        String sufijo = this.getSufijoCedula();

        if(sufijo == null) return false;

        Pattern p = Pattern.compile("^[0-9]{4}[A-Y]$");
        Matcher m = p.matcher(sufijo);
        if (!m.find())
            return false;

        return true;
    }

    /**
     * Retorna true si la letra de la c&eacute;dula es v&aacute;lida
     * false en caso contrario
     *
     * @return
     */
    public boolean isLetraValida(){
        String letraValida = null;
        String letra = this.getLetraCedula();

        if(letra == null) return false;

        letraValida = String.valueOf(this.calcularLetra());

        return letraValida.equals(letra);
    }

    /**
     * Retorna un entero que representa la posici&oacute;n en la cadena LETRAS
     * de la letra final de una c&eacute;dula
     *
     * @return
     */
    public int getPosicionLetra(){
        int posicionLetra = 0;
        String cedulaSinLetra = this.getCedulaSinLetra();
        long numeroSinLetra = 0;

        if(cedulaSinLetra == null) return -1;

        try{
            numeroSinLetra =  Long.parseLong(cedulaSinLetra);
        }catch(NumberFormatException e){
            return -1;
        }

        posicionLetra = (int)(numeroSinLetra - Math.floor((double)numeroSinLetra/23.0) * 23);

        return posicionLetra;
    }

    public char calcularLetra(){
        int posicionLetra = getPosicionLetra();

        if(posicionLetra <0 || posicionLetra >= LETRAS.length())
            return '?';

        return LETRAS.charAt(posicionLetra);
    }

    /**
     * Fiajr el N&uacute;mero de C&eacute;dula
     *
     * @param cedula N&uacute;mero de C&eacute;dula con o sin guiones
     */
    public void setCedula(String cedula) {
        cedula = cedula.trim().replaceAll("-","");

        if(cedula == null || cedula.length() != 14)
            this.cedula = null;
        else
            this.cedula = cedula.trim().replaceAll("-","").toUpperCase();
    }

    /**
     * Retorna el N&uacute;mero de C&eacute;dula
     * @return
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * Retorna el prefijo de la c&eacute;dula
     * @return
     */
    public String getPrefijoCedula(){
        if(cedula!=null)
            return cedula.substring(0,3);
        else
            return null;
    }

    /**
     * Retorna la fecha en la c&eacute;dula
     * @return
     */
    public String getFechaCedula(){
        if(cedula!=null)
            return cedula.substring(3,9);
        else
            return null;
    }

    /**
     * Retorna el sufijo en la c&eacute;dula
     * @return
     */
    public String getSufijoCedula(){
        if(cedula!=null)
            return cedula.substring(9,14);
        else
            return null;
    }

    /**
     * Retorna la letra de la c&eacute;dula
     * @return
     */
    public String getLetraCedula(){
        if(cedula!=null)
            return cedula.substring(13,14);
        else
            return null;
    }

    /**
     * Retorna la c&eacute;dula sin la letra de validaci&oacute;n
     * @return
     */
    public String getCedulaSinLetra(){
        if(cedula!=null)
            return cedula.substring(0,13);
        else
            return null;
    }
}
