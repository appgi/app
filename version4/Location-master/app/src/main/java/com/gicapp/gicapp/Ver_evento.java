package com.gicapp.gicapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.eventos.ListEvent;
import com.gicapp.gicapp.sql.BDAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class Ver_evento extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{

    TextView titulo,descripcion,fecha_inicio,fecha_final,detalle,estado,ttname_asignado,name_cliente;
    String idevento, id_user,nombre_cliente,nombre_user,telefono_user,id_emp,mobil_alert,mail_alert;

    public static BDAdapter BD;

    private MyReceiver myReceiver2;
    private Gson gson = new Gson();

    private boolean mBound = false;
    private GPS_Service mService = null;

    //volley
    StringRequest stringRequest;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_evento);

        BD = new BDAdapter(this);
        BD.openDB();

        //se obtiene el id del evento
        Bundle mibundle = this.getIntent().getExtras();
        if(mibundle!=null){
            idevento = mibundle.getString("idevento");
            nombre_cliente = mibundle.getString("nombre_cliente");
        }

        LoginPrefences p = new LoginPrefences(getApplicationContext());
        if (!p.isEmpty()){
            id_user = p.Getid();
            nombre_user = p.GetName();
            telefono_user = p.GetTelefono();
            id_emp = p.GetEmp();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
        }

        //**************************************************************************************
        myReceiver2 = new MyReceiver();
        //**************************************************************************************

        titulo = findViewById(R.id.txt_tituloevento);
        descripcion = findViewById(R.id.txt_descripcionevento);
        fecha_inicio = findViewById(R.id.txt_fechastar);
        fecha_final = findViewById(R.id.txt_fechaend);
        detalle = findViewById(R.id.txt_detalleevento);
        estado = findViewById(R.id.txt_estadoevento);

        ttname_asignado = findViewById(R.id.ttname_asignado);
        name_cliente = findViewById(R.id.txt_namecliente);

        if(nombre_cliente.equals("")){
            ttname_asignado.setVisibility(View.GONE);//se oculta y no se muestra en el diseno
            name_cliente.setVisibility(View.GONE);
        }

        //ver_evento();
        ver_info_evento();
//        titulo_.setText("xd");
//        estado_.setText("xd // ");
    }

    public void ver_info_evento(){
        help_ver_evento pb = new help_ver_evento();
        BD.ver_info_evento(pb, idevento);
        titulo.setText(pb.getTitle());
        name_cliente.setText(pb.getNombre_cliente());
        descripcion.setText(pb.getDescripcion());
        fecha_inicio.setText("- " + pb.getStart_event());
        fecha_final.setText("- " +pb.getEnd_event());
        if(pb.getDetalles() == null || pb.getDetalles().isEmpty())  detalle.setText("No Posee Detalles");
        else  detalle.setText(pb.getDetalles());
        estado.setText(pb.getEstado());
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.verevento));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver2);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver2,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
                Toast.makeText(Ver_evento.this, Utils.getlatitud(location) + " /event/ " + Utils.getlongitud(location),
                        Toast.LENGTH_SHORT).show();
                trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            verifica_mi_ubicacion(lat, lon);
                            guardartrayectoria(id_user,lat, lon,emp);
                        }
                        else {
                            guardartrayectorialocal(id_user,lat, lon,emp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon,emp);
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2,String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2, String emp) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            //BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifica_mi_ubicacion(String latitud, String longitud){
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    System.out.println("Respues ======> " + valida);

                                    if(valida.equals("Fuera")){
//                                        Toasty.error(this, "Fuera de su Trayectoria!!!!", Toast.LENGTH_LONG).show();
                                        send_correo(latitud,longitud);
                                        enviar_mensaje(latitud,longitud);
                                    }
                                    i++;
                                }

                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.info(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);
                params.put("latitud", latitud);//12.119639
                params.put("longitud", longitud);//-86.311440
                params.put("idemp", id_emp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola " + nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(mobil_alert, null, text , null, null);
    }

    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                    System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
//                params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Ver_evento.this, ListEvent.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }
}