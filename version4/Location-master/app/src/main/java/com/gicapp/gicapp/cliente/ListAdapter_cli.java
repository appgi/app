package com.gicapp.gicapp.cliente;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gicapp.gicapp.R;

import java.util.ArrayList;

public class ListAdapter_cli extends BaseAdapter {
    private Context context;
    private  int layout;
    private ArrayList<ListElements_cli> list_cliente;

    public ListAdapter_cli(Context context, int layout, ArrayList<ListElements_cli> list_cliente) {
        this.context = context;
        this.layout = layout;
        this.list_cliente = list_cliente;
    }

    @Override
    public int getCount() {
        return list_cliente.size();
    }

    @Override
    public Object getItem(int position) {
        return list_cliente.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        TextView nombre_cliente,direccion,mail,telefono;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.nombre_cliente =  row.findViewById(R.id.nombre_cliente);
            holder.direccion =  row.findViewById(R.id.direccion_cliente);
            holder.mail =  row.findViewById(R.id.correo_cliente);
            holder.telefono =  row.findViewById(R.id.telefono_cliente);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder) row.getTag();
        }

        ListElements_cli info_cli = list_cliente.get(position);

        holder.nombre_cliente.setText(info_cli.getFirst_name() + " "+ info_cli.getSecond_name() + " "+ info_cli.getSurname1() + " "+ info_cli.getSurname2());
        holder.direccion.setText(info_cli.getAddress());
        if(info_cli.getMail().isEmpty()) holder.mail.setText("No Posee Correo");
        else holder.mail.setText(info_cli.getMail());
        holder.telefono.setText(info_cli.getPhone1());

        return row;
    }

}
