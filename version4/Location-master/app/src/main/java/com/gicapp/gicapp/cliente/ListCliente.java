package com.gicapp.gicapp.cliente;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.LoginPrefences;
import com.gicapp.gicapp.MainActivity;
import com.gicapp.gicapp.R;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.VolleySingleton;
import com.gicapp.gicapp.help_coordenadas;
import com.gicapp.gicapp.notificacion.ExampleService;
import com.gicapp.gicapp.sql.BDAdapter;
import com.gicapp.gicapp.tipo_doc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class ListCliente extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{
    //spinner
    ArrayList<String> listaPersonas;
    ArrayList<tipo_doc> personasList;

    private MyReceiver myReceiver3;
    ListView listView;
    ArrayList<ListElements_cli> list;
    ListAdapter_cli adapter_cli = null;
    LoginPrefences p;
    help_coordenadas help_coordenadas;
    public static BDAdapter BD;
    //volley
    StringRequest stringRequest;
    //variables
    String id_user,emp,nombre_user,telefono_user,mobil_alert,mail_alert;
    String var;
    ProgressDialog progreso;


    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_cliente);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        help_coordenadas = new help_coordenadas();

        //open database
        BD = new BDAdapter(this);
        BD.openDB();

        //**************************************************************************************
        myReceiver3 = new ListCliente.MyReceiver();
        setContentView(R.layout.list_cliente);

        //get idusuario mediante sharedpreferneces
        p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
            emp = p.GetEmp();
            nombre_user = p.GetName();
            telefono_user = p.GetTelefono();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
        }


        //Toast.makeText(FormuList.this,emp,Toast.LENGTH_SHORT).show();

        listView = findViewById(R.id.listview_cliente);
        list = new ArrayList<>();
        adapter_cli = new ListAdapter_cli(this, R.layout.cliente_item, list);
        listView.setAdapter(adapter_cli);

        listView.setOnItemClickListener((adapterView, view, pos, l) -> {
            final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(ListCliente.this);
            dialogDelete.setTitle("Alerta!!");
            dialogDelete.setMessage("Desea enviar este Registro?");
            dialogDelete.setPositiveButton("OK", (dialog, which) -> {
                try {

                    progreso=new ProgressDialog(ListCliente.this);
                    progreso.setMessage("Cargando...");
                    progreso.show();
                    //RequestQueue queue = Volley.newRequestQueue(FormuList.this);
                    stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/guardar_cliente.php",//change save_cliente
                    //stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE2 + "guardar_cliente.php",
                            response -> {
                                try {
                                    JSONObject objResultado = new JSONObject(response);
                                    String estadox = objResultado.get("estado").toString();
                                    if(!estadox.contains("exito")){
                                        Toasty.error(this, "No se ha registrado", Toast.LENGTH_LONG).show();
                                    }else{
                                        //get_id_form(list.get(pos).getIdevento());
//                                        get_idformulario(list.get(pos).getUsername(),list.get(pos).getCellphone(),list.get(pos).getCorreo(),list.get(pos).getName(),
//                                                list.get(pos).getFecha(), list.get(pos).getConclusion(),list.get(pos).getIdevento());
                                        Toasty.success(this, "Enviado satisfactoriamente", Toast.LENGTH_LONG).show();
                                        BD.delete_cliente(list.get(pos).getIdcliente());
                                        updateList();
                                        //BD.closeDB();
                                        Intent intent = new Intent(ListCliente.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                    progreso.hide();
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }, error -> {
                            Toasty.error(ListCliente.this, "No se ha Guardado su Cliente, Verifique su Conexion a Internet", Toast.LENGTH_LONG).show();
                            progreso.hide();
                            Intent intent = new Intent(ListCliente.this, MainActivity.class);
                            intent.putExtra("valoruser", id_user);//ojo
                            startActivity(intent);
                    }) {
                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<>();
                            params.put("idemp",list.get(pos).getId_emp());
                            params.put("idtype", list.get(pos).getIdtype());//new
                            params.put("tipo", list.get(pos).getType());
                            params.put("documento",list.get(pos).getDocument());

                            params.put("p_nombre",list.get(pos).getFirst_name());
                            params.put("s_nombre",list.get(pos).getSecond_name());
                            params.put("surname1",list.get(pos).getSurname1());
                            params.put("s_apellido", list.get(pos).getSurname2());

                            params.put("direccion",list.get(pos).getAddress());
                            params.put("mail",list.get(pos).getMail());
                            params.put("telefono1",list.get(pos).getPhone1());
                            params.put("telefono2",list.get(pos).getPhone2());

                            params.put("lat",list.get(pos).getLati());
                            params.put("lon",list.get(pos).getLongi());

                            params.put("state","Activo");
                            params.put("idusuario",list.get(pos).getIdusuario());
                            params.put("fecha",list.get(pos).getFecha_cli());

                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }
            });
            dialogDelete.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            dialogDelete.show();
        });


        // se obtiene los formularios registrado para un usuario
        Cursor cursor =  BD.getData("SELECT * FROM cliente WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                int idcliente = cursor.getInt(0);
                String idtype = cursor.getString(1);
                String type = cursor.getString(2);
                String document = cursor.getString(3);
                String first_name = cursor.getString(4);
                String second_name = cursor.getString(5);
                String surname1 = cursor.getString(6);
                String surname2 = cursor.getString(7);
                String address = cursor.getString(8);
                String mail = cursor.getString(9);
                String phone1 = cursor.getString(10);
                String phone2 = cursor.getString(11);
                String lat = cursor.getString(12);
                String lon = cursor.getString(13);
                String idemp = cursor.getString(14);
                String idusuario = cursor.getString(15);
                String fecha = cursor.getString(16);
                list.add(new ListElements_cli(idcliente,idtype,type,document,first_name,second_name,surname1,surname2,address,mail,phone1,phone2,lat,lon,idemp,idusuario,fecha));
            }
        }else{
            Toasty.info(this, "No hay Registro de Clientes Pendientes", Toast.LENGTH_LONG).show();
        }
        adapter_cli.notifyDataSetChanged();
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }
    //se actualiza la lista
    private void updateList(){
        // get all data from sqlite
        Cursor cursor = BD.getData("SELECT * FROM cliente WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext()) {
                int idcliente = cursor.getInt(0);
                String idtype = cursor.getString(1);
                String type = cursor.getString(2);
                String document = cursor.getString(3);
                String first_name = cursor.getString(4);
                String second_name = cursor.getString(5);
                String surname1 = cursor.getString(6);
                String surname2 = cursor.getString(7);
                String address = cursor.getString(8);
                String mail = cursor.getString(9);
                String phone1 = cursor.getString(10);
                String phone2 = cursor.getString(11);
                String lat = cursor.getString(12);
                String lon = cursor.getString(13);
                String idemp = cursor.getString(14);
                String idusuario = cursor.getString(15);
                String fecha = cursor.getString(16);
                list.add(new ListElements_cli(idcliente,idtype,type,document,first_name,second_name,surname1,surname2,address,mail,phone1,phone2,lat,lon,idemp,idusuario,fecha));
            }
        }
        else{
            ///se detiene el servicio de la notificacion
            stopService();
            Toasty.info(this, "No hay Registro de Clientes Pendientes", Toast.LENGTH_SHORT).show();
        }
        adapter_cli.notifyDataSetChanged();
    }

    @Override
    public boolean onSupportNavigateUp() {
//        onBackPressed();
        Intent intent = new Intent(ListCliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ListCliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.clientes));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver3);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver3,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(FormuList.this, Utils.getlatitud(location) + " /formu_list/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
                verifica_coord(Utils.getlatitud(location), Utils.getlongitud(location));
//                help_coordenadas.setLat(Utils.getlatitud(location));
//                help_coordenadas.setLon(Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void verifica_coord(String lat,String lon){
        if(lat == null || lat.isEmpty() && lon == null || lon.isEmpty())
            trayectoria_verifica_internet(p.Getlat(), p.Getlon());
        else
            trayectoria_verifica_internet(lat, lon);
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        //RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                //StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            verifica_mi_ubicacion(lat, lon);
                            guardartrayectoria(id_user,lat, lon,emp);
                        }
                        else {
//                            Toast.makeText(FormuList.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,lat, lon,emp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon,emp);
        }) {
        };
        //queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2,String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        //RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
                // StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        //queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2,String emp) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            //BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifica_mi_ubicacion(String latitud, String longitud)
    {
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
        //stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE2 + "alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    System.out.println("Respues ======> " + valida);

                                    if(valida.equals("Fuera")){
//                                        Toasty.error(this, "Fuera de su Trayectoria!!!!", Toast.LENGTH_LONG).show();
                                        send_correo(latitud,longitud);
                                        enviar_mensaje(latitud,longitud);
                                    }
                                    i++;
                                }

                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.info(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);
                params.put("latitud", latitud);//12.119639
                params.put("longitud", longitud);//-86.311440
                params.put("idemp", emp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola " + nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(mobil_alert, null, text , null, null);
        //Toasty.success(Formulario.this, "Mensaje enviado", Toast.LENGTH_LONG).show();
    }
    //se envia el correo
    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                    System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
//                params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
