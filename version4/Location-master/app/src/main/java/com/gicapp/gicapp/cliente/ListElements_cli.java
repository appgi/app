package com.gicapp.gicapp.cliente;

public class ListElements_cli {
    public int idcliente;
    public String idtype;
    public String type;
    public String document;
    public String first_name;
    public String second_name;
    public String surname1;
    public String surname2;
    public String address;
    public String mail;
    public String phone1;
    public String phone2;
    public String lati;
    public String longi;
    public String id_emp;
    public String idusuario;
    public String fecha_cli;

    public ListElements_cli(int idcliente,String idtype, String type, String document, String first_name, String second_name, String surname1, String surname2,
                            String address, String mail, String phone1, String phone2, String lati, String longi, String id_emp, String idusuario, String fecha_cli) {
        this.idcliente = idcliente;
        this.idtype = idtype;
        this.type = type;
        this.document = document;
        this.first_name = first_name;
        this.second_name = second_name;
        this.surname1 = surname1;
        this.surname2 = surname2;
        this.address = address;
        this.mail = mail;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.lati = lati;
        this.longi = longi;
        this.id_emp = id_emp;
        this.idusuario = idusuario;
        this.fecha_cli = fecha_cli;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getFecha_cli() {
        return fecha_cli;
    }

    public void setFecha_cli(String fecha_cli) {
        this.fecha_cli = fecha_cli;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getSurname1() {
        return surname1;
    }

    public void setSurname1(String surname1) {
        this.surname1 = surname1;
    }

    public String getSurname2() {
        return surname2;
    }

    public void setSurname2(String surname2) {
        this.surname2 = surname2;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getLati() {
        return lati;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public String getId_emp() {
        return id_emp;
    }

    public void setId_emp(String id_emp) {
        this.id_emp = id_emp;
    }
}
