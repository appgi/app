package com.gicapp.gicapp.eventos;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gicapp.gicapp.Formulario;
import com.gicapp.gicapp.LoginPrefences;
import com.gicapp.gicapp.R;
import com.gicapp.gicapp.Ver_evento;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<ListElements> mData;
    private final LayoutInflater mInflater;
    private Context context;
    private String nombre;

    public ListAdapter(List<ListElements> itemList,Context context){
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mData = itemList;
        setItems(itemList);
    }
    @Override
    public int getItemCount(){
        return mData.size();
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = mInflater.inflate(R.layout.list_elements,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ListAdapter.ViewHolder holder,final int position){
        holder.binData(mData.get(position));
        LoginPrefences p = new LoginPrefences(context);
        if (!p.isEmpty()){
            nombre = p.GetName();
            //Toast.makeText(this,"id"+p.Getid(),Toast.LENGTH_SHORT).show();
        }
        holder.itemView.setOnClickListener(view -> {
//                Toast.makeText(
//                        context,
//                        holder.title + " id: "+ holder.id,
//                        Toast.LENGTH_LONG).show();

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Opciones");
            builder.setMessage("Hola " + nombre + "!, Eliga una Opción: ");        // add the buttons
            builder.setPositiveButton("Ver Evento", (dialog1, which) -> {
                Intent intent = new Intent(context, Ver_evento.class);
                Bundle mibundle = new Bundle();
                mibundle.putString("idevento",holder.id);//se envia el id del evento
                mibundle.putString("nombre_cliente",holder.nombre_cliente);//Se envia el nombre del cliente
                intent.putExtras(mibundle);
                context.startActivity(intent);
            });
            builder.setNegativeButton("Completar Evento", (dialog1, which) -> {
                Intent intent = new Intent(context,Formulario.class);
                Bundle mibundle = new Bundle();
                mibundle.putString("idevento",holder.id);//se envia el id del evento
                mibundle.putString("nombre_cliente",holder.nombre_cliente);//Se envia el nombre del cliente
                intent.putExtras(mibundle);
                context.startActivity(intent);
            });
            builder.setNeutralButton("Cancelar", (dialog1, which) -> {
                dialog1.dismiss();
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        });
    }
    public void setItems(List<ListElements> items){
        mData = items;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView Cv;
        LinearLayout linear;
        ImageView iconImage;
        TextView titulo,fecha,fecha2,estatus;
        String title = "",id = "",nombre_cliente = "", id_cliente = "";
        Context context2;
        @SuppressLint("UseCompatLoadingForDrawables")
        ViewHolder(View itemView){
            super(itemView);
            Cv =  itemView.findViewById(R.id.cv);
            linear = itemView.findViewById(R.id.bk);
            //linear2 = itemView.findViewById(R.id.bk2);
            iconImage = itemView.findViewById(R.id.imgview);
            titulo = itemView.findViewById(R.id.txtTitulo);
            fecha = itemView.findViewById(R.id.txtDescripcion);
            fecha2 = itemView.findViewById(R.id.txtDescripcion2);
            estatus = itemView.findViewById(R.id.txt_estado);

            context2 = itemView.getContext();
        }

        @SuppressLint("SetTextI18n")
        void binData(final ListElements item){
            iconImage.setColorFilter(Color.parseColor(item.getcolor()), PorterDuff.Mode.SRC_IN);
           // linear.setBackgroundColor(Color.parseColor(item.getcolor()));
            nombre_cliente = item.getNombre_cliente();//se obtiene el nombre del usuario

            String newString = new String(item.getTitle().getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
            titulo.setText(newString+" "+ nombre_cliente);

            // titulo.setTextColor(Color.parseColor(item.text_color)); //se cambia el color al textview
            fecha.setText(item.getStart_event());
            fecha2.setText(item.getend_event());
            estatus.setText(item.getEstado());

            System.out.println("xd========> " + item.getEstado());

            title = item.getTitle();
            id = item.getId();
            id_cliente = item.getId_cliente();//se obtine el id del usuario
            ((GradientDrawable)linear.getBackground()).setColor(Color.parseColor(item.getcolor()));

            titulo.setOnClickListener(view -> {
               // Toasty.info(context2, item.getTitle(), Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context2);
                String newString1 = new String(item.getTitle().getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                builder1.setMessage(newString1+" "+ nombre_cliente);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Aceptar",
                        (dialog, id) -> dialog.cancel());

                AlertDialog alert11 = builder1.create();
                alert11.show();
            });
        }
//        public String retorna_titulo(String titulo){
//            String _titulo;
//            title_ = titulo;
//            for (int i = 0; i < title_.length(); i++) {
//                // Si el carácter en [i] es un espacio (' ') aumentamos el contador
//                if (title_.charAt(i) == ' ') cantidad_espacio++;
//            }
//            if(cantidad_espacio >= 5){
//                result = title_.split(" ", 5);
//                a = result[0] + " " +result[1] + " " + result[2] + " " + result[3]; //+ " " +result[4];
//                _titulo = a + " - Ver mas...";
//            }else{
//                _titulo = title_;
//            }
//            return _titulo;
//        }
    }



}
