package com.gicapp.gicapp.eventos;

public class ListElements  {
    public String id;
    public String title;
    public String descripcion;
    public String start_event;
    public String end_event;
    public String color;
    public String text_color;
    public String create_it;
    public String create_by;
    public String create_at;
    public String update_by;
    public String update_at;
    public String detalles;
    public String estado;
    public String id_cliente;
    public String nombre_cliente;

    public ListElements(String id, String title, String descripcion, String start_event, String end_event, String color, String text_color, String create_it, String create_by, String create_at, String update_by, String update_at, String detalles, String estado, String id_cliente,String nombre_cliente) {
        this.id = id;
        this.title = title;
        this.descripcion = descripcion;
        this.start_event = start_event;
        this.end_event = end_event;
        this.color = color;
        this.text_color = text_color;
        this.create_it = create_it;
        this.create_by = create_by;
        this.create_at = create_at;
        this.update_by = update_by;
        this.update_at = update_at;
        this.detalles = detalles;
        this.estado = estado;
        this.id_cliente = id_cliente;
        this.nombre_cliente = nombre_cliente;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getStart_event() {
        return start_event;
    }

    public void setStart_event(String start_event) {
        this.start_event = start_event;
    }

    public String getend_event() {
        return end_event;
    }

    public void setend_event(String end_event) {
        this.end_event = end_event;
    }

    public String getcolor() {
        return color;
    }

    public void setcolor(String color) {
        this.color = color;
    }

    public String getText_color() {
        return text_color;
    }

    public void setText_color(String text_color) {
        this.text_color = text_color;
    }

    public String getCreate_it() {
        return create_it;
    }

    public void setCreate_it(String create_it) {
        this.create_it = create_it;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(String update_by) {
        this.update_by = update_by;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
