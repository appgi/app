package com.gicapp.gicapp.eventos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Get_Hours;
import com.gicapp.gicapp.LoginPrefences;
import com.gicapp.gicapp.MainActivity;
import com.gicapp.gicapp.R;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.VolleySingleton;
import com.gicapp.gicapp.notificacion.ExampleService;
import com.gicapp.gicapp.notificacion.NotificationHelper;
import com.gicapp.gicapp.sql.BDAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static com.gicapp.gicapp.notificacion.NotificationHelper.CHANNEL_SERVICE_ID;

public class ListEvent extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{

    BDAdapter BD;
    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver2;
    List<ListElements> elements,elements_local;
    private Gson gson = new Gson();
    String id_user,nombre_user,nombre_cliente,telefono_user,id_emp,mobil_alert,mail_alert;

    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.

    //volley
    StringRequest stringRequest;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_event);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        BD = new BDAdapter(this);
        BD.openDB();
        //get idusuario mediante sharedpreferneces
        LoginPrefences p = new LoginPrefences(getApplicationContext());
        if (!p.isEmpty())
        {
            id_user = p.Getid();
            nombre_user = p.GetName();
            telefono_user = p.GetTelefono();
            id_emp = p.GetEmp();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
            //Toast.makeText(this,"id"+p.Getid(),Toast.LENGTH_SHORT).show();
        }

        //**************************************************************************************
        myReceiver2 = new ListEvent.MyReceiver();
        setContentView(R.layout.activity_list_event);
        //**************************************************************************************
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 || tipoConexion2) {
                //********************************* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
//                Toast.makeText(getApplicationContext(), "hay internet ", Toast.LENGTH_SHORT).show();
                verifica_internet();
            }
        }
        else {
            //Toast.makeText(getApplicationContext(), "Favor encender el WIFI ó Datos Moviles", Toast.LENGTH_LONG).show();
            /* ******************************** No estas conectado a internet********************************  */
            Toasty.info(this, "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_SHORT).show();
            //muestra los eventos que estan guardado de manera local
            show_events_local();
        }
    }

    //*********************************************** verifica interet ***********************************************
    //se verifica la conexiona a internet
    public void verifica_internet(){
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(Login.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            planner_sin();
                            planner_con();
                        }else {
                            Toasty.warning(this, "No tiene Acceso a Internet", Toast.LENGTH_LONG).show();
                            show_events_local();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error ->   show_events_local()){//Toasty.error(this, "Error en la Conexion", Toast.LENGTH_LONG).show()) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void planner_sin() {
        elements = new ArrayList<>();
       // RequestQueue queue = Volley.newRequestQueue(ListEvent.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/Planner1.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1": // EXITO
                                // Obtener array "metas" Json
                                JSONArray array = objResultado.getJSONArray("datos");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String id =object1.getString("id");
                                    String title =object1.getString("title");
                                    String description =object1.getString("descripcion");
                                    String start_event =object1.getString("start");
                                    String end_event =object1.getString("end");
                                    String color =object1.getString("color");
                                    String text_color =object1.getString("text_color");
                                    String create_it = "create_it";
                                    String create_by = "create_by";
                                    String create_at = "create_at";
                                    String update_by ="update_by";
                                    String update_at = "update_at";
                                    String detalles =object1.getString("detalles");
                                    String estado =object1.getString("estado");
                                    String estado_local = object1.getString("estado");
                                    String id_cliente = object1.getString("idcliente");

                                    if(id_cliente == null || id_cliente.isEmpty())
                                        nombre_cliente = "";

                                    String stage = "Cumplida";
                                    if(!BD.verifica_evento_local_ser(id,stage)){
                                        elements.add(new ListElements(id,title,description,start_event,end_event,color,text_color,create_it,create_by,create_at,update_by,update_at,detalles,estado,id_cliente,nombre_cliente));

                                        if(BD.verifica_evento_mainactivity(id)){
                                            BD.update_campos_eventos(id,title,description,start_event,end_event,color,text_color,detalles,estado,estado_local,id_cliente,nombre_cliente);//se actualiza el evento local
                                        }else{
                                            //se valida para que no inserte datos repetidos en la base de datos local
                                            if(!BD.verifica_evento(id,id_user,title,description)) {
                                                BD.guardar_evento(id, id_user, title, description, start_event, end_event, color, text_color, create_it, create_by, create_at, update_by, update_at, detalles, estado, estado_local,id_cliente,nombre_cliente);
                                                Toasty.success(this, "Se ha Agregado un Evento Nuevo", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                    ListAdapter listAdapter = new ListAdapter(elements,this);
                                    RecyclerView recyclerView = findViewById(R.id.list_recycler);
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                                    recyclerView.setAdapter(listAdapter);
                                    i++;
                                }
                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toasty.error(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toasty.error(
                                        this,
                                        "el valor de state es: " + state + " 1",
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(ListEvent.this, "Verifique su Conexion a Internet", Toast.LENGTH_LONG).show() ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario",id_user);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //Con el idcliente
    public void planner_con() {
        elements = new ArrayList<>();
       // RequestQueue queue = Volley.newRequestQueue(ListEvent.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/Planner.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        // Obtener atributo "estado"
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1": // EXITO
                                // Obtener array "metas" Json
                                JSONArray array = objResultado.getJSONArray("datos");
                                int i = 0;
                                while (i<array.length()){
                                    JSONObject object1=array.getJSONObject(i);
                                    String id =object1.getString("id");
                                    String title =object1.getString("title");
                                    String description =object1.getString("descripcion");
                                    String start_event =object1.getString("start");
                                    String end_event =object1.getString("end");
                                    String color =object1.getString("color");
                                    String text_color =object1.getString("text_color");
                                    String create_it = "create_it";
                                    String create_by = "create_by";
                                    String create_at = "create_at";
                                    String update_by ="update_by";
                                    String update_at = "update_at";
                                    String detalles =object1.getString("detalles");
                                    String estado =object1.getString("estado");
                                    String estado_local = object1.getString("estado");
                                   // String idempresa = object1.getString("idemp");
                                    //se obtiene los datos del cliente
                                    String id_cliente = object1.getString("idcliente");
                                    String p_nombre = object1.getString("p_nombre");
                                    String s_nombre = object1.getString("s_nombre");
                                    String p_apellido = object1.getString("p_apellido");
                                    String s_apellido = object1.getString("s_apellido");

                                    String nombrecliente;
                                    //se verifica si el campo nombre del cliente
                                    if(p_nombre == null || p_nombre.isEmpty())
                                        nombrecliente = "";
                                    else
                                        nombrecliente = p_nombre + " " + s_nombre + " " + p_apellido + " " +s_apellido;

                                    String stage = "Cumplida";
                                    if(!BD.verifica_evento_local_ser(id,stage)){
                                        elements.add(new ListElements(id,title,description,start_event,end_event,color,text_color,create_it,create_by,create_at,update_by,update_at,detalles,estado,id_cliente,nombrecliente));
                                        //Se verifica si el evento es igual al de la nube
                                        if(BD.verifica_evento_mainactivity(id)){//si hay un cambio, se actualiza localmente
                                            BD.update_campos_eventos(id,title,description,start_event,end_event,color,text_color,detalles,estado,estado_local,id_cliente,nombrecliente);
                                        }else{
                                            if(!BD.verifica_evento(id,id_user,title,description)) {
                                                BD.guardar_evento(id, id_user, title, description, start_event, end_event, color, text_color, create_it, create_by, create_at, update_by, update_at, detalles, estado, estado_local,id_cliente,nombrecliente);
                                                Toasty.success(this, "Se ha Agregado un Evento Nuevo", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                    }
                                    ListAdapter listAdapter = new ListAdapter(elements,this);
                                    RecyclerView recyclerView = findViewById(R.id.list_recycler);
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                                    recyclerView.setAdapter(listAdapter);
                                    i++;
                                }
                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toasty.error(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toasty.error(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Error en la Conexion", Toast.LENGTH_LONG).show()) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("idusuario",id_user);
                    return params;
                }
        };//queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void alert(){
        NotificationHelper notificationHelper = new NotificationHelper(this);
        notificationHelper.createChannels();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_SERVICE_ID)
                .setSmallIcon(R.drawable.ic_today_black_24dp)
                .setContentTitle("Evento")
                .setContentText("Hola "+ nombre_user+", Se Ha Registrado un Nuevo Evento")
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
        notificationManager.notify(0, builder.build());

//        if(aler){
//            startService();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    stopService();
//                }
//            },20000);
//        }
    }
    //notificaciones
    public void startService() {
        String input = "Se ha Agregado un Nuevo Evento ";
        Intent serviceIntent = new Intent(this, ExampleService.class);
        serviceIntent.putExtra("inputExtra", input);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }
    //se carga la lista de eventos al recyclerview
    public void show_events_local()
    {
        String stage_local = "Cumplida";
        elements_local = new ArrayList<>();
        // se muestra los eventos asignados a un usuario
        Cursor cursor =  BD.getData("SELECT * FROM evento where id_usuario = '"+id_user+"' and estado_local != '"+stage_local+"'");
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                //int id = cursor.getInt(0);
                String id = cursor.getString(1);
                String title = cursor.getString(3);
                String description = cursor.getString(4);
                String start_event = cursor.getString(5);
                String end_event = cursor.getString(6);
                String color = cursor.getString(7);
                String text_color = cursor.getString(8);
                String create_it = cursor.getString(9);//no
                String create_by = cursor.getString(10);//no
                String create_at = cursor.getString(11);//no
                String update_by = cursor.getString(12);//no
                String update_at = cursor.getString(13);//no
                String detalles = cursor.getString(14);
                String estado = cursor.getString(15);
                String id_cliente = cursor.getString(17);
                String nombre_cliente = cursor.getString(18);

                elements_local.add(new ListElements(id,title,description,start_event,end_event,color,text_color,create_it,create_by,create_at,update_by,update_at,detalles,estado,id_cliente,nombre_cliente));
                ListAdapter listAdapter = new ListAdapter(elements_local,this);
                RecyclerView recyclerView = findViewById(R.id.list_recycler);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(listAdapter);
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.eventos));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver2);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver2,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(ListEvent.this, Utils.getlatitud(location) + " /event/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
               /// trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));

                System.out.println("==LIST===========> " + Utils.getlatitud(location)+" /*/ "+Utils.getlongitud(location) + " / " + Get_Hours.getHora("hh:mm a"));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();

        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
//         StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //si hay internet las coordenadas se guardaran en el servidor
                            verifica_mi_ubicacion(lat, lon);
                            //guardartrayectoria(id_user,lat, lon,emp);
                        }
//                        else {
//                            //Toast.makeText(ListEvent.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
//                            //si no hay internet las coordenadas se guardaran localmente
//                           // guardartrayectorialocal(id_user,lat, lon,emp);
//                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> { Toasty.warning(ListEvent.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
            //guardartrayectorialocal(id_user,lat, lon,emp);
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2,String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        //RequestQueue queue = Volley.newRequestQueue(ListEvent.this);
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//         StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2, String emp) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            //BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //se verifica si la ubicacion actual esta dentro del Area
    public void verifica_mi_ubicacion(String latitud, String longitud){
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
        //stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE2 + "alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    System.out.println("Respues ======> " + valida);

                                    if(valida.equals("Fuera")){
//                                        Toasty.error(this, "Fuera de su Trayectoria!!!!", Toast.LENGTH_LONG).show();
                                        send_correo(latitud,longitud);
                                        //enviar_mensaje(latitud,longitud);
                                    }
                                    i++;
                                }

                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toasty.error(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toasty.error(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "CONEXION INVALIDA - MI_UBICACION", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);
                params.put("latitud", latitud);//12.119639
                params.put("longitud", longitud);//-86.311440
                params.put("idemp", id_emp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola " + nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(mobil_alert, null, text , null, null);
        //Toasty.success(ListEvent.this, "Mensaje enviado", Toast.LENGTH_LONG).show();
    }

    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                    System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
//                params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ListEvent.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }
}