package com.gicapp.gicapp.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.gicapp.gicapp.help_ver_evento;

public class BDAdapter {
    Context c;
    SQLiteDatabase db;
    BDHelper helper;

    public BDAdapter(Context c) {
        this.c = c;
        helper=new BDHelper(c);
    }

    //Abrir Base de Datos
    public void openDB()
    {
        try
        {
            db=helper.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //Cerrar Base de Datos
    public void closeDB()
    {
        try
        {
            helper.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    //************************************************************* the table formulario*************************************************************
    public void insertData(String username, String cellphone, String correo, byte[] image_c, byte[] firma, String name, String fecha,String idevento, String idusuario,String conclusion){
//        db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO form VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, username);
        statement.bindString(2, cellphone);
        statement.bindString(3, correo);
        statement.bindBlob(4, image_c);
        statement.bindBlob(5, firma);
        statement.bindString(6, name);
        statement.bindString(7, fecha);
        statement.bindString(8, idevento);
        statement.bindString(9, idusuario);
        statement.bindString(10,conclusion);

        statement.executeInsert();

        closeDB();
    }

    public void deleteData(int id) {
//        db =  helper.getWritableDatabase();
        openDB();
        String sql = "DELETE FROM form WHERE id = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
//        db.close();
        closeDB();
    }

    public Cursor getData(String sql){
        db =  helper.getReadableDatabase();
        return  db.rawQuery(sql, null);
    }
    public void get_idformulario(prueba_var pb)
    {
        Cursor cursor=null;
        String sql = "SELECT MAX(id) FROM form";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIdform(cursor.getString(0));
            }while (cursor.moveToNext());
        }
    }

    public void ver_info_evento(help_ver_evento pb, String idevento)
    {
        db =  helper.getReadableDatabase();
        Cursor cursor=null;
        String sql = "SELECT * FROM evento where id = '"+idevento+"'";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setTitle(cursor.getString(3));
                pb.setDescripcion(cursor.getString(4));
                pb.setStart_event(cursor.getString((5)));
                pb.setEnd_event(cursor.getString(6));
                pb.setDetalles(cursor.getString(14));
                pb.setEstado(cursor.getString(15));
                pb.setNombre_cliente(cursor.getString(18));
            }while (cursor.moveToNext());
        }
//        db.close();
        closeDB();
    }

    //************************************************************* the table user*************************************************************
    public void insert_tbuser(String id_user, String username, String estado, String emp){
       // db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO usuario VALUES (NULL,?, ?, ?,?,NULL,NULL)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, id_user);
        statement.bindString(2, username);
        statement.bindString(3, estado);
        statement.bindString(4, emp);
        statement.executeInsert();
//        db.close();
        closeDB();
    }
    public void insert_tb_mov(String id_user, String android_id, String code,String id_mov,String alert_movil, String alert_mail){
       // db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO movilidad VALUES (NULL,?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, android_id);
        statement.bindString(2, code);
        statement.bindString(3, id_user);
        statement.bindString(4, id_mov);
        statement.bindString(5, alert_movil);
        statement.bindString(6, alert_mail);
        statement.executeInsert();
//        db.close();
        closeDB();
    }
    public void update_android_id( String android_id, String id_mov){
        //db =  helper.getWritableDatabase();
        openDB();
        ContentValues valores = new ContentValues();
        valores.put("android_id",android_id);

        String[] args = new String[]{id_mov};
        db.update("movilidad", valores, "id_movilidad = ?", args);

//        db.close();
        closeDB();
    }


    public boolean verifica_user(String username,String password)
    {
        //db =  helper.getWritableDatabase();
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(username.length()>0 && password.length()>0 ) {
            String sql = "SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = '"+username+"' and movilidad.code = '"+password+"'";
            c = db.rawQuery(sql, null);

            if (c.getCount() > 0) {
                existe = true;
            }
        }
        //db.close();
        closeDB();
        return existe;
    }
    public void get_iduser_mobilidad_estado(prueba_var pb,String username,String password)
    {
        openDB();
        Cursor cursor=null;
        String sql = "SELECT * FROM usuario INNER JOIN movilidad WHERE movilidad.id_usuario = usuario.id_user and usuario.username = '"+username+"' and movilidad.code = '"+password+"'";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIduser_login(cursor.getString(1));
                pb.setMobilidad_login("1");
                pb.setEstado_login(cursor.getString(3));
                pb.setIdemp(cursor.getString(4));
//                pb.setTelefono(cursor.getString(7));
            }while (cursor.moveToNext());
        }
        closeDB();
    }

    public void get_idmovilidad(prueba_var pb)
    {
        openDB();
        Cursor cursor=null;
        String sql = "SELECT * FROM movilidad";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setId_movilidad(cursor.getString(4));
            }while (cursor.moveToNext());
        }
        closeDB();
    }

    public void get_iduser(prueba_var pb,String username,String password){
        Cursor cursor=null;
        String sql = "SELECT * FROM usuario where username = '"+username+"' AND password = '"+password+"'"; //+ username + " AND password = " + password;
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setIduser_login(cursor.getString(1));
            }while (cursor.moveToNext());
        }
    }
    //trayec
    public void insert_trayect(String usuario, String lat, String lon,String fecha,String emp){
       // db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO trayectoria VALUES (NULL, ?, ?, ?,?,?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, usuario);
        statement.bindString(2, lat);
        statement.bindString(3, lon);
        statement.bindString(4, fecha);
        statement.bindString(5, emp);

        statement.executeInsert();
        closeDB();
    }

    public void deletelisttrayectoria(int id) {
        //db =  helper.getWritableDatabase();
        openDB();
        String sql = "DELETE FROM trayectoria WHERE id_trayectoria = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
//        db.close();
        closeDB();
    }

    //************************************************ guardar evento local*****
    public boolean verifica_evento(String id, String id_usuario,String title,
                                   String descripcion)
    {
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0 && id_usuario.length()>0 && title.length()>0 &&
                descripcion.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM evento where id = '"+id+"' AND id_usuario = '"+id_usuario+"' AND title = '"+title+"' AND descripcion = '"+descripcion+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }

    public void update_campos_eventos(String idevento,String title,String description,
                                      String start_event,String end_event,String color, String text_color,
                                      String detalles,String estado,String estado_local,String id_cliente,String nombre_cliente){

       // db =  helper.getWritableDatabase();
        openDB();
        ContentValues valores = new ContentValues();
        valores.put("title",title);
        valores.put("descripcion",description);
        valores.put("start",start_event);
        valores.put("end",end_event);
        valores.put("color",color);
        valores.put("text_color",text_color);
        valores.put("detalles",detalles);
        valores.put("estado",estado);
        valores.put("estado_local",estado_local);
        valores.put("id_cliente",id_cliente);
        valores.put("nombre_cliente",nombre_cliente);

        String[] args = new String[]{idevento};
        db.update("evento", valores, "id = ?", args);
        closeDB();
    }

    public boolean verifica_evento_local_ser(String id,String stage){
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM evento where id = '"+id+"' AND estado_local = '"+stage+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }

    public boolean verifica_evento_mainactivity(String id)
    {
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM evento where id = '"+id+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }

    public void guardar_evento(String id, String id_usuario,String title,
                               String descripcion, String start,
                               String end, String color,
                               String text_color, String create_it,
                               String create_by, String create_at,
                               String update_by, String update_at,
                               String detalles, String estado,String estado_local,String id_cliente,String nombre_cliente){
       // db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO evento VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, id);
        statement.bindString(2, id_usuario);
        statement.bindString(3, title);
        statement.bindString(4, descripcion);
        statement.bindString(5, start);
        statement.bindString(6, end);
        statement.bindString(7, color);
        statement.bindString(8, text_color);
        statement.bindString(9, create_it);
        statement.bindString(10, create_by);
        statement.bindString(11, create_at);
        statement.bindString(12, update_by);
        statement.bindString(13, update_at);
        statement.bindString(14, detalles);
        statement.bindString(15, estado);
        statement.bindString(16, estado_local);
        statement.bindString(17, id_cliente);
        statement.bindString(18, nombre_cliente);
        statement.executeInsert();
        closeDB();
    }

    public void actualizar_eventos(String idevento){
        //db =  helper.getWritableDatabase();
        openDB();
        String[] args = new String[]{idevento};
        db.delete("evento", "id = ?", args);
        closeDB();
    }

    public void update_stage_local(String idevento){
        openDB();
        String stage = "Cumplida";
        //db =  helper.getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put("estado_local",stage);

        String[] args = new String[]{idevento};
        db.update("evento", valores, "id = ?", args);
        closeDB();
    }

    public void get_img_user(prueba_var pb, String idusuario){
        openDB();
        Cursor cursor=null;
        String sql = "SELECT * FROM usuario WHERE id_user= '"+idusuario+"'";
        cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                pb.setImg_user(cursor.getBlob(5));
            }while (cursor.moveToNext());
        }
        closeDB();
    }

    public boolean verifica_url_user(String url){
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(url.length()>0) {
            String sql = "SELECT * FROM usuario where url_img = '"+url+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }

    public void update_url_user(String url,String iduser){
        //db =  helper.getWritableDatabase();
        openDB();
        ContentValues valores = new ContentValues();
        valores.put("url_img",url);

        String[] args = new String[]{iduser};
        db.update("usuario", valores, "id_user = ?", args);
        closeDB();
    }

    public void update_img_user(byte[] img_user,String iduser){
       // db =  helper.getWritableDatabase();
        openDB();
        ContentValues valores = new ContentValues();
        valores.put("img_user",img_user);

        String[] args = new String[]{iduser};
        db.update("usuario", valores, "id_user = ?", args);
        closeDB();
    }


    public void insert_tb_doc(String idtype, String name){
       // db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO type_doc VALUES (NULL, ?, ?,NULL,NULL,NULL)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, idtype);
        statement.bindString(2, name);
        statement.executeInsert();
        closeDB();
    }
    //se verifica si existe el doc
    public boolean verifica_doc(String id, String nombre)
    {
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM type_doc where id_type = '"+id+"' AND name = '"+nombre+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }
    public void update_typedoc(String id,String nombre){
       // db =  helper.getWritableDatabase();
        openDB();
        ContentValues valores = new ContentValues();
        valores.put("name",nombre);

        String[] args = new String[]{id};
        db.update("type_doc", valores, "id_type = ?", args);
        closeDB();
    }
    public boolean verifica_doc_1(String id)
    {
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM type_doc where id_type = '"+id+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }

    //guardar cliente
    public void guardar_cliente(String idtype, String type,String document,
                                String first_name, String second_name,
                                String surname1, String surname2,
                                String address, String mail,
                                String phone1, String phone2,
                                String lat, String lon,
                                String idemp,String idusario,String fecha){
       // db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO cliente VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, idtype);
        statement.bindString(2, type);
        statement.bindString(3, document);
        statement.bindString(4, first_name);
        statement.bindString(5, second_name);
        statement.bindString(6, surname1);
        statement.bindString(7, surname2);
        statement.bindString(8, address);
        statement.bindString(9, mail);
        statement.bindString(10, phone1);
        statement.bindString(11, phone2);
        statement.bindString(12, lat);
        statement.bindString(13, lon);
        statement.bindString(14, idemp);
        statement.bindString(15, idusario);
        statement.bindString(16, fecha);
        statement.executeInsert();
        closeDB();
    }
    //
    public void delete_cliente(int id) {
       // db =  helper.getWritableDatabase();
        openDB();

        String sql = "DELETE FROM cliente WHERE idcliente = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);
        statement.execute();
       // db.close();
        closeDB();
    }

    //visita cliente

    public void insert_tb_visitacliente(String idcliente, String nombre){
        //db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO visita_cliente VALUES (NULL, ?, ?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, idcliente);
        statement.bindString(2, nombre);
        statement.executeInsert();
        closeDB();
    }

    public boolean verifica_visitacliente(String id,String nombre)
    {
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0 && nombre.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM visita_cliente where idcliente = '"+id+"' AND nombre = '"+nombre+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }

    public boolean verifica_namecliente(String id)
    {
        openDB();
        Cursor c=null;
        boolean existe = false;

        if(id.length()>0) {
            //String sql = "SELECT * FROM " + Constans.TB_NAME_USER + " WHERE " + Constans.USERNAME_USER + " AND '=" + telefono + "%' ";
            String sql = "SELECT * FROM visita_cliente where idcliente = '"+id+"'";
            c = db.rawQuery(sql, null);
            if (c.getCount() > 0) {
                existe = true;
            }
        }
        closeDB();
        return existe;
    }

    public void update_name_cliente(String id,String nombre){
        //db =  helper.getWritableDatabase();
        openDB();
        ContentValues valores = new ContentValues();
        valores.put("nombre",nombre);

        String[] args = new String[]{id};
        db.update("visita_cliente", valores, "idcliente = ?", args);
        closeDB();
    }

    public void guardar_visita_cliente(String idusuario, String id_cliente, byte[] foto_cli, String comentario, String latitud, String longitud, String fecha,String idemp,
                                       String nombrecliente, String nombreimg){
       // db =  helper.getWritableDatabase();
        openDB();
        String sql = "INSERT INTO inv_visita_cliente VALUES (NULL, ?,?,?,?,?,?,?,?,?,?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, idusuario);
        statement.bindString(2, id_cliente);
        statement.bindBlob(3, foto_cli);
        statement.bindString(4, comentario);
        statement.bindString(5, latitud);
        statement.bindString(6, longitud);
        statement.bindString(7, fecha);
        statement.bindString(8, idemp);
        statement.bindString(9, nombrecliente);
        statement.bindString(10, nombreimg);
        statement.executeInsert();
        closeDB();
    }

    public void delete_visitacliente(int id){
        //db =  helper.getWritableDatabase();
        openDB();

        String sql = "DELETE FROM inv_visita_cliente WHERE idcli = ?";
        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
//        db.close();
        closeDB();
    }

}
