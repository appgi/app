package com.gicapp.gicapp.sql;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDHelper extends SQLiteOpenHelper {

    public BDHelper(Context context) {
        super(context, Constans.DB_NAME, null, Constans.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(Constans.CREATE_TB);
            db.execSQL(Constans.CREATE_TB_USER);
            db.execSQL(Constans.CREATE_TB_POSITION);
            db.execSQL(Constans.CREATE_TB_TRAYECTORIA);
            db.execSQL(Constans.CREATE_TB_MOVILIDAD);
            db.execSQL(Constans.CREATE_TB_EVENTO);
            db.execSQL(Constans.CREATE_TB_TYPE_DOC);
            db.execSQL(Constans.CREATE_TB_CLIENTE);
            db.execSQL(Constans.CREATE_TB_VISITA_CLIENTE);
            db.execSQL(Constans.CREATE_TB_INV_VISITA_CLIENTE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_USER);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_POSITION);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_TRAYECTORIA);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_MOVILIDAD);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_EVENTO);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_TYPE_DOC);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_CLIENTE);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_VISITACLIENTE);
        db.execSQL("DROP TABLE IF EXISTS " + Constans.TB_NAME_INVVISITA_CLIENTE);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.setForeignKeyConstraintsEnabled(true);
        }
    }
}