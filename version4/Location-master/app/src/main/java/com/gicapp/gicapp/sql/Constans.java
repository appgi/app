package com.gicapp.gicapp.sql;

public class Constans {
    //Columnas
    static final String ROW_ID="id";
    static final String USERNAME="username";
    static final String CELLPHONE="cellphone";
    static final String CORREO="correo";
    static final String IMAGE_C="image_c";
    static final String FIRMA="firma";
    static final String NAME="name";

    //tabla usuario
    static final String USERNAME_USER="username";
    static final String PASSWORD_USER="password";

    //DB
    static final String DB_NAME="AppDB";
    static final String TB_NAME="form";
    static final String TB_NAME_USER="usuario";
    static final String TB_NAME_POSITION="position";
    static final String TB_NAME_TRAYECTORIA="trayectoria";
    static final String TB_NAME_MOVILIDAD="movilidad";
    static final String TB_NAME_EVENTO="evento";
    static final String TB_NAME_TYPE_DOC="type_doc";
    static final String TB_NAME_CLIENTE="cliente";
    static final String TB_NAME_VISITACLIENTE="visita_cliente";
    static final String TB_NAME_INVVISITA_CLIENTE="inv_visita_cliente";
    static final int DB_VERSION=2;

    //Tabla Formulario
    static final String CREATE_TB="CREATE TABLE form (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " username TEXT NOT NULL, " +
            " cellphone TEXT NOT NULL, " +
            " correo TEXT NOT NULL, " +
            " image_c BLOB NOT NULL, " +
            " firma BLOB NOT NULL, " +
            " name TEXT NOT NULL, " +
            " fecha TEXT NOT NULL, " +
            " idevento TEXT NOT NULL, " +
            " idusuario TEXT NOT NULL, " +
            " conclusion TEXT NOT NULL);";
    //Tabla Usuario
    static final String CREATE_TB_USER="CREATE TABLE usuario (iduserlocal INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " id_user TEXT NOT NULL, " +
            " username TEXT NOT NULL, " +
            " estado TEXT NOT NULL, " +
            " id_emp TEXT NOT NULL, " +
            " img_user BLOB NULL, " +
            " url_img TEXT NULL);";
    //Tabla Posicion, donde se guarda la posicion del formulario completado
    static final String CREATE_TB_POSITION="CREATE TABLE position (id_posicion INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " id_user TEXT NOT NULL, " +
            " id_formu TEXT NOT NULL, " +
            " latitud TEXT NOT NULL, " +
            " longitud TEXT NOT NULL);";
    //Tabla Trayectoria
    static final String CREATE_TB_TRAYECTORIA="CREATE TABLE trayectoria (id_trayectoria INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " id_user TEXT NOT NULL, " +
            " latitud TEXT NOT NULL, " +
            " longitud TEXT NOT NULL, " +
            " fecha TEXT NOT NULL, " +
            " emp TEXT NOT NULL);";
    //Tabla Movibilidad
    static final String CREATE_TB_MOVILIDAD="CREATE TABLE movilidad (id_movilidad_local INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "android_id  TEXT NOT NULL, " +
            " code TEXT NOT NULL, " +
            " id_usuario TEXT NOT NULL, " +
            "id_movilidad TEXT NOT NULL, " +
            "alert_mobil TEXT NOT NULL, " +
            "alert_mail TEXT NOT NULL);";
    //Tabla EVENTO
    static final String CREATE_TB_EVENTO="CREATE TABLE evento (id_evento_local INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id  TEXT NOT NULL, " + //id del evento
            "id_usuario  TEXT NOT NULL, " +
            "title TEXT NOT NULL, " +
            "descripcion TEXT NOT NULL, " +
            "start  TEXT NOT NULL, " +
            "end TEXT NOT NULL, " +
            "color TEXT NOT NULL, " +
            "text_color  TEXT NOT NULL, " +
            "create_it TEXT NOT NULL, " +
            "create_by TEXT NOT NULL, " +
            "create_at  TEXT NOT NULL, " +
            "update_by TEXT NOT NULL, " +
            "update_at TEXT NOT NULL, " +
            "detalles TEXT NOT NULL, " +
            "estado TEXT NOT NULL, " +
            "estado_local TEXT NOT NULL, " +
            "id_cliente TEXT NOT NULL, " +
            "nombre_cliente  TEXT NOT NULL);";
    //Tabla Tipo Documento
    static final String CREATE_TB_TYPE_DOC="CREATE TABLE type_doc (id_typedoc INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id_type  TEXT NOT NULL, " +
            "name TEXT NOT NULL, " +
            "mask TEXT NULL, " +
            "state TEXT NULL, " +
            "fecha  TEXT NULL);";
    //Tabla Cliente
    static final String CREATE_TB_CLIENTE="CREATE TABLE cliente (idcliente INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "idtype  TEXT NOT NULL, " + // 1
            "type  TEXT NOT NULL, " +
            "document TEXT NOT NULL, " +
            "first_name TEXT NOT NULL, " +
            "second_name  TEXT NOT NULL, " + //5
            "surname1 TEXT NOT NULL, " +
            "surname2 TEXT NOT NULL, " +
            "address  TEXT NOT NULL, " +
            "mail TEXT NOT NULL, " +
            "phone1 TEXT NOT NULL, " +//10
            "phone2  TEXT NOT NULL, " +
            "lat TEXT NOT NULL, " +
            "lon TEXT NOT NULL, " +
            "idemp TEXT NOT NULL, " +//14
            "idusuario TEXT NOT NULL, " +
            "fecha  TEXT NOT NULL);";
    //Tabla cliente[id,nombre_completo]
    static final String CREATE_TB_VISITA_CLIENTE="CREATE TABLE visita_cliente (id_vcliente INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "idcliente  TEXT NOT NULL, " +
            "nombre  TEXT NOT NULL);";
    //Tabla visita cliente
    static final String CREATE_TB_INV_VISITA_CLIENTE="CREATE TABLE inv_visita_cliente (idcli INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "idusuario  TEXT NOT NULL, " +
            "idcliente  TEXT NOT NULL, " +
            "fotocli  BLOB NOT NULL, " +
            "comentario  TEXT NOT NULL, " +
            "latitud  TEXT NOT NULL, " +
            "longitud  TEXT NOT NULL, " +
            "fecha  TEXT NOT NULL, " +
            "idemp  TEXT NOT NULL, " +
            "nombrecliente  TEXT NOT NULL, " +
            "nombreimg  TEXT NOT NULL);";
}
