package com.gicapp.gicapp.sql;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Get_Hours;
import com.gicapp.gicapp.LoginPrefences;
import com.gicapp.gicapp.MainActivity;
import com.gicapp.gicapp.R;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.VolleySingleton;
import com.gicapp.gicapp.help_coordenadas;
import com.gicapp.gicapp.notificacion.ExampleService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class FormuList extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{
    private MyReceiver myReceiver3;

    ListView gridView;
    ArrayList<formu> list;
    FormuListAdapter adapter = null;

    public static BDAdapter BD;
    String id_user,nombre_user,telefono_user,mobil_alert,mail_alert;
    String var;
    ProgressDialog progreso;
    String emp;

    Bitmap bitmap_img,bitmap_firma;
    LoginPrefences p;
    help_coordenadas help_coordenadas;

    private boolean mBound = false;
    private GPS_Service mService = null;

    //volley
    StringRequest stringRequest;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_registro);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        help_coordenadas = new help_coordenadas();

        //open database
        BD = new BDAdapter(this);
        BD.openDB();

        //**************************************************************************************
        myReceiver3 = new FormuList.MyReceiver();
        setContentView(R.layout.activity_list_registro);
        //**************************************************************************************

        //get idusuario mediante sharedpreferneces
        p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
            telefono_user = p.GetTelefono();
            nombre_user = p.GetName();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
        }

        emp = p.GetEmp();//se obtiene el id de la empresa

        //Toast.makeText(FormuList.this,emp,Toast.LENGTH_SHORT).show();

        gridView = findViewById(R.id.listview);
        list = new ArrayList<>();
        adapter = new FormuListAdapter(this, R.layout.form_item, list);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener((adapterView, view, pos, l) -> {
            final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(FormuList.this);
            dialogDelete.setTitle("Alerta!!");
            dialogDelete.setMessage("Desea Enviar su Formulario?");
            dialogDelete.setPositiveButton("OK", (dialog, which) -> {
                try {
                    byte[] image, firma;

                    image = list.get(pos).getImage_c();
                    bitmap_img = BitmapFactory.decodeByteArray(image, 0, image.length);
                    bitmap_img = redimensionarImagen(bitmap_img);

                    firma = list.get(pos).getFirma();
                    bitmap_firma = BitmapFactory.decodeByteArray(firma, 0, firma.length);

                    progreso=new ProgressDialog(FormuList.this);
                    progreso.setMessage("Cargando...");
                    progreso.show();
                    //RequestQueue queue = Volley.newRequestQueue(FormuList.this);
                    stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_form.php",
                    //StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_form.php",
                            response -> {
                                try {
                                    JSONObject objResultado = new JSONObject(response);
                                    String estadox = objResultado.get("estado").toString();
                                    if(!estadox.contains("exito")){
                                        Toasty.error(this, "No se ha registrado", Toast.LENGTH_LONG).show();
                                    }else{
                                        //get_id_form(list.get(pos).getIdevento());
                                        get_idformulario(list.get(pos).getUsername(),list.get(pos).getCellphone(),list.get(pos).getCorreo(),list.get(pos).getName(),
                                                list.get(pos).getFecha(), list.get(pos).getConclusion(),list.get(pos).getIdevento());
                                        Toasty.success(this, "Enviado satisfactoriamente", Toast.LENGTH_LONG).show();
                                        BD.deleteData(list.get(pos).getId());
                                        updateList();
                                        //BD.closeDB();
                                    }
                                    progreso.hide();
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }, error -> {Toasty.error(FormuList.this, "No se ha Guardado su Formulario, Verifique su Conexion a Internet",
                            Toast.LENGTH_LONG).show(); progreso.hide();
                            Intent intent = new Intent(FormuList.this, MainActivity.class);
                            intent.putExtra("valoruser", id_user);//ojo
                            startActivity(intent);
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            String imagen=convertirImgString(bitmap_img);
                            String firma=convertirImgString(bitmap_firma);

                            Map<String, String> params = new HashMap<>();
                            params.put("username",list.get(pos).getUsername());
                            params.put("cellphone",list.get(pos).getCellphone());
                            params.put("correo",list.get(pos).getCorreo());
                            params.put("imagen_c",imagen);
                            params.put("firma",firma);
                            params.put("nombre_img",list.get(pos).getName());
                            params.put("id_user",id_user);
                            params.put("fecha",list.get(pos).getFecha());
                            params.put("idemp",emp);
                           // params.put("conclusion",list.get(pos).getConclusion());//**************delete**************
                            return params;
                        }
                    };
                    //queue.add(stringRequest);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }
            });
            dialogDelete.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            dialogDelete.show();
        });

        // se obtiene los formularios registrado para un usuario
        Cursor cursor =  BD.getData("SELECT * FROM form WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(0);
                String username = cursor.getString(1);
                String cellphone = cursor.getString(2);
                String correo = cursor.getString(3);
                byte[] image_c = cursor.getBlob(4);
                byte[] firma = cursor.getBlob(5);
                String name = cursor.getString(6);
                String fecha = cursor.getString(7);
                String idevento = cursor.getString(8);
                String conclusion = cursor.getString(10);
                list.add(new formu(username,cellphone,correo,image_c,firma,name,fecha,id,idevento,conclusion));
            }
        }else{
            Toasty.info(this, "No hay Formularios Pendientes", Toast.LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
    }

    private Bitmap redimensionarImagen(Bitmap bitmap) {

        int ancho=bitmap.getWidth();
        int alto=bitmap.getHeight();

        if(ancho> (float) 600 || alto> (float) 800){
            float escalaAncho= (float) 600 /ancho;
            float escalaAlto= (float) 800 /alto;

            Matrix matrix=new Matrix();
            matrix.postScale(escalaAncho,escalaAlto);

            return Bitmap.createBitmap(bitmap,0,0,ancho,alto,matrix,false);

        }else{
            return bitmap;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.list));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }

    private String convertirImgString(Bitmap bitmap) {
        ByteArrayOutputStream array=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,array);
        byte[] imagenByte=array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);

        return imagenString;
    }
    //se actualiza la lista del listview
    private void updateList(){
        // get all data from sqlite
        Cursor cursor = BD.getData("SELECT * FROM form WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String username = cursor.getString(1);
                String cellphone = cursor.getString(2);
                String correo = cursor.getString(3);
                byte[] image_c = cursor.getBlob(4);
                byte[] firma = cursor.getBlob(5);
                String name = cursor.getString(6);
                String fecha = cursor.getString(7);
                String idevento = cursor.getString(8);
                String conclusion = cursor.getString(10);
                list.add(new formu(username, cellphone, correo, image_c, firma, name, fecha,id,idevento,conclusion));
            }
        }else{
            stopService();//se detiene el servico de la notificacion
            Toasty.info(this, "No hay Formularios Pendientes", Toast.LENGTH_SHORT).show();
        }
        adapter.notifyDataSetChanged();
    }

    public void get_idformulario(String username,String telefono,String correo,String name,String fecha,String conclusion, String ideventolocal) {
        //String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        // RequestQueue queue = Volley.newRequestQueue(ListEvent.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/get_id_form.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("state").toString();

                        if (estadox.equals("No Encontrado")) {
                            Toasty.error(this, "Datos No Valido", Toast.LENGTH_LONG).show();
                            System.out.println("============> Datos No Valido");

                        } else if (estadox.equals("ERROR")) {
                            Toasty.error(this, "Faltan Datos Por Ingresar", Toast.LENGTH_LONG).show();
                            // Toast.makeText(this, "Faltan Datos Por Ingresar", Toast.LENGTH_LONG).show();
                        } else {
                            String idformulario = objResultado.get("id_formu").toString();
//                            Toasty.info(this, "=> " + idformulario, Toast.LENGTH_LONG).show();
//                            System.out.println("=> " + idformulario);
                            guardarGps(idformulario,ideventolocal,conclusion);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error ->  Toasty.error(this, "ERROR EN LA CONEXION - GET ID FORM", Toast.LENGTH_LONG).show() ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username",username);
                params.put("cellphone",telefono);
                params.put("correo",correo);

                params.put("nombre_img",name);
                params.put("id_user",id_user);
                params.put("fecha",fecha);

                params.put("idemp",emp);
               // params.put("conclusion",conclusion);//delete
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void guardarGps(String idformu, String ideventolocal, String conclusion)
    {
//        Toast.makeText(FormuList.this, id_user + " / " + idformu + " / " + help_coordenadas.getLat() + " / " +
//                help_coordenadas.getLon() + " / " + ideventolocal + " / " + emp, Toast.LENGTH_LONG).show();
        if(help_coordenadas.getLat() == null || help_coordenadas.getLat().isEmpty() && help_coordenadas.getLon() == null || help_coordenadas.getLon().isEmpty())
            savegps(idformu, ideventolocal, p.Getlat(), p.Getlon(),conclusion);
        else
            savegps(idformu, ideventolocal, help_coordenadas.getLat(), help_coordenadas.getLon(),conclusion);
    }
    public void savegps(String idformu, String ideventolocal, String lat, String lon,String conclusion){
       // RequestQueue queue = Volley.newRequestQueue(FormuList.this);
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/registrar_position.php",
                // StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "registrar_position.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            //var = "error";
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            BD.actualizar_eventos(ideventolocal);
                            Toasty.success(this, "Guardado", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(FormuList.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida al Enviar la Posiciòn", Toast.LENGTH_LONG).show()){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", id_user);
                params.put("id_formu", idformu);
                params.put("lat", lat);
                params.put("lon", lon);
                params.put("idevento", ideventolocal);
                params.put("idemp", emp);
                params.put("conclusion",conclusion);
                return params;
            }
        };
        //queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(FormuList.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
//        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(FormuList.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver3);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver3,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
                //verifica_coord(Utils.getlatitud(location), Utils.getlongitud(location));
                help_coordenadas.setLat(Utils.getlatitud(location));
                help_coordenadas.setLon(Utils.getlongitud(location));
                System.out.println("==FormuList=====> " + Utils.getlatitud(location)+" /*/ "+Utils.getlongitud(location) + " / " + Get_Hours.getHora("hh:mm a"));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void verifica_coord(String lat,String lon){
        if(lat == null || lat.isEmpty() && lon == null || lon.isEmpty())
            trayectoria_verifica_internet(p.Getlat(), p.Getlon());
        else
            trayectoria_verifica_internet(lat, lon);
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            verifica_mi_ubicacion(lat, lon);
                            guardartrayectoria(id_user,lat, lon,emp);
                        }
                        else {
//                            Toast.makeText(FormuList.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,lat, lon,emp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon,emp);
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2,String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2,String emp) {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
           // BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifica_mi_ubicacion(String latitud, String longitud){
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    System.out.println("Respues ======> " + valida);

                                    if(valida.equals("Fuera")){
//                                        Toasty.error(this, "Fuera de su Trayectoria!!!!", Toast.LENGTH_LONG).show();
                                        send_correo(latitud,longitud);
                                        enviar_mensaje(latitud,longitud);
                                    }
                                    i++;
                                }

                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.info(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);
                params.put("latitud", latitud);//12.119639
                params.put("longitud", longitud);//-86.311440
                params.put("idemp", emp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola " + nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(mobil_alert, null, text , null, null);
    }

    //se envia el correo
    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                    System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
//                params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
