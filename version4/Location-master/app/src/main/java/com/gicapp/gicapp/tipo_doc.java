package com.gicapp.gicapp;

import java.io.Serializable;

public class tipo_doc implements Serializable {

    private String id;
    private String nombre;

    public tipo_doc(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public tipo_doc(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
