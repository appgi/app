package com.gicapp.gicapp.visita_guarda;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Get_Hours;
import com.gicapp.gicapp.LoginPrefences;
import com.gicapp.gicapp.MainActivity;
import com.gicapp.gicapp.R;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.VolleySingleton;
import com.gicapp.gicapp.help_coordenadas;
import com.gicapp.gicapp.sql.BDAdapter;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Visita_cliente extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{

    //se instancia la clase
    public static BDAdapter BD;
    help_coordenadas help_coordenadas;
    LoginPrefences p;
    //spinner de los clientes
    ArrayList<String> listaPersonas;
    ArrayList<datos_cliente> personasList;
    String id_user,id_cliente,nombre_cliente;
    int pb_idcliente;
    //volley
    StringRequest stringRequest;

    //spinner
    Spinner spinner_cli,spinner_cli2;
    TextInputLayout input_comentario;

    //tomar foto
    Button botonCargar,btn_registrar_vcliente;
    ImageView imagen;
    String path_img, idemp,nombre_user,mail_alert,mobil_alert;
    private final String CARPETA_RAIZ="camiplanner/";
    private final String RUTA_IMAGEN=CARPETA_RAIZ+"Fotosclientes";

    final int COD_FOTO=20;

    Bitmap bitmap_img;

    // Creating Separate Directory for saving Generated Images
    String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/foto_cli/";
    String pic_name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

    String StoredPath = DIRECTORY + pic_name + ".png";
    String name = pic_name;
    //////
    //segundo plano
    private MyReceiver myReceiver2;
    private Gson gson = new Gson();


    ProgressDialog progreso;

    private boolean mBound = false;
    private GPS_Service mService = null;
    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visita_cliente);

        BD = new BDAdapter(this);
        BD.openDB();

        progreso = new ProgressDialog(Visita_cliente.this);
        help_coordenadas = new help_coordenadas();
        p = new LoginPrefences(this);
        if (!p.isEmpty()){
            id_user = p.Getid();
            idemp = p.GetEmp();
            nombre_user = p.GetName();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
        }

        botonCargar= findViewById(R.id.btnCargarImg_cli);
        spinner_cli = findViewById(R.id.spinner_cli);
        input_comentario = findViewById(R.id.input_comentario);
        imagen = findViewById(R.id.imagen_cli);
        btn_registrar_vcliente = findViewById(R.id.btn_registar_cli);
        spinner_cli2 = findViewById(R.id.spinner_cli2);

        //segundo p
        myReceiver2 = new MyReceiver();

        //se obtiene la lista de cliente
        //consultar_cliente();
        consultar_lista_cliente();

//        ArrayAdapter<CharSequence> adaptador=new ArrayAdapter(this,R.layout.item_spinner,listaPersonas);
//        spinner_cli.setAdapter(adaptador);
//        spinner_cli.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long idl) {
//                if (position!=0){
//                    //System.out.println("Mostrando======> "+pos+ " ||| "+personasList.get(position-1).getNombrecompleto());
//                    if(spinner_cli.getItemAtPosition(spinner_cli.getSelectedItemPosition()).toString().equals(personasList.get(position-1).getNombrecompleto())){
//                        System.out.println("Mostrando======> id: "+ personasList.get(position-1).getId_cliente());
//                        id_cliente = personasList.get(position-1).getId_cliente();
//                        nombre_cliente = personasList.get(position-1).getNombrecompleto();
//                    }else if(spinner_cli.getItemAtPosition(spinner_cli.getSelectedItemPosition()).toString().equals("Seleccione")){
//                        Toasty.error(getApplicationContext(), "Seleccione un Cliente", Toast.LENGTH_LONG).show();
//                        System.out.println("Mostrando======>  xd");
//                    }
//                }
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        ArrayAdapter<CharSequence> adaptador2=new ArrayAdapter(this,R.layout.item_spinner,listaPersonas);
        spinner_cli2.setAdapter(adaptador2);
        spinner_cli2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long idl) {
                if (position!=0){
                    //System.out.println("Mostrando======> "+pos+ " ||| "+personasList.get(position-1).getNombrecompleto());
                    if(spinner_cli2.getItemAtPosition(spinner_cli2.getSelectedItemPosition()).toString().equals(personasList.get(position-1).getNombrecompleto())){
                        System.out.println("Mostrando======> id: "+ personasList.get(position-1).getId_cliente());
                        id_cliente = personasList.get(position-1).getId_cliente();
                        nombre_cliente = personasList.get(position-1).getNombrecompleto();
                    }else if(spinner_cli2.getItemAtPosition(spinner_cli2.getSelectedItemPosition()).toString().equals("Seleccione")){
                        Toasty.error(getApplicationContext(), "Seleccione un Cliente", Toast.LENGTH_LONG).show();
                        System.out.println("Mostrando======>  xd");
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //******camara*******
        if(validaPermisos()){
            botonCargar.setEnabled(true);
        }else{
            botonCargar.setEnabled(false);
        }

        //********************************************************** verificacion de internet **********************************************************
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 || tipoConexion2) {
                /* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                btn_registrar_vcliente.setOnClickListener(v -> {
                    //se valida que los campos nombre telefono correo esten completo

                   if(!valida_spinner())
                       return;

                    if(imagen.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.img_base).getConstantState())
                        Toasty.error(this, "Agrege una Foto", Toast.LENGTH_LONG).show();

                    if(!validateComentario())
                        return;

                    verifica_internet();

                });
            }
        }
        else {
            /* No estas conectado a internet */
            Toasty.info(getApplicationContext(), "No hay intenet. Por lo tanto, los datos se guardaran localmente", Toast.LENGTH_SHORT).show();
            //boton registrar -> sin conexion a internet
            btn_registrar_vcliente.setOnClickListener(v -> {
                //se valida que los campos nombre telefono correo esten completo

                if(!valida_spinner())
                    return;

                if(imagen.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.img_base).getConstantState())
                    Toasty.error(this, "Agrege una Foto", Toast.LENGTH_LONG).show();

                if(!validateComentario())
                    return;

                verifica_internet();

            });
        }
    }
    public boolean valida_spinner(){
        if(spinner_cli2.getSelectedItemPosition() == 0){
            Toasty.error(this, "Seleccione un Cliente", Toast.LENGTH_LONG).show();
            return false;
        }else{
            return true;
        }
    }
    ///********************** valida campo *********************
    private boolean validateComentario() {
        String comentarioInput = input_comentario.getEditText().getText().toString().trim();
        if (comentarioInput.isEmpty()) {
            input_comentario.setError("Campo vacio");
            Toasty.error(this, "Complete el Campo Comentario", Toast.LENGTH_LONG).show();
            return false;
        }else {
            input_comentario.setError(null);
            return true;
        }
    }

    //************************************** obtener lista cliente *******************
    public void consultar_cliente(){
        //se obtiene el id y nombre de los clientes
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/get_id_cliente.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        // Obtener atributo "estado"
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1": // EXITO
//                                // Obtener array "metas" Json
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;

                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String id =object1.getString("Id_cliente");
                                    // String nombre =object1.getString("name");
                                    //StandardCharsets -> Permite palabras con  Acentos
                                    String p_nombre = new String(object1.getString("first_name").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                    String s_nombre = new String(object1.getString("second_name").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                    String p_apellido = new String(object1.getString("surname1").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                                    String s_apellido = new String(object1.getString("surname2").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);


                                    String nombre_completo = p_nombre + " " + s_nombre  + " " + p_apellido  + " " + s_apellido;

//                                    System.out.println("Guardando ======> " + nombre_completo);
                                    if(BD.verifica_namecliente(id)){
                                        BD.update_name_cliente(id,nombre_completo);
                                    }else{
                                        if(!BD.verifica_visitacliente(id,nombre_completo)){
                                            BD.insert_tb_visitacliente(id,nombre_completo);
                                        }
                                    }
                                    i++;
                                }
                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "No Tiene Acceso a Intenet - GET ID CLIENTE", Toast.LENGTH_LONG).show()){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idemp",idemp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void consultar_lista_cliente(){
        datos_cliente persona=null;
        personasList =new ArrayList<datos_cliente>();

        Cursor cursor =  BD.getData("SELECT * FROM visita_cliente");
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                persona = new datos_cliente();

                persona.setId_cliente(cursor.getString(1));
                persona.setNombrecompleto(cursor.getString(2));
//                persona.setS_name(cursor.getString(3));
//                persona.setSurname1(cursor.getString(4));
//                persona.setSurname2(cursor.getString(5));
                personasList.add(persona);

            }
        }

        obtenerLista();
    }
    private void obtenerLista() {
        listaPersonas=new ArrayList<String>();
        listaPersonas.add("Seleccione");

        for(int i=0;i<personasList.size();i++){
            listaPersonas.add(personasList.get(i).getNombrecompleto());
        }

    }
    //*********************************section tomar foto**********************************
    public void tomar_foto_cli(View v){
        File fileImagen=new File(Environment.getExternalStorageDirectory(),RUTA_IMAGEN);
        boolean isCreada=fileImagen.exists();
        String nombreImagen="";
        if(!isCreada){
            isCreada=fileImagen.mkdirs();
        }

        if(isCreada){
            nombreImagen=(System.currentTimeMillis()/1000)+".png";
        }


        path_img=Environment.getExternalStorageDirectory()+
                File.separator+RUTA_IMAGEN+File.separator+nombreImagen;

        File imagen=new File(path_img);

        Intent intent=null;
        intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ////
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
        {
            String authorities=getApplicationContext().getPackageName()+".provider";
            Uri imageUri= FileProvider.getUriForFile(this,authorities,imagen);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        }else
        {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imagen));
        }
        startActivityForResult(intent,COD_FOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == COD_FOTO) {
                MediaScannerConnection.scanFile(this, new String[]{path_img}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("Ruta de almacenamiento", "Path: " + path);
                            }
                        });

                bitmap_img = BitmapFactory.decodeFile(path_img);
                //establecer la foto en el imageview dependiendo la posicion del telefono
                try {
                    ExifInterface exif = new ExifInterface(path_img);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    Matrix matrix = new Matrix();
                    if (orientation == 6) {
                        matrix.postRotate(90);
                    }
                    else if (orientation == 3) {
                        matrix.postRotate(180);
                    }
                    else if (orientation == 8) {
                        matrix.postRotate(270);
                    }
                    bitmap_img = Bitmap.createBitmap(bitmap_img, 0, 0, bitmap_img.getWidth(), bitmap_img.getHeight(), matrix, true); // rotating bitmap
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                bitmap_img=redimensionarImagen(bitmap_img);
                imagen.setImageBitmap(bitmap_img);
            }
        }
    }

    private Bitmap redimensionarImagen(Bitmap bitmap) {
        int ancho=bitmap.getWidth();
        int alto=bitmap.getHeight();

        if(ancho> (float) 600 || alto> (float) 800){
            float escalaAncho= (float) 600 /ancho;
            float escalaAlto= (float) 800 /alto;

            Matrix matrix=new Matrix();
            matrix.postScale(escalaAncho,escalaAlto);

            return Bitmap.createBitmap(bitmap,0,0,ancho,alto,matrix,false);

        }else{
            return bitmap;
        }
    }
    //permisos
    private boolean validaPermisos() {

        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
            return true;
        }

        if((checkSelfPermission(CAMERA)== PackageManager.PERMISSION_GRANTED)&&
                (checkSelfPermission(WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED)){
            return true;
        }

        if((shouldShowRequestPermissionRationale(CAMERA)) ||
                (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
            cargarDialogoRecomendacion();
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==100){
            if(grantResults.length==2 && grantResults[0]== PackageManager.PERMISSION_GRANTED
                    && grantResults[1]==PackageManager.PERMISSION_GRANTED){
                botonCargar.setEnabled(true);
            }else{
                solicitarPermisosManual();
            }
        }
    }

    private void cargarDialogoRecomendacion() {
        AlertDialog.Builder dialogo=new AlertDialog.Builder(Visita_cliente.this);
        dialogo.setTitle("Permisos Desactivados");
        dialogo.setMessage("Debe aceptar los permisos para el correcto funcionamiento de la App");

        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
            }
        });
        dialogo.show();
    }

    private void solicitarPermisosManual() {
        final CharSequence[] opciones={"si","no"};
        final AlertDialog.Builder alertOpciones=new AlertDialog.Builder(Visita_cliente.this);
        alertOpciones.setTitle("¿Desea configurar los permisos de forma manual?");
        alertOpciones.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (opciones[i].equals("si")){
                    Intent intent=new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri=Uri.fromParts("package",getPackageName(),null);
                    intent.setData(uri);
                    startActivity(intent);
                }else{
                    Toasty.error(Visita_cliente.this, "Los permisos no fueron aceptados", Toast.LENGTH_LONG).show();
                    dialogInterface.dismiss();
                }
            }
        });
        alertOpciones.show();
    }
    ////////////////////////////////////////////////////////////////////////
    /// verifica internet
    public void verifica_internet(){
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            verifica_coord();
                        }else{
                            verifica_coord2();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error ->  verifica_coord2() ){// Toasty.error(this, "ERROR EN LA CONEXION - VERIFICA INTENERT", Toast.LENGTH_LONG).show()) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void verifica_coord()
    {
        if(help_coordenadas.getLat() == null || help_coordenadas.getLat().isEmpty() && help_coordenadas.getLon() == null || help_coordenadas.getLon().isEmpty()) {
            send_visita_cliente(p.Getlat(),p.Getlon());
        }else {
            send_visita_cliente(help_coordenadas.getLat() , help_coordenadas.getLon());
        }
    }

    public void verifica_coord2()
    {
        if(help_coordenadas.getLat() == null || help_coordenadas.getLat().isEmpty() && help_coordenadas.getLon() == null || help_coordenadas.getLon().isEmpty()) {
            save_visita_cliente_local(p.Getlat(),p.Getlon());
        }else {
            save_visita_cliente_local(help_coordenadas.getLat() , help_coordenadas.getLon());
        }
    }

    public void send_visita_cliente(String latitud, String longitud)
    {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        progreso.setMessage("Cargando...");
        progreso.show();

        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/guardar_visita_cliente.php",
        //stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE2 + "guardar_visita_cliente.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(Visita_cliente.this, "No se pudo registrar", Toast.LENGTH_LONG).show();
                        }else{
                            System.out.println("====>> " + latitud + " / " + longitud + " // " + id_cliente);
                            Toasty.success(Visita_cliente.this,"se ha registrado con exito",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Visita_cliente.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        progreso.hide();
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }, error -> { save_visita_cliente_local(latitud,longitud);//Toasty.error(Visita_cliente.this, "Conexion invalida - Guardar Cliente",Toast.LENGTH_LONG).show();
            progreso.hide();
            }) {
            @Override
            protected Map<String, String> getParams() {
                String imagen=convertirImgString(bitmap_img);

                Map<String, String> params = new HashMap<>();
                params.put("idusuario",id_user);
                params.put("id_cliente",id_cliente);
                params.put("nombrecliente",nombre_cliente);

                params.put("foto_cli",imagen);
                params.put("comentario",input_comentario.getEditText().getText().toString().trim());
                params.put("latitud",latitud);

                params.put("longitud",longitud);
                params.put("fecha",fecha);
                params.put("idemp",idemp);//new
                params.put("nombre_img",name);
                params.put("state","Activo");//new
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void save_visita_cliente_local(String latitud, String longitud){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
//        progreso=new ProgressDialog(Visita_cliente.this);
//        progreso.setMessage("Cargando...");
//        progreso.show();

        //se guarda la visita al cliente de manera local
        BD.guardar_visita_cliente(id_user,id_cliente,imageViewToByte(imagen), input_comentario.getEditText().getText().toString().trim(), latitud, longitud,
                fecha, idemp, nombre_cliente,name);

        //BD.closeDB();
        Toasty.success(this, "Su Registro Visita Cliente, se ha Guardado Localmente", Toast.LENGTH_LONG).show();
       // progreso.hide();
        //retorna al main activity
        Intent intent = new Intent(Visita_cliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    private String convertirImgString(Bitmap bitmap) {
        ByteArrayOutputStream array=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,array);
        byte[] imagenByte=array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);
        return imagenString;
    }
    //segundo plano
    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.visitacliente));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver2);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver2,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    @Override
    public boolean onSupportNavigateUp() {
//        onBackPressed();
        Intent intent = new Intent(Visita_cliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
        return true;
    }



    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(Visita_cliente.this, Utils.getlatitud(location) + " /event/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
             //  trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));

                System.out.println("==visita_cliente===========> " + Utils.getlatitud(location)+" /*/ "+Utils.getlongitud(location) + " / " + Get_Hours.getHora("hh:mm a"));
                help_coordenadas.setLat(Utils.getlatitud(location));
                help_coordenadas.setLon(Utils.getlongitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon)
    {
        //RequestQueue queue = Volley.newRequestQueue(Formulario.this);
        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            verifica_mi_ubicacion(lat, lon);
                            guardartrayectoria(id_user,lat, lon);
                        }
                        else {
                            Toasty.info(this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            guardartrayectorialocal(id_user,lat, lon);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon);
        }) {
        };
        // queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();

        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
                //StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error ->  Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp",emp);
                return params;
            }
        };
        //queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2) {
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            //BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifica_mi_ubicacion(String latitud, String longitud){
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/alerta.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String state = objResultado.getString("state");
                        switch (state) {
                            case "1":
                                JSONArray array = objResultado.getJSONArray("respuesta");
                                int i = 0;
                                while (i<array.length())
                                {
                                    JSONObject object1=array.getJSONObject(i);
                                    String valida =object1.getString("valida");

                                    System.out.println("Respues ======> " + valida);

                                    if(valida.equals("Fuera")){
//                                        Toasty.error(this, "Fuera de su Trayectoria!!!!", Toast.LENGTH_LONG).show();
                                        send_correo(latitud,longitud);
                                        enviar_mensaje(latitud,longitud);
                                    }
                                    i++;
                                }
                                break;
                            case "2": // FALLIDO
                                String mensaje2 = objResultado.getString("mensaje");
                                Toast.makeText(
                                        this,
                                        mensaje2,
                                        Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(
                                        this,
                                        "el valor de state es: " + state,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.info(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", id_user);
                params.put("latitud", latitud);
                params.put("longitud", longitud);
                params.put("idemp", idemp);//add
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void send_correo(String lati,String longit)
    {
        String maps = "https://maps.google.com/maps?q="+lati+","+longit+"";
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/send_correo.php",
                response -> {
                    System.out.println("==========> Correo Enviado "+ maps);
                }, error -> Toasty.warning(this, "Conexion Invalida - correo", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("nombre_usuario", nombre_user);//Change
                params.put("ubicacion",maps);
//                params.put("correo",mail_alert);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    public void enviar_mensaje(String lat, String lon)
    {
        String maps = "https://maps.google.com/maps?q="+lat+","+lon+"";
        String text = "Hola " + nombre_user +"\n\nSe ha salido de su Trayectoria, Su Ubicación: " + maps;
        System.out.println(text);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(mobil_alert, null, text , null, null);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Visita_cliente.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }

}