package com.gicapp.gicapp.visita_guarda;

public class class_visita_cliente {
    private int idcli;
    private String idusuario;
    private String idcliente;
    private byte[] fotocli;
    private String comentario;
    private String latitud;
    private String longitud;
    private String fecha;
    private String idemp;
    private String nombrecliente;
    private String nombreimg;

    public class_visita_cliente(int idcli, String idusuario, String idcliente, byte[] fotocli, String comentario, String latitud, String longitud, String fecha, String idemp, String nombrecliente, String nombreimg) {
        this.idcli = idcli;
        this.idusuario = idusuario;
        this.idcliente = idcliente;
        this.fotocli = fotocli;
        this.comentario = comentario;
        this.latitud = latitud;
        this.longitud = longitud;
        this.fecha = fecha;
        this.idemp = idemp;
        this.nombrecliente = nombrecliente;
        this.nombreimg = nombreimg;
    }

    public int getIdcli() {
        return idcli;
    }

    public void setIdcli(int idcli) {
        this.idcli = idcli;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(String idcliente) {
        this.idcliente = idcliente;
    }

    public byte[] getFotocli() {
        return fotocli;
    }

    public void setFotocli(byte[] fotocli) {
        this.fotocli = fotocli;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIdemp() {
        return idemp;
    }

    public void setIdemp(String idemp) {
        this.idemp = idemp;
    }

    public String getNombrecliente() {
        return nombrecliente;
    }

    public void setNombrecliente(String nombrecliente) {
        this.nombrecliente = nombrecliente;
    }

    public String getNombreimg() {
        return nombreimg;
    }

    public void setNombreimg(String nombreimg) {
        this.nombreimg = nombreimg;
    }
}
