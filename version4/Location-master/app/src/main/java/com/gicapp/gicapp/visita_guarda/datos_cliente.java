package com.gicapp.gicapp.visita_guarda;

public class datos_cliente {
    private String id_cliente;
    private String nombrecompleto;
    private String f_name;
    private String s_name;
    private String surname1;
    private String surname2;

    public datos_cliente(String id_cliente, String f_name, String s_name, String surname1, String surname2) {
        this.id_cliente = id_cliente;
        this.f_name = f_name;
        this.s_name = s_name;
        this.surname1 = surname1;
        this.surname2 = surname2;
    }

    public datos_cliente(){

    }

    public String getNombrecompleto() {
        return nombrecompleto;
    }

    public void setNombrecompleto(String nombrecompleto) {
        this.nombrecompleto = nombrecompleto;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public String getSurname1() {
        return surname1;
    }

    public void setSurname1(String surname1) {
        this.surname1 = surname1;
    }

    public String getSurname2() {
        return surname2;
    }

    public void setSurname2(String surname2) {
        this.surname2 = surname2;
    }
}
