package com.gicapp.gicapp.visita_guarda;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gicapp.gicapp.R;

import java.util.ArrayList;

public class visitaclienteAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<class_visita_cliente> visitacliente_list;

    public visitaclienteAdapter(Context context, int layout, ArrayList<class_visita_cliente> visitacliente_list) {
        this.context = context;
        this.layout = layout;
        this.visitacliente_list = visitacliente_list;
    }
    @Override
    public int getCount() {
        return visitacliente_list.size();
    }

    @Override
    public Object getItem(int position) {
        return visitacliente_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView idcliente,comentario,fecha;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.idcliente = (TextView) row.findViewById(R.id.txt_idcliente);
            holder.comentario = (TextView) row.findViewById(R.id.txt_comentariocli);
            holder.fecha = (TextView) row.findViewById(R.id.txt_fechacli);
            holder.imageView = (ImageView) row.findViewById(R.id.imagen_visitacliente);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder) row.getTag();
        }

        class_visita_cliente c_visita_cliente = visitacliente_list.get(position);

        holder.idcliente.setText(c_visita_cliente.getIdcliente());
        holder.comentario.setText(c_visita_cliente.getComentario());
//        if(formulario.getCorreo() == null || formulario.getCorreo().isEmpty()) holder.correo.setText("No Posee Correo");
//        else
        holder.fecha.setText(c_visita_cliente.getFecha());


        byte[] foto_cli = c_visita_cliente.getFotocli();
        Bitmap bitmap = BitmapFactory.decodeByteArray(foto_cli, 0, foto_cli.length);
        holder.imageView.setImageBitmap(bitmap);

        return row;
    }
}
