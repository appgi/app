package com.gicapp.gicapp.visita_guarda;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.gicapp.gicapp.Get_Hours;
import com.gicapp.gicapp.LoginPrefences;
import com.gicapp.gicapp.MainActivity;
import com.gicapp.gicapp.R;
import com.gicapp.gicapp.Services.GPS_Service;
import com.gicapp.gicapp.Services.Utils;
import com.gicapp.gicapp.VolleySingleton;
import com.gicapp.gicapp.help_coordenadas;
import com.gicapp.gicapp.notificacion.ExampleService;
import com.gicapp.gicapp.sql.BDAdapter;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class visitacliente_list extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{
    String id_user,telefono_user,nombre_user,mobil_alert,mail_alert;
    Bitmap fotocli;
    LoginPrefences p;
    help_coordenadas help_coordenadas;

    public static BDAdapter BD;

    ListView listView;
    ArrayList<class_visita_cliente> list;
    visitaclienteAdapter adapter = null;

    ProgressDialog progreso;

    private MyReceiver myReceiver2;
    private Gson gson = new Gson();

    private boolean mBound = false;
    private GPS_Service mService = null;

    //volley
    StringRequest stringRequest;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            GPS_Service.LocalBinder binder = (GPS_Service.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visitacliente_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        //open database
        BD = new BDAdapter(this);
        BD.openDB();
        progreso = new ProgressDialog(visitacliente_list.this);
        p = new LoginPrefences(this);

        if (!p.isEmpty()){
            id_user = p.Getid();
            telefono_user = p.GetTelefono();
            nombre_user = p.GetName();
            mobil_alert = p.GetAlert_mobil();
            mail_alert = p.GetAlert_mail();
        }

        myReceiver2 = new MyReceiver();
        setContentView(R.layout.visitacliente_list);

        listView = findViewById(R.id.listview_visitacliente);
        list = new ArrayList<>();
        adapter = new visitaclienteAdapter(this, R.layout.visitacliente_item, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((adapterView, view, pos, l) -> {
            final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(visitacliente_list.this);
            dialogDelete.setTitle("Alerta!!");
            dialogDelete.setMessage("Desea enviar este Registro?");
            dialogDelete.setPositiveButton("OK", (dialog, which) -> {
                try {
                    byte[] image;
                    image = list.get(pos).getFotocli();
                    fotocli = BitmapFactory.decodeByteArray(image, 0, image.length);
                    fotocli = redimensionarImagen(fotocli);

                    progreso.setMessage("Cargando...");
                    progreso.show();
                    stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/guardar_visita_cliente.php",
                   // stringRequest = new StringRequest(Request.Method.POST,  Ruta.URL_WEB_SERVICE2 + "guardar_visita_cliente.php",
                            response -> {
                                try {
                                    JSONObject objResultado = new JSONObject(response);
                                    String estadox = objResultado.get("estado").toString();
                                    if(!estadox.contains("exito")){
                                        Toasty.error(this, "No se ha registrado", Toast.LENGTH_LONG).show();
                                    }else{
                                        Toasty.success(this, "Enviado satisfactoriamente", Toast.LENGTH_LONG).show();
                                        updatelist();
                                        BD.delete_visitacliente(list.get(pos).getIdcli());
                                        //BD.closeDB();
                                        Intent intent = new Intent(visitacliente_list.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                    progreso.hide();
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }, error -> {
                            Toasty.error(visitacliente_list.this, "No se ha Guardado su Visita Cliente, Verifique su Conexion a Internet",
                            Toast.LENGTH_LONG).show();
                            progreso.hide();
                            Intent intent = new Intent(visitacliente_list.this, MainActivity.class);
                            intent.putExtra("valoruser", id_user);//ojo
                            startActivity(intent);
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            String imagen=convertirImgString(fotocli);
                            Map<String, String> params = new HashMap<>();
                            params.put("idusuario",list.get(pos).getIdusuario());
                            params.put("id_cliente",list.get(pos).getIdcliente());
                            params.put("nombrecliente",list.get(pos).getNombrecliente());

                            params.put("foto_cli",imagen);
                            params.put("comentario",list.get(pos).getComentario());
                            params.put("latitud",list.get(pos).getLatitud());

                            params.put("longitud",list.get(pos).getLongitud());
                            params.put("fecha",list.get(pos).getFecha());
                            params.put("idemp",list.get(pos).getIdemp());//new
                            params.put("nombre_img",list.get(pos).getNombreimg());
                            params.put("state","Activo");//new
                            return params;
                        }
                    };
                    //queue.add(stringRequest);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);

                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }
            });
            dialogDelete.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
            dialogDelete.show();
        });

        Cursor cursor =  BD.getData("SELECT * FROM inv_visita_cliente WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(0);
                String idusuario = cursor.getString(1);
                String id_cliente = cursor.getString(2);
                byte[] foto_cli = cursor.getBlob(3);
                String comentario = cursor.getString(4);
                String latitud = cursor.getString(5);
                String longitud = cursor.getString(6);
                String fecha = cursor.getString(7);
                String idemp = cursor.getString(8);
                String nombrecliente = cursor.getString(9);
                String nombreimg = cursor.getString(10);
                list.add(new class_visita_cliente(id,idusuario,id_cliente,foto_cli,comentario,latitud,longitud,fecha,idemp,nombrecliente,nombreimg));
            }
        }else{
            Toasty.info(this, "No hay Registro de Visita Clientes", Toast.LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
    }
    //se actualiza la lista de cliente
    public void updatelist(){
        Cursor cursor =  BD.getData("SELECT * FROM inv_visita_cliente WHERE idusuario = '"+id_user+"'");
        list.clear();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(0);
                String idusuario = cursor.getString(1);
                String id_cliente = cursor.getString(2);
                byte[] foto_cli = cursor.getBlob(3);
                String comentario = cursor.getString(4);
                String latitud = cursor.getString(5);
                String longitud = cursor.getString(6);
                String fecha = cursor.getString(7);
                String idemp = cursor.getString(8);
                String nombrecliente = cursor.getString(9);
                String nombreimg = cursor.getString(10);
                list.add(new class_visita_cliente(id,idusuario,id_cliente,foto_cli,comentario,latitud,longitud,fecha,idemp,nombrecliente,nombreimg));
            }
        }else{
            stopService();//se detiene el servico de la notificacion
            Toasty.info(this, "No hay Registro de Visita Clientes", Toast.LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ExampleService.class);
        stopService(serviceIntent);
    }

    private String convertirImgString(Bitmap bitmap) {
        ByteArrayOutputStream array=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,array);
        byte[] imagenByte=array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);

        return imagenString;
    }

    private Bitmap redimensionarImagen(Bitmap bitmap) {
        int ancho=bitmap.getWidth();
        int alto=bitmap.getHeight();

        if(ancho> (float) 600 || alto> (float) 800){
            float escalaAncho= (float) 600 /ancho;
            float escalaAlto= (float) 800 /alto;

            Matrix matrix=new Matrix();
            matrix.postScale(escalaAncho,escalaAlto);

            return Bitmap.createBitmap(bitmap,0,0,ancho,alto,matrix,false);

        }else{
            return bitmap;
        }
    }
    //segundo plano
    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        boolean flag = true;

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.listacli));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(new Intent(this, GPS_Service.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }
    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver2);
    }
    @Override
    protected void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver2,
                new IntentFilter(GPS_Service.ACTION_BROADCAST));
    }
    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(GPS_Service.EXTRA_LOCATION);
            if (location != null) {
//                Toast.makeText(Visita_cliente.this, Utils.getlatitud(location) + " /event/ " + Utils.getlongitud(location),
//                        Toast.LENGTH_SHORT).show();
               // trayectoria_verifica_internet(Utils.getlatitud(location), Utils.getlongitud(location));
//
                System.out.println("==visita_cliente_list======> " + Utils.getlatitud(location)+" /*/ "+Utils.getlongitud(location) + " / " + Get_Hours.getHora("hh:mm a"));
//                help_coordenadas.setLat(Utils.getlatitud(location));
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
//        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
//            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
//                    false));
//        }
    }

    public void trayectoria_verifica_internet(String lat, String lon){
        LoginPrefences p = new LoginPrefences(this);
        String emp = p.GetEmp();

        stringRequest = new StringRequest(Request.Method.POST, "http://webser.nicaapps.com/app/webservices/verifica_internet.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        if(objResultado.get("internet").toString().equals("yes")) {
                            //Toast.makeText(MainActivity.this, "Tiene Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //si hay internet las coordenadas se guardaran en el servidor
                            //verifica_mi_ubicacion(lat, lon);
                            guardartrayectoria(id_user,lat, lon,emp);
                        }
                        else {
                            //Toast.makeText(ListEvent.this, "No hay Acceso a INTERNET", Toast.LENGTH_LONG).show();
                            //si no hay internet las coordenadas se guardaran localmente
                            guardartrayectorialocal(id_user,lat, lon,emp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            guardartrayectorialocal(id_user,lat, lon,emp);
        }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //se guarda la trayectoria del usuario
    public void guardartrayectoria(String iduser,String c1, String c2,String emp){
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        stringRequest = new StringRequest(Request.Method.POST,  "http://webser.nicaapps.com/app/webservices/save_trayectoria.php",
//         StringRequest stringRequest = new StringRequest(Request.Method.POST, Ruta.URL_WEB_SERVICE + "save_trayectoria.php",
                response -> {
                    try {
                        JSONObject objResultado = new JSONObject(response);
                        String estadox = objResultado.get("estado").toString();
                        if(!estadox.contains("exito")){
                            Toasty.error(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
                        }else{
                            Toasty.success(this, "Guardando Trayectoria en el Servidor..", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> Toasty.error(this, "Conexion Invalida - Trayectoria", Toast.LENGTH_LONG).show()){//var = "error2") {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", iduser);
                params.put("lat", c1);
                params.put("lon", c2);
                params.put("fecha", fecha);
                params.put("idemp", emp);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //guarda la trayectoria del usuario localmente
    public void guardartrayectorialocal(String iduser,String c1, String c2, String emp)
    {
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        try {
            BD.insert_trayect(iduser, c1, c2, fecha,emp);
            Toasty.success(this, "Guardando Trayectoria Localmente..", Toast.LENGTH_LONG).show();
            //BD.closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(visitacliente_list.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//desapila las actividades
        startActivity(intent);
    }
}
